require('dotenv').config()
const { ObjectId } = require('mongodb')
const fs = require('fs-extra')
// axiosMethods, methods sheizleba momavalshi gamovikenot
import { enums } from 'helpers'
// const enums = require('helpers')
const cors = require('cors')
const express = require('express')
const WebSocket = require('./utils/websocket')
const path = require('path')
const bodyParser = require('body-parser')
const EventEmitter2 = require('eventemitter2').EventEmitter2
const postService = require('./chat/post')
const createDirIfNotExists = require('./utils/fileUploader').createDirIfNotExists
const createChatService = require('./chat/chat')
const { MongoClient } = require('./utils/MongoClient')
const { logger } = require('./utils/logger')
const { getRelPathAndFilenameFromMediaUrl } = require('./utils/help')
global.$log = logger
global.$enums = enums
const dbName = 'desmongo' // socketchat
const namespace = 'chat'
const { translateTextArray, translateText } = require('./utils/translator')

console.log('cs3 testInvoiceUpload = ')
// testInvoiceUpload()
function testInvoiceUpload() {
  const emitter = new EventEmitter2({
    newListener: false,
    wildcard: true
  })
  global.$mongo = new MongoClient(process.env.MONGO_URL, dbName, emitter)
  emitter.on('mongo-connected', () => {
    $log.info('createChatService')
    // global.$webSocket = new WebSocket(server, emitter, namespace)
    const inv = readFile('./testFolder/invoice1.txt')
    const adminInv = readFile('./testFolder/invoice2.txt')
    console.log('cs3 adminInv = ', adminInv)
    console.log('cs3 adminInv.admin = ', adminInv.admin)
    const request = {
      body: { inv, adminInv }
      // body: { name: 'gia', email: 'info2@deservice.net', subject: 'Lob', contactText: 'testettext' }
    }
    const response = {
      test2: true,
      setHeader: () => {},
      send: (t) => {
        console.log('responce send t = ', t)
      }
    }
    postService('/uploadInvoice', request, response)
  })
}
function readFile(path) {
  try {
    var data = fs.readFileSync(path, 'utf8')
    var obj = eval('(' + data + ')')
    const inv = JSON.parse(JSON.stringify(obj))
    console.log(inv)
    return inv
  } catch (e) {
    console.log('Error:', e.stack)
  }
}
// translateTextArray(['Guten Tag', 'Wie alt bist du?'], 'de', 'ka')
translateText('Wie alt bist du?', 'de', 'ka')
  .then((result) => {
    // { translatedText, translationError }
    console.log('result = ', result)
  })
  .catch(({ translationError }) => {
    $log.info('po translateText error: ' + translationError.errors)
  })
// es folder imageDirBase sheqmnili unda gvqondes. live-shi sheqmna ar aris kargioo
// const imageDirBase = '../cfmedias/' amass sxva modulebs pirdapir exports.imageDirBase-it ver gadavcemt, radgan gviandeba
// rasac klienti ikenebs suratebis gamosazaxeblad -> http://localhost:4129/docs/1599165318843_49098.jpeg
// const imageDirURl = '/docs/'
const mediaDirParams = {
  base: process.env.CFMEDIAS_DIR, // '../cfmedias/', საწყისი საერთო ფოლდერი
  chatDb: 'chats/', // სადაც ეს სერვერი შეინახავს ბაზისთვის -> /chats/1599165318843_49098.jpeg
  chatUrl: '/chats/', // cmedias რასაც კლიენტი გამოიძახებს url-ში -> /chats/1599165318843_49098.jpeg
  questionnaireDb: 'questionnaires/', // სადაც ეს სერვერი შეინახავს ბაზისთვის -> /questionnaires/1599165318843_49098.jpeg
  questionnaireUrl: '/questionnaires/' // qmedias რასაც კლიენტი გამოიძახებს url-ში -> /questionnaires/1599165318843_49098.jpeg
}
global.$mediaDirParams = mediaDirParams
// API_BASE_URL_TEMP droebit aris. unda ikos -> API_BASE_URL
let desbackHost = process.env.API_BASE_URL + process.env.CFBACK_HOST_URL
// process.env.TEMP isedac tavistavad arsebobs -> process.env.TEMP = C:\Users\Andreas\AppData\Local\Temp
/* if (process.env._TEMP && JSON.parse(process.env._TEMP)) {
  desbackHost = process.env.API_BASE_URL_TEMP + process.env.CFBACK_HOST_URL
} */
console.log('desbackHost = ', desbackHost)
const desBackData = {
  host: desbackHost,
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcm9tIjoicWNoYXQifQ.bNS04SOgfLOB7qi3zhAcwOGWQsVsaqgybm3e8N5r9bM'
}
createDirIfNotExists(mediaDirParams.base + mediaDirParams.chatDb, null, (success) => {
  $log.info('cS dir created' + mediaDirParams.base + mediaDirParams.chatDb + ' success = ' + success)
})
createDirIfNotExists(mediaDirParams.base + mediaDirParams.questionnaireDb, null, (success) => {
  $log.info('cS dir created' + mediaDirParams.base + mediaDirParams.questionnaireDb + ' success = ' + success)
})
// serverData-shi sxvadasxva const-ebs chavcert, romlebic momavalshi sheizleba dagvchirdes
// serverData.uploadType = 'stream', 'siofu', 'axios'
const maxFileSize = 2000000
const chunkSize = 1024 * 1000
const serverData = { mediaDirParams, maxFileSize, chunkSize, desBackData }
global.$serverData = serverData
const app = express()
const port = process.env.PORT // 'http://136.243.102.152:4129/chat'

// app.use(makeInjection, express.urlencoded({extended: true}), express.json(), cookieParser())
app.use(express.urlencoded({ extended: true }), express.json())
// eslint-disable-next-line no-unused-vars
const errorHandler = (err, _req, res, _next) => {
  const code = err.status || 400
  res.status(code).json(err)
}

const server = app.listen(port, () => {
  $log.info('cS Server is listening on port ' + port)
})

/* const emitter = new EventEmitter2({
  newListener: false,
  wildcard: true
}) */
// for test only
// postService('/getDataById', { body: { cardId: 5 } }, null)
// jenkinsis .env-shi ceria -> MONGO_URL=mongodb://admin:imereti1@mongo:27017/dbName?authSource=admin
// global.$mongo = new MongoClient(process.env.MONGO_URL, dbName, emitter)
/* emitter.on('mongo-connected', () => {
  $log.info('createChatService')
  global.$webSocket = new WebSocket(server, emitter, namespace)
  createChatService(emitter)
  // postService('/getQValuesByCardId', { body: { cardId: 124 } }, null, emitter, serverData)
}) */
// app.use(cors({credentials: true, origin: true}));
// ar aris mgoni aucilebeli
app.use(cors())
app.use((req, res, next) => {
  $log.info('cS req.url ' + req.url)
  // hh movacilet cin '/qchat'   /qchat/login -> /login
  req.url = req.url.replace('/qchat', '')
  // res.header('Access-Control-Allow-Origin', '*')
  // res.header('Access-Control-Allow-Origin', req.header('origin') )
  // res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, foo')
  next()
})

// chat-istvis modis -> /chats/1599165318843_49098.jpeg
app.get('/chats/*', (req, res) => {
  getAndSendFile(res, req.path)
})
// questionnaire-stvis modis -> /questionnaires/1599165318843_49098.jpeg
app.get('/questionnaires/*', (req, res) => {
  getAndSendFile(res, req.path)
})
// questionnaire-is da chat-is file-is chamotvirtva ->  req.body = {"relPath":"/questionnaires/1611527943463_241760.pdf"}
app.post('/downloadFile', (req, res, next) => {
  getAndSendFile(res, req.body.relPath)
})
function getAndSendFile(res, url) {
  const relPathAndFilename = getRelPathAndFilenameFromMediaUrl(url)
  const relPath = relPathAndFilename.relPath
  const filename = relPathAndFilename.filename
  $log.info('relPath ' + relPath)
  $log.info('filename ' + filename)
  // orive movsinjo
  res.sendFile(filename, { root: relPath })
  // res.download(filePath, fileName)
}
// invoice-is chamotvirtva
app.post('/downloadInvoice', (request, response, next) => {
  $log.info('cs downloadInvoice request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/downloadInvoice', request, response)
})
// invoice-is atvirtva
app.post('/uploadInvoice', (request, response, next) => {
  $log.info('cs uploadInvoice request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/uploadInvoice', request, response)
})
// questionnaireValues gamozaxeba
app.post('/getQValuesByCardId', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/getQValuesByCardId', request, response)
})
// data aris an user an card
app.post('/getDataById', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/getDataById', request, response)
})
app.post('/sendPendingMessage', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/sendPendingMessage', request, response)
})
app.post('/uploadMediaChat', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/uploadMediaChat', request, response)
})
app.post('/uploadMediaQuestionnaire', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/uploadMediaQuestionnaire', request, response)
})
app.post('/uploadQuestionnaire', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  postService('/uploadQuestionnaire', request, response)
})
exports.app = app
