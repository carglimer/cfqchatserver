// const { ObjectId } = require('mongodb')
require('dotenv').config()
const { enums, methods } = require('helpers')
// import { de } from 'desqchat'
const cors = require('cors')
const express = require('express')
const WebSocket = require('./utils/websocket')
const path = require('path')
const bodyParser = require('body-parser')
// import cookieParser from 'cookie-parser'
const EventEmitter2 = require('eventemitter2').EventEmitter2
// import { EventEmitter2 } from 'eventemitter2'
const postService = require('./chat/post')
const createDirIfNotExists = require('./utils/fileUploader').createDirIfNotExists
// import createChatService from './chat'
const createChatService = require('./chat/chat')
const { MongoClient } = require('./utils/MongoClient')
// process.env.NODE_ENV = 'test'
const { logger } = require('./utils/logger')
const { downloadInvoicePdf } = require('./utils/invoice')
// import logger from './utils/logger'
// global.$de = de
global.$log = logger
global.$enums = enums
global.$events = enums.events
global.$caller = enums.caller
global.$etexts = enums.etexts
global.$status = enums.status
global.$errors = enums.errors
global.$intents = enums.intents
global.$typingState = enums.typingState
global.$methods = methods
console.log('JSON.parse(process.env.EXAMPLE_QUESTIONNAIRE_LANGUAGES) = ', JSON.parse(process.env.EXAMPLE_QUESTIONNAIRE_LANGUAGES))
/* const { translateText } = require('./utils/translator')
translateText('Guten Tag', 'de', 'en').then(result => {
  // { translatedText, translationError }
  console.log('result = ', result)
}) */
const dbName = 'desmongo' // socketchat
const namespace = 'chat'
// es folder imageDirBase sheqmnili unda gvqondes. live-shi sheqmna ar aris kargioo
// const imageDirBase = '../cfmedias/' amass sxva modulebs pirdapir exports.imageDirBase-it ver gadavcemt, radgan gviandeba
// rasac klienti ikenebs suratebis gamosazaxeblad -> http://localhost:4129/docs/1599165318843_49098.jpeg
// const imageDirURl = '/docs/'
const mediaDirParams = {
  base: process.env.CFMEDIAS_DIR, // '../cfmedias/', საწყისი საერთო ფოლდერი
  chatDb: 'chats/', // სადაც ეს სერვერი შეინახავს ბაზისთვის -> /chats/1599165318843_49098.jpeg
  chatUrl: '/cmedias/', // რასაც კლიენტი გამოიძახებს url-ში -> /cmedias/1599165318843_49098.jpeg
  questionnaireDb: 'questionnaires/', // სადაც ეს სერვერი შეინახავს ბაზისთვის -> /questionnaires/1599165318843_49098.jpeg
  questionnaireUrl: '/qmedias/' // რასაც კლიენტი გამოიძახებს url-ში -> /qmedias/1599165318843_49098.jpeg
}
// console.log('process.env.MONGO_URL ', process.env.MONGO_URL)
// process.env.DESBACK_HOST = 'localhost' || 'http://136.243.102.152'
// 'localhost:8080/api/qchat/' || 'http://136.243.102.152:8080/api/qchat/'
// const desbackHost = process.env.DESBACK_HOST + ':' + process.env.DESBACK_PORT + process.env.CFBACK_HOST_URL
// const desbackHost = process.env.API_BASE_URL + process.env.CFBACK_HOST_URL
// API_BASE_URL_TEMP droebit aris. unda ikos -> API_BASE_URL
let desbackHost = process.env.API_BASE_URL + process.env.CFBACK_HOST_URL
// process.env.TEMP isedac tavistavad arsebobs -> process.env.TEMP = C:\Users\Andreas\AppData\Local\Temp
/* if (process.env._TEMP & JSON.parse(process.env._TEMP)) {
  desbackHost = process.env.API_BASE_URL_TEMP + process.env.CFBACK_HOST_URL
} */
console.log('desbackHost = ', desbackHost)
const desBackData = {
  host: desbackHost,
  token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcm9tIjoicWNoYXQifQ.bNS04SOgfLOB7qi3zhAcwOGWQsVsaqgybm3e8N5r9bM'
}
createDirIfNotExists(mediaDirParams.base + mediaDirParams.chatDb, null, (success) => {
  $log.info('cS dir created' + mediaDirParams.base + mediaDirParams.chatDb + ' success = ' + success)
})
createDirIfNotExists(mediaDirParams.base + mediaDirParams.questionnaireDb, null, (success) => {
  $log.info('cS dir created' + mediaDirParams.base + mediaDirParams.questionnaireDb + ' success = ' + success)
})
// serverData-shi sxvadasxva const-ebs chavcert, romlebic momavalshi sheizleba dagvchirdes
// serverData.uploadType = 'stream', 'siofu', 'axios'
const maxFileSize = 2000000
const chunkSize = 1024 * 1000
const serverData = { mediaDirParams, maxFileSize, chunkSize, desBackData }
global.$serverData = serverData
const app = express()

const port = process.env.PORT // 'http://136.243.102.152:4129/chat'
const mongoUrl = 'mongodb://admin:imereti1@localhost:27017/dbName?authSource=admin'

/* function makeInjection(req, _res, next) {
  req.errors = []
  req.user = {}
  req.decodedToken = {}
  req.providerCode = ''
  next()
} */

// app.use(makeInjection, express.urlencoded({extended: true}), express.json(), cookieParser())
app.use(express.urlencoded({ extended: true }), express.json())
// eslint-disable-next-line no-unused-vars
const errorHandler = (err, _req, res, _next) => {
  const code = err.status || 400
  res.status(code).json(err)
}
// app.use('/api', api, errorHandler)
// es daemata,radgan https://kfz-soft.com/chat --> http://136.243.102.152:4129/chat/
// app.use(express.static(__dirname + '/chat'))
const server = app.listen(port, () => {
  $log.info('cS Server is listening on port ' + port)
})

const emitter = new EventEmitter2({
  newListener: false,
  wildcard: true
})
/* let t = {ert:ObjectId("518cbb1389da79d3a25453f9")}
console.log('t ', t)  -> { ert: 518cbb1389da79d3a25453f9 }
let t2 = {ert:ObjectId("518cbb1389da79d3a25453f9").toString()}
console.log('t 2', t2)  -> { ert: '518cbb1389da79d3a25453f9' }

let s = {ert:"518cbb1389da79d3a25453f9"}
console.log('s +', s) */
// console.log('ws.socketMaps:', ws.socketMaps)
/*
const message = {
    meta: { collName: 'messages', receiver: { id: '123' } },
    ts: Date.now(),
    data: { textData: { text: 'erti', senderLang: 'de', receiverLang: 'de' } },
  }
  for test only */
// postService('/sendPendingMessage', message, null, emitter, mongo, serverData)

const request = {
  body: { name: 'gia', email: 'gia-lomidze@web.de', subject: 'Lob', contactText: 'testettext' }
}
postService('/postContactRequest', request)
const mongo = new MongoClient(process.env.MONGO_URL, dbName, emitter)
emitter.on('mongo-connected', () => {
  $log.info('createChatService')
  global.$webSocket = new WebSocket(server, emitter, namespace)
  createChatService(emitter)
  // const request = { body: { cardId: 124 } }
  const request = {
    // body: { inv: { cardId: 124, admin: false, price: 37 }, adminInv: { cardId: 124, admin: true, price: 14.3 } }
    body: { name: 'gia', email: 'info2@deservice.net', subject: 'Lob', contactText: 'testettext' }
  }
  const response = { test2: true, setHeader: () => {}, send: () => {} }
  // postService('/uploadInvoice', request, response, mongo, serverData)
  // postService('/postContactRequest', request)
})

function insertOneMessage(message, mongo) {
  /* if (message._id) {
    // mashin ukve chanaceria
    $log.info('c already has _id message = ' + JSON.stringify(message))
    return
  } */
  insertOne(message, mongo)
    .then(
      (insertedId) => {
        if (insertedId) {
          insertedId = insertedId.toString()
          $log.info('...insertedId ' + insertedId)
          // gavagebinet gamomgzavns, rom mivighet message
          message.meta.msgId = insertedId
          console.log('cs2 message', JSON.stringify(message))
        }
      },
      // reject nacilistvis
      (err) => {
        $log.info('insert err: ' + err)
      }
    )
    .catch((err) => $log.info('err2: ' + err.toString()))
}
function insertOne(data, mongo) {
  $log.info('insertOne')
  const coll = (mongo ? mongo : this.mongo).getCollection(data.meta.collName)
  // orjer rom ar chaceros ts unique aris
  // coll.createIndex({ ts: data.ts }, { unique: true })
  // giorgim rom mirchia is varianti async await ar mushaobs
  return new Promise((resolve, reject) => {
    coll.insertOne(data, (err, res) => {
      if (err) {
        $log.info('mongo insertOne err ' + err.toString())
        reject(err)
      } else {
        $log.info('res.insertedId ' + res.insertedId)
        resolve(res.insertedId)
      }
    })
  })
}
// app.use(cors({credentials: true, origin: true}));
// ar aris mgoni aucilebeli
app.use(cors())
app.use((req, res, next) => {
  $log.info('cS req.url ' + req.url)
  // hh movacilet cin '/gqchat'   /gqchat/login -> /login
  req.url = req.url.replace('/gqchat', '')
  // res.header('Access-Control-Allow-Origin', '*')
  // res.header('Access-Control-Allow-Origin', req.header('origin') )
  // res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

// chatistvis modis -> /cmedias/1599165318843_49098.jpeg
/*
const mediaDirParams = {
  base: '../cfmedias/',
  chatDb: 'chats/',
  chatUrl: '/cmedias/',
  questionnaireDb: 'questionnaires/'
  questionnaireUrl: '/qmedias/'
}
*/
app.get('/cmedias/*', (req, res) => {
  // __dirname = C:\Users\Andreas\Desktop\vue\qchatServer
  $log.info('cS req.path ' + req.path)
  // req.path = /cmedias/1599165318843_49098.jpeg
  $log.info('cS req.path ' + req.path)
  // filename = 1599165318843_49098.jpeg
  const chatUrl = mediaDirParams.chatUrl // -> /cmedias/
  const filename = req.path.replace(chatUrl, '')
  $log.info('cS filename ' + filename)
  // reqPath = C:\Users\Andreas\Desktop\vue\deservice\chats
  // const reqPath = path.join(__dirname, imageDirBase)
  const reqPath = mediaDirParams.base + mediaDirParams.chatDb
  $log.info('cS reqPath ' + reqPath)
  // res.sendFile(reqPath + '1599165318843_49098.jpeg')
  // reqPath sheizleba ikos rogorc relative aseve, absolute path
  res.sendFile(filename, { root: reqPath })
})
// questionnaire-stvis modis -> /qmedias/1599165318843_49098.jpeg
app.get('/qmedias/*', (req, res) => {
  const questionnaireUrl = mediaDirParams.questionnaireUrl // -> qmedias/
  const filename = req.path.replace(questionnaireUrl, '')
  $log.info('cS filename ' + filename)
  const reqPath = mediaDirParams.base + mediaDirParams.questionnaireDb
  $log.info('reqPath ' + reqPath)
  res.sendFile(filename, { root: reqPath })
})
// postService('/getDataById', { body: [{ recId: 5 }, { recId: 6 }] }, null)
// questionnaireValues gamozaxeba
/* app.post('/getQValuesByCardId', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/getQValuesByCardId', request, response)
})
// fort test only
postService('/getDataById', { body: { recId: 5 } }, null)
// data aris an user an card
app.post('/getDataById', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/getDataById', request, response)
})
app.post('/sendPendingMessage', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/sendPendingMessage', request, response, mongo, serverData)
})
app.post('/uploadMedia', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/uploadMedia', request, response, mongo, serverData)
})
app.post('/uploadQuestionnaire', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  postService('/uploadQuestionnaire', request, response, mongo, serverData)
}) */
exports.app = app
