// npm testSocket -> qoqavs
const ss = require('socket.io-stream')
const fs = require('fs-extra')
// SocketIOFileUpload mxolod brauzershi mushaobs da aq ar gamova misi shemocemba
// const SocketIOFileUpload = require('socketio-file-upload')

// const { events, status } = require('./utils/enums')
const testUploadFilePath = 'testUpload.jpg'
const initSocket = require('./utils/initSocket').initSocket
const request = require('supertest')
const assert = require('assert')
var app = require('./chatServer').app
// var avatarSrcDefault = '/tfotos/avatar_default/avatar.png'
const testUserId = '5d6ae44c4f4c182674954eeb' // shesacvlelia realurit
const token = '' // shesacvlelia realurit ?
const chatUrl = 'http://localhost:4129' // shesacvlelia realurit -> http://136.243.102.152:4129
const loginUrl = 'http://localhost:4128' // shesacvlelia realurit -> http://136.243.102.152:4128
const avatarSrcDefault = '/avatar_default/avatar.png'

/* message asetia:
  {meta: {type: 'text/file', ext: 'zip',size: '16297', collName: 'messages'},
  data: {author: 'me', text: 'ert', senderLang: 'de', username: 'misha', chatetName:'Herr Fisher', avatarSrc:'http..some.png'},
  meta: {sender: '5d4b41f38b7a432c541ab3b4', msgId: 'afgadfgadfg', receiver: 'admin', status: 'notSent', tsList: {}},
  admin: false}      
*/
let message = {
  meta: { type: 'text', collName: 'messages' },
  data: {
    author: 'me',
    text: 'ert',
    senderLang: 'de',
    username: 'misha',
    chatetName: 'Herr Fisher',
    avatarSrc: 'http..some.png'
  },
  meta: {
    tokenoff: true,
    sender: testUserId,
    msgId: 'afgadfgadfg',
    receiver: 'admin2',
    status: 'notSent',
    tsList: { notSent: Date.now() }
  },
  sender: testUserId,
  receiver: 'admin2',
  admin: false
}
let store = {
  getters: {
    user: {
      _id: testUserId
    },
    isAdmin: false,
    token,
    chatUrl
  }
}
let socket = null
let setSocket = function (sock) {
  socket = sock
}
let payload = {
  socket: null,
  store,
  message,
  setSocket
}
function socketOff(socket) {
  if (socket && socket.connected) {
    socket.disconnect()
    socket = null
  }
}
describe('chatServer Tests', function () {
  it('should check pending massages', function (done) {
    this.timeout(15000) // es mocha-s timeout aris, torem ise 2000-shi amtavrebs
    socketOff(socket)
    initSocket(payload)
    socket.off($enums.events.chatFromBack)
    socket.on($enums.events.chatFromBack, (messages, callback) => {
      // document.getElementById('message').innerHTML = messages;
      console.log('messages=', messages)
      let passing = false
      if (messages) {
        passing = true
      }
      // assert.equal(passing, true)
      assert.equal(!!messages, true)
      socketOff(socket)
      done()
    }) // .finally(done)
  })
  it('should handler message, setSavedStatusAndTsAndRenewToken and get message.meta', function (done) {
    socketOff(socket)
    initSocket(payload)
    socket.off($enums.events.chatToBack)
    socket.emit($enums.events.chatToBack, message, (meta) => {
      // dabrunda bazidan meta ... status:'savedInDb'
      console.log('meta=', meta)
      assert.equal(!!meta, true)
      console.log('socket.connected ', socket.connected)
      socketOff(socket)
      done()
    })
  })
  it('should handler typing message', function (done) {
    let status = 'typing' // onFocus, offFocus
    let typingMessage = {
      meta: { test: true, type: 'typing' },
      meta: { sender: testUserId, receiver: '', status }
    }
    socketOff(socket)
    initSocket(payload)
    socket.off($enums.events.typingToBack)
    socket.emit($enums.events.typingToBack, typingMessage, (meta) => {
      // mxolod test-is shemtxvevashi brundeba bazidan meta ... status:'typing'
      console.log('meta=', meta)
      /* let passing = false
      if (meta) {
        passing = true
      } */
      assert.equal(!!meta, true)
      console.log('socket.connected ', socket.connected)
      socketOff(socket)
      done()
      /*  setTimeout(() => {
        process.exit()
      }, 3000)   */
    })
  })
  it('should upload file-stream to server', function (done) {
    this.timeout(15000)
    let fileMessage = JSON.parse(JSON.stringify(message))
    fileMessage.meta.type = 'file'
    fileMessage.meta.ext = 'jpg'
    fileMessage.meta.size = '1834000'
    /* let fileMessage = { 
      meta: { test: true, type: 'file', ext:'jpg' }, 
      data: { author: 'me', text, file, handleErr, handleProgress, handleSuccess }
    } */
    socketOff(socket)
    initSocket(payload)
    let stream = ss.createStream()
    ss(socket).emit($enums.events.fileStreamToBack, stream, fileMessage, (meta) => {
      console.log('meta ', JSON.stringify(meta))
      assert.equal(meta.status, $enums.status.savedInDb)
      socketOff(socket)
      done()
      /* setTimeout(() => {
        process.exit()
      }, 3000)  */
    })
    fs.createReadStream(testUploadFilePath).pipe(stream)
  })
  it('should download file-stream from server', function (done) {
    /*message ->   
      {
        {meta: {type: 'file', ext: 'png',size: '16297', collName: 'messages'},
        data: {author: null, file: null, text: 'ert', senderLang: 'de', username: 'misha', chatetName:'Herr Fisher', avatarSrc:'http://localhost:4128/tfotos/5d6ae44c4f4c182674954eeb/avatar/1567534881767_16722.png'},
        callbacks: null,
        meta: { token: null, sender: 5d6ae44c4f4c182674954eeb, msgId: 5d6eec696e2bca1aac22145f, receiver: 5d6ae3b14f4c182674954eea, status: received,
            tsList: {notSent: 1567550568768, savedInDb: 1567550569017, received: 1567550569024}, tokenoff: true,
            relpath: '5d6ae44c4f4c182674954eeb/2019-9-4/1567550568768_14285.png' },
        admin: false,
        _id: 5d6eec696e2bca1aac22145f}
      */
    this.timeout(15000)
    let fileMessage = {
      meta: { type: 'file', ext: 'png', size: '1834000', collName: 'messages' },
      data: { author: null, file: null, text: 'ert', senderLang: 'de' },
      callbacks: null,
      meta: {
        token: null,
        sender: '5d6ae44c4f4c182674954eeb',
        msgId: '5d6eec696e2bca1aac22145f',
        receiver: 'admin2',
        status: $enums.status.received,
        tokenoff: true,
        relpath: avatarSrcDefault
      },
      admin: false,
      _id: '5d6eec696e2bca1aac22145f'
    }
    //fileMessage.meta.type = 'file'
    //fileMessage.meta.ext = 'jpg'
    //fileMessage.meta.size = '1834000'
    /* let fileMessage = { 
      meta: { test: true, type: 'file', ext:'jpg' }, 
      data: { author: 'me', text, file, handleErr, handleProgress, handleSuccess }
    } */
    socketOff(socket)
    initSocket(payload)
    let stream = ss.createStream()
    socket.emit($enums.events.downloadFileFromBack, fileMessage, (message) => {
      // dabrunda bazidan {download:true}
      console.log('message=', message)
      ss(socket).on($enums.events.downloadFileFromBack, (stream, message, callback) => {
        console.log('received message', message)
        var binaryString = ''
        stream.on('data', (data) => {
          console.log('data')
          for (var i = 0; i < data.length; i++) {
            binaryString += String.fromCharCode(data[i])
          }
        })
        stream.on('end', (data) => {
          console.log('binaryString.length', binaryString.length)
          assert.notEqual(binaryString.length, 0)
          socketOff(socket)
          done()
          setTimeout(() => {
            process.exit()
          }, 3000)
          binaryString = ''
        })
      })
    })
    // fs.createReadStream(testUploadFilePath).pipe(stream)
  })

  /* 
  // SocketIOFileUpload mxolod brauzershi mushaobs da aq ar gamova misi shemocemba
  
  it("should upload file-siofu to server", function (done) {   
    let fileMessage = JSON.parse(JSON.stringify(message))
    fileMessage.meta.type = 'file'
    fileMessage.meta.ext = 'jpg'
    fileMessage.meta.size = '1834000'
    
    socketOff(socket)
    initSocket(payload)
    console.log('149 SocketIOFileUpload ', SocketIOFileUpload)
    const uploader = new SocketIOFileUpload(socket)
   
    console.log('154 uploader ',uploader)
    var content = fs.readFileSync(testUploadFilePath, 'base64');
    
    uploader.addEventListener("start", event => {
      console.log('start uploader')
      this.socket.emit(events.fileSiofuToBack, message, meta=> {
        console.log('meta ', JSON.stringify(meta))
        assert.equal(meta.status, status.savedInDb)
        socketOff(socket)     
        done()       
      })
    })
    uploader.submitFiles([content])    
  }) */

  /*
  it("should return image file", function (done) {
    request(app)
      .get(avatarSrcDefault)
      .expect('Content-Type', 'image/png')
      .expect(200)
      .end(done)
  });
  */
})
