import fs from 'fs-extra'
import ffmpegStatic from 'ffmpeg-static'
import ffmpeg from 'fluent-ffmpeg'
// api-> https://github.com/fluent-ffmpeg/node-fluent-ffmpeg
ffmpeg.setFfmpegPath(ffmpegStatic)

const dbPath1 = './testFolder/safari_file_audio.mp3'
const dbPath2 = './testFolder/chrome_file_audio.mp3'
const dbPath3 = './testFolder/chromeToSafari_audio_converted.mp3'
const dbPath4 = './testFolder/windows_chrome_video.mp4'
const dbPath5 = './testFolder/video.mp4'
const dbPath6 = './testFolder/video2.mp4'
// თუ იმ სახელის ფაილი დახვდა მაშინ გადააწერს, მაგრამ საწყისი ფაილის სახელს ვერ მიანიჭებს

function processFfmpeg() {
  ffmpeg()
    .input(dbPath5)
    // .input(fs.createReadStream(dbPath5))
    // Audio bit rate
    // .outputOptions('-ab', '192k')
    // Output file
    .saveToFile(dbPath6)
    // Log the percentage of work completed
    .on('progress', (progress) => {
      if (progress.percent) {
        console.log(`Processing: ${Math.floor(progress.percent)}% done`)
      }
    })

    // The callback that is run when FFmpeg is finished
    .on('end', () => {
      console.log('FFmpeg has finished.')
    })

    // The callback that is run when FFmpeg encountered an error
    .on('error', (error) => {
      console.error(error)
    })
}
processFfmpeg()
function processFfmpeg2() {
  ffmpeg()
    // .input('video.mp4')
    .input(dbPath2)
    // Audio bit rate
    // .outputOptions('-ab', '192k')
    // Output file
    .saveToFile(dbPath3)
    // Log the percentage of work completed
    .on('progress', (progress) => {
      if (progress.percent) {
        console.log(`Processing: ${Math.floor(progress.percent)}% done`)
      }
    })

    // The callback that is run when FFmpeg is finished
    .on('end', () => {
      console.log('FFmpeg has finished.')
    })

    // The callback that is run when FFmpeg encountered an error
    .on('error', (error) => {
      console.error(error)
    })
}
/* function processFfmpeg2(dbPath) {
  try {
    const process = new ffmpeg2(dbPath1)
    process.then((audio) => {
      audio.fnExtractSoundToMP3(dbPath2, (error, file) => {
        if (error) {
          console.log('audio error = ', error)
        } else {
          console.log('audio file = ', file)
        }
      })
    })
  } catch (err) {
    console.log('audio err code = ', err.code)
    console.log('audio err msg = ', err.msg)
  }
}
 */
