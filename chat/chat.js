// const { ObjectId } = require('mongodb')
// const axios = require('axios')
// const httpErrs = require('../utils/httpErrors')
// const { handlerFileMessage_Stream } = require('../utils/fileUploader')
// const ss = require('socket.io-stream')
// const { translateText } = require('../utils/translator')
// const { find, updateOne } = require('../utils/MongoClient')
import { ObjectId } from 'mongodb'
import axios from 'axios'
import { handlerFileMessage_Stream } from '../utils/fileUploader'
import ss from 'socket.io-stream'
import { translateText } from '../utils/translator'
import { find, updateOne } from '../utils/MongoClient'

export default function chat(emitter) {
  // serverData.uploadType = 'stream' // 'stream', 'siofu', 'axios'
  // this.serverData = serverData
  // console.log('c connected imageDirBase', serverData.imageDirBase)
  emitter.on($events.connection, (socket) => {
    $log.info('c socket on fn socket.id = ' + socket.id)
    // jer shevamocmot tu aris raime messages mis saxelze
    // checkPendingMassages(socket)
    socket.on($events.chatToBack, handlerMessage()) // chat
    socket.on($events.typingToBack, handlerTyping()) // typing info
    socket.on($events.downloadUserAllMessages, handlerGetUserAllMessages())
    // roca servers vatkobinebt all message-ebis mighebas
    socket.on($events.acknowlidgeServerAboutReadAllUserMessages, handlerAcknowlidgeServerAboutReadAllUserMessages(socket))

    // files
    /* const indexData = {
      socket
    } */
    ss(socket).on($events.fileStreamToBack, handlerFileMessage_Stream()) // file stream

    // console.log('allConnectedClients ', webSocket.getConnectedSocketsKeys())
    socket.on($events.disconnect, () => {
      let userId = socket.handshake.query.userId
      $log.info('c from websocket removed userId = ' + userId)
      delete $webSocket.socketMaps[userId]
      // uploaderSiofu.destroy()
      // uploaderSiofu = null
    })
  })
}
// roca klientma sheatkobina, rom miigho all messages
function handlerAcknowlidgeServerAboutReadAllUserMessages(socket) {
  return (messageList) => {
    $log.info('c messageList.length = ' + messageList.length + ' messageList[0] =' + JSON.stringify(messageList[0]))
    // da chavcerot rogorc receiveris mier mighebuli
    for (const message of messageList) {
      const senderId = message.meta.sender.id
      const cardId = message.meta.sender.cardId
      let senderSocket = $webSocket.getSocketByUserIdAndCardId(senderId, cardId)
      /* if (senderSocket) {
        // console.log('c userIsConnected senderId = ', senderId)
        senderSocket.emit($events.chatNotifFromBack, message.meta)
      } */
      // tu xazzea gaagzavnis, tu ara bazashi darcheba
      updateAndSendMessageStatus({ meta: message.meta, socket: senderSocket })
    }
  }
}
// typing statuss vatkobinebt. bazashi ar vinaxavt, radgan live-shi aqvs azri da callback-ebs ar vaketebt
function handlerTyping() {
  return (message, callback) => {
    $log.info('c handlerTyping message' + JSON.stringify(message))
    if (message.meta.type === 'typing') {
      // update sender-is locale
      if (message.meta.state === $typingState.onFocus) {
        $webSocket.socketMaps[message.meta.sender.id].lang = message.meta.sender.lang
        $log.info('c $webSocket.socketMaps[message.meta.sender.id].lang = ' + $webSocket.socketMaps[message.meta.sender.id].lang)
      }
      if (message.meta.test && callback) {
        callback(message.meta)
        return
      }
      const receiverId = message.meta.receiver.id
      const cardId = message.meta.receiver.cardId
      if ($webSocket.userIsConnected(receiverId, cardId)) {
        let receiverSocket = $webSocket.getSocketByUserIdAndCardId(receiverId, cardId)
        $log.info('c receiverSocket ' + receiverSocket.id)
        receiverSocket.emit($events.typingFromBack, message)
      }
    }
  }
}

function handlerMessage() {
  return (message, callback) => {
    $log.info('c handlerMessage ' + JSON.stringify(message))
    translateAndInsertMessage(message, callback)
  }
}
export async function translateAndInsertMessage(message, callback) {
  if (message.meta.collName === 'messages') {
    // meta:{sender : userId, msgId:'', receiver : '101', status:'notSent', tsList:{}}
    message.meta.status = $status.savedInDb
    setSavedStatusAndTsAndRenewToken(message.meta)
    // $log.info('in wrapper message = ' + JSON.stringify(message))
    const textData = message.data.textData
    // shevamocmot receiver-s xom ar sheucvlia ena
    /* const receiverLang = $webSocket.getReceiverLang(message.meta.receiver.id)
    // console.log('c receiverLang = ', receiverLang)
    if (receiverLang) {
      textData.receiverLang = receiverLang
    } */
    const translate = process.env.TRANSLATE && JSON.parse(process.env.TRANSLATE)
    // if sender and receiver have different languages
    // textData = {senderLang:'de', receiverLang: 'ka', text: 'Haus Tür', translatedText:'სახლი კარი' }
    if (translate && textData && textData.senderLang !== textData.receiverLang) {
      $log.info('c translate textData = ' + JSON.stringify(textData))
      try {
        const result = await translateText(textData.text, textData.senderLang, textData.receiverLang)
        $log.info('c translate result = ' + JSON.stringify(result))
        if (result.translatedText) {
          textData.translatedText = result.translatedText
        }
        if (result.translationError) {
          textData.translationError = result.translationError
        }
        insertOneMessage(message, callback)
      } catch (err) {
        $log.info('c translateAndInsertMessage err = ' + $etexts.translateTextFailed, err.message)
        if (callback) {
          callback({ error: $etexts.translateTextFailed, err: err.toString() })
        }
      }
    } else {
      insertOneMessage(message, callback)
    }
  }
}
async function insertOneMessage(message, callback) {
  if (message._id) {
    // mashin ukve chanaceria
    $log.info('c already has _id message = ' + JSON.stringify(message))
    return
  }
  // radgan ar gvinda ratomghac orjer chaiceros amiton updateOne-s vxmarobt -> upsert=true
  let data = { collName: 'messages' }
  data.query = { ts: message.ts }
  data.update = { $set: message }
  data.options = { upsert: true }
  try {
    const resp = await updateOne(data)
    if (resp) {
      $log.info('c resp = ' + JSON.stringify(resp))
      // insertedId aris null tu update moxda
      $log.info('c insertedId = ' + resp.insertedId)
      // შევატყობინეთ  მურმანს
      acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(message, $caller.message)
      // gavagebinet gamomgzavns, rom mivighet message
      message.meta.msgId = resp.insertedId
      if (callback) {
        callback(message.meta)
      }
      // tu receiver xazzea, mashin gadavugzavnot es message
      sendMessageIfReceiverOnline(message)
    }
  } catch (err) {
    $log.info('c err.toString() = ' + err.toString())
    if (callback) {
      callback(err.toString())
    }
  }
}
// indexData = { socket }
export function sendMessageIfReceiverOnline(message) {
  const senderId = message.meta.sender.id
  const cardId = message.meta.sender.cardId
  const receiverId = message.meta.receiver.id
  // $log.info('c userIsConnected cardId isNumber = ' + (typeof cardId === 'number'))
  // $log.info('c userIsConnected receiverId isNumber = ' + (typeof receiverId === 'number'))
  // if (!socket && $webSocket.socketMaps[senderId]) {
  const senderSocket = $webSocket.getSocketByUserIdAndCardId(senderId, cardId)

  if ($webSocket.userIsConnected(receiverId, cardId)) {
    // $log.info('c receiverSocket cardId = ' + $webSocket.socketMaps[receiverId].cardId)
    const cardId = message.meta.receiver.cardId
    const receiverSocket = $webSocket.getSocketByUserIdAndCardId(receiverId, cardId)
    $log.info('c receiverSocket = ' + receiverSocket.id)
    // const stream = ss.createStream()
    // ss(receiverSocket).emit(events.fileFromBack, stream, message, resp_meta => {
    receiverSocket.emit($events.chatFromBack, [message], (respArr_meta) => {
      // mimghebidan pasuxia, rom miigho meta ...status : status.read
      $log.info('c resp_meta = ' + JSON.stringify(respArr_meta))
      updateAndSendMessageStatus({ meta: respArr_meta[0], socket: senderSocket })
    })
    // stream-is gagzavnas ar vaketebt, mxolod public url
    // fs.createReadStream(this.serverData.imageDirBase + message.meta.relPath).pipe(stream)
    // receiverSocket.emit($events.typingFromBack, message)
  } else {
    // $log.info('c mimghebi xazze ar aris receiverId = ' + receiverId)
    $log.info('c mimghebi xazze ar aris message = ' + JSON.stringify(message))
    // tu mimghebi xazze ar aris, gagzavnis gareshe davimaxsovrot, rac gvaqvs
    updateAndSendMessageStatus({ meta: message.meta })
    // da murmanis servers shevatkobinot push-istvis
  }
  /* try {
    const respData = await acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(message, $caller.message)
    $log.info('c respData = ' + JSON.stringify(respData))
  } catch (error) {
    $log.info('c sendMessageIfReceiverOnline error = ' + error)
  } */
}

// mainBack murmanis serveria. vatkobinebt, rom message an questionnaire movida da mainBack-ma mimghebs unda notificate gauketos
export function acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(payload, caller) {
  if (!payload) {
    return Promise.resolve(null)
  }
  // vekitxebit murmanis servers
  // path: 'https://api.deservice.net/api/qchat/   notifications/chat'
  let subj = 'notifications/chat'
  let body
  let text = ''
  let userId, savedInDb

  if (caller === $caller.message) {
    $log.info('c payload = ' + JSON.stringify(payload))
    subj = 'chat'
    const message = payload
    userId = message.meta.sender.id
    savedInDb = message.meta.tsList.savedInDb
    if (message.data && message.data.textData && message.data.textData.text) {
      text = message.data.textData.text
      /* // push-ში არ გვინდა თარგმანი
      if (message.data.textData.translatedText) {
        text = text + '\n' + message.data.textData.translatedText
      } */
    }
    if (message.meta.type !== 'text') {
      switch (message.meta.type) {
        case 'audio':
          text = $etexts.youHaveAudioDe
          break
        case 'video':
          text = $etexts.youHaveVideoDe
          break
        case 'image':
          text = $etexts.youHaveImageDe
          break
        case 'file':
          text = $etexts.youHaveFileDe
          break

        default:
          break
      }
    }
    body = {
      // type 5 value-თი text, video , audio, image, file
      type: message.meta.type,
      text,
      recId: message.meta.receiver.id,
      role: message.meta.receiver.role,
      orderId: message.meta.receiver.orderId,
      cardId: message.meta.receiver.cardId,
      issuer: { recId: message.meta.sender.id, role: message.meta.sender.role, nickName: message.meta.sender.nickname }
    }
  } else if (caller === $caller.questionnaire) {
    subj = 'questionnaire'
    const questionaireValues = payload
    const filled = questionaireValues.filled
    const lang = questionaireValues.lang
    if (filled) {
      text = lang === 'de' ? $etexts.youHaveQvaluesNotificationDeFilled : $etexts.youHaveQvaluesNotificationEnFilled
    } else {
      text = lang === 'de' ? $etexts.youHaveQvaluesNotificationDeUnfilled : $etexts.youHaveQvaluesNotificationEnUnfilled
    }
    body = {
      text,
      cardId: questionaireValues.cardId,
      uploadId: questionaireValues.uploadId,
      filled,
      issuer: { recId: questionaireValues.checkerId, role: 'checker', nickName: questionaireValues.checkerNickname }
    }
  } else if (caller === $caller.invoice) {
    // payload -> invData = {"cardId":70,"serviceType":"check","main":{"price":50},"issuer":{"recId":41,"role":"checker","nickname":"Marie"},"lang":"de","admin":{"vat":19}}
    text = payload.lang === 'de' ? $etexts.youHaveInvoiceDe : $etexts.youHaveInvoiceEn
    subj = payload.serviceType === 'resell' ? 'resellInvoice' : 'invoice'
    // payload -> invData = {"cardId":101,"serviceType":"resell","main":{"price":"6.00","vat":null},"issuer":{"recId":100,"role":"checker","nickName":"giochecker"},
    // "lang":"de","text":"Eine Rechnung steht für Sie zur Verfügung."}
    body = payload
    body.text = text
    // return Promise.reject('test reject') // <- testistvis
    // return Promise.resolve('Mark as invoiced!') // <- testistvis
  }
  // // https://api.check-first.net/qchat/invoice
  $log.info('c cfBackData.host + subj = ' + global.$cfBackData.host + subj)
  // body = {"cardId":70,"serviceType":"check","main":{"price":50},"issuer":{"recId":41,"role":"checker","nickname":"Marie"},"lang":"de","admin":{"vat":19},"text":"Eine Rechnung steht für Sie zur Verfügung."}

  const postBody = { cardId: body.cardId }
  if (userId) {
    postBody.userId = userId
  }
  if (savedInDb) {
    postBody.savedInDb = savedInDb
  }
  $log.info('c postBody = ' + JSON.stringify(postBody))
  return new Promise(async (resolve, reject) => {
    if ($withoutMurmanBack) {
      const resp = { data: 'withoutMurmanBack' }
      resolve(resp.data)
      return
    }
    // მურმანიმ მარტო { cardId, userId } გამომიგზავნეო
    try {
      const resp = await axios.post(
        global.$cfBackData.host + subj, // https://api.check-first.net/qchat/
        postBody,
        {
          headers: {
            token: global.$cfBackData.token
            // 'Content-Type': 'application/x-www-form-urlencoded'
          }
        }
      )
      // dabrunda murmanis bazidan data, romlis sadme gadagzavna ar gvchirdeba
      // invoice-ის შემთხვევაში -> 'Mark as invoiced and Posted for notification!'
      $log.info('c murman resp.data =' + JSON.stringify(resp.data))
      resolve(resp.data)
    } catch (err) {
      $log.info('c acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire err: ' + err)
      reject(err)
    }
  })
}

export function setSavedStatusAndTsAndRenewToken(meta) {
  // meta.status = $status.savedInDb
  if (meta.tsList) {
    meta.tsList[$status.savedInDb] = Date.now()
  }
  if (meta.token) {
    const newToken = $webSocket.renewToken(meta.token)
    meta.token = newToken
  }
}
// payload -> { meta, socket }
export async function updateAndSendMessageStatus(payload) {
  $log.info('c payload.meta = ' + JSON.stringify(payload.meta))
  let data = { collName: 'messages' }
  // $log.info('updateAndSendMessageStatus data ' + data)
  // let options = {}
  data.query = { _id: ObjectId(payload.meta.msgId) }
  // meta: { sender: userId, id:'', receiver : '101', status:'notSent', tsList:{}}
  data.update = { $set: { meta: payload.meta } }
  data.options = {}
  try {
    const res = await updateOne(data)
    $log.info('updateAndSendMessageStatus res = ' + res)
    if (payload && payload.socket) {
      payload.socket.emit($events.chatNotifFromBack, payload.meta)
    }
  } catch (err) {
    $log.info('c updateAndSendMessageStatus err = ' + err.message)
  }
}

function handlerGetUserAllMessages() {
  return async (message, callback) => {
    $log.info('handlerGetUserAllMessages message' + JSON.stringify(message))
    // aq type text are gamikvirdes, radgan es gagzavnis message aris da koveltivs text aris
    if (message.meta.type === 'text') {
      if (!message.meta.sender || !message.meta.receiver) {
        if (callback) {
          message.meta.error = { text: 'sender or receiver undefined!' }
          callback([message])
        }
        return
      }
      const cardId = message.cardId
      const senderId = message.meta.sender.id
      const receiverId = message.meta.receiver.id
      // let receiverId = receiverSocket.handshake.query.userId
      $log.info('receiverId ' + receiverId)
      // let data = { meta: { collName: 'messages' } }
      let data = { collName: 'messages', query: { cardId } }
      // let options = {}
      // meta:{sender : userId, id:'', receiver : '101', status:'notSent', tsList:{}}
      // options.query = {'meta.receiver': receiverId, 'meta.status': { $ne: status.received } }
      // options.query = {$and:[{'meta.sender': senderId},{'meta.receiver': receiverId}]}
      //options.query = {"$or": [{ $and:[{'meta.sender': senderId},{'meta.receiver': receiverId}] },
      //                         { $and:[{'meta.sender': receiverId},{'meta.receiver': senderId}]}]}
      // options.query = {$and:[{'meta.sender': { $in: [senderId, receiverId] }},{'meta.receiver': { $in: [senderId, receiverId] }}]}
      // options.query = { senderId: { $in: [senderId, receiverId] }, receiverId: { $in: [senderId, receiverId] } }
      // options.query = { cardId }
      // data.options = options
      try {
        const messages = await find(data)
        if (messages) {
          $log.info('handlerGetUserAllMessages find messages.length = ' + messages.length)
          if (callback) {
            callback(messages)
          }
        }
      } catch (err) {
        $log.info('c handlerGetUserAllMessages err = ' + err)
      }
    }
  }
}
// mimghebistvis gankutvnili message, romlebic jer ar misvlia status !== received
//ar vxmarobt, radgan allMessages izaxebs koveli shemosvlisas
function checkPendingMassages(receiverSocket) {
  let receiverId = receiverSocket.handshake.query.userId
  $log.info('receiverId ' + receiverId)
  const data = { collName: 'messages' }
  // let options = {}
  // meta:{sender : userId, id:'', receiver : '101', status:'notSent', tsList:{}}
  data.query = { receiverId: receiverId, 'meta.status': { $ne: $status.received } }
  // data.options = options
  find(data).then((messages) => {
    if (messages && messages.length) {
      $log.info('messages ' + messages)
      // gavugzavnet mimghebs mistvis gankutvnili messages
      receiverSocket.emit($events.chatFromBack, messages, (respArr) => {
        // mimghebidan pasuxia, rom miigho,  array [meta,..]
        $log.info('respArr ' + respArr)
        for (const message of messages) {
          // ObjectId aris da unda gavstringot
          const oid = message._id.toString()
          const resp = respArr.find((item) => item.msgId === oid)
          $log.info('resp ' + JSON.stringify(resp))
          if (!resp) {
            return
          }
          message.meta = resp
          // update da gavugzavnot mimghebs, rom mighebulia misi mighebis dadasturebis message
          updateAndSendMessageStatus({ meta: resp, socket: receiverSocket })
          // tu gamomgzavnebi xazzea, mashin shevakobinot, rom  mati messages mighebulia
          const senderId = message.meta.sender.id
          const cardId = message.meta.sender.cardId
          $log.info('senderId ' + senderId)
          if ($webSocket.userIsConnected(senderId, cardId)) {
            const senderSocket = $webSocket.socketMaps[senderId].socket
            $log.info('senderSocket ' + senderSocket.id)
            // aq calback aghar gvchirdeba, radgan tu axla raime mizezis gamo ver miighebs, shemdeg
            // misi messages gamozaxebisasa mainc sheitkobs
            // tu momavalshi mimghebis mxridan cakitxvis statuss davamatebt, mashin aqac gadasaketebeli iqneba
            senderSocket.emit($events.chatNotifFromBack, resp)
          } else {
            // tu gamomgzavni xazze araa, mashin roca chamotvirtavs mistvis gankutvnil messages, mashin gaigebs
          }
        }
      })
    } else {
      // vugzavnit cariel messages
      receiverSocket.emit($events.chatFromBack, [])
    }
  })
}
