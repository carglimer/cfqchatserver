import axios from 'axios'
// import { ObjectId } from 'mongodb'
import { uploadMediaChatQuestionnaire } from '../utils/fileUploader'
import { getAdminInvoiceNr, setAdminInvoiceNr } from '../utils/invoice/createInvoice'
import { downloadInvoice, uploadInvoice, getInvoiceByCardIdAndServiceType, createInvoice } from '../utils/invoice'
import { validationInfo, uploadQuestionnaire, uploadExampleQuestionnaire, removeQMediaOnMongo, getQValuesByCardId, getQStandsArrForCarlinkOrVin6 } from '../utils/questionnaire'
import { translateText, translateTextArray } from '../utils/translator'
import EmailService from '../utils/email'

// console.log('po translateAndInsertMessage = ', translateAndInsertMessage)
export default function postService(url, request, response) {
  $log.info('po url =' + url)
  switch (url) {
    case '/uploadMediaChat':
      uploadMediaChatQuestionnaire({ request, response })
      break
    case '/uploadMediaQuestionnaire':
      uploadMediaChatQuestionnaire({ request, response })
      break
    case '/validationInfo':
      validationInfo(request.body, response)
      break
    case '/createInvoice':
      createInvoice(request.body, response)
      break
    case '/downloadInvoice':
      downloadInvoice(request.body, response)
      break
    case '/uploadInvoice':
      const adminInv = request.body.adminInv
      $log.info('po adminInv = ' + adminInv)
      if (adminInv) {
        getAdminInvoiceNr(adminInv).then((invoiceNrData) => {
          setAdminInvoiceNr(invoiceNrData).then(() => {
            uploadInvoice(request.body, response, invoiceNrData)
          })
        })
      } else uploadInvoice(request.body, response) // <- aq marto checkerIvoice aris
      break
    case '/getInvoiceByCardIdAndServiceType':
      getInvoiceByCardIdAndServiceType(request.body, response)
      break
    case '/removeQMediaOnMongo':
      removeQMediaOnMongo(request.body, response)
      break
    case '/uploadQuestionnaire':
      uploadQuestionnaire(request.body, response)
      break
    case '/uploadExampleQuestionnaire':
      uploadExampleQuestionnaire(request.body, response)
      break
    case '/getQValuesByCardId':
      getQValuesByCardId(request.body, response)
      break
    case '/getQStandsArrForCarlinkOrVin6':
      getQStandsArrForCarlinkOrVin6(request.body, response)
      break
    case '/getDataById':
      // request.body = { userId || cardId }
      getDataById(request.body, response)
      break
    case '/postContactRequest':
      postContactRequest(request.body, response)
      break
    case '/translateText':
      translateTextAndSend(request.body, response)
      break
  }
  // $log.info('po requestBody ' + requestBody)
}

function translateTextAndSend({ text, from, to }, response) {
  // requestBody = { text, from, to }
  $log.info('po requestBody: ' + JSON.stringify({ text, from, to }))
  const translate = Array.isArray(text) ? translateTextArray : translateText
  try {
    const translatedText = translate(text, from, to)
    response && response.send(translatedText)
  } catch ({ translationError }) {
    $log.info('po translateText error: ' + translationError.toString())
    response && response.send({ error: $etexts.translateTextFailed, err: translationError.toString() })
  }
  /* translate(text, from, to)
    .then(({ translatedText }) => {
      // $log.info('po translatedText = ' + translatedText)
      response && response.send(translatedText)
    })
    .catch(({ translationError }) => {
      $log.info('po translateText error: ' + translationError.toString())
      response && response.send({ error: $etexts.translateTextFailed, err: translationError.toString() })
    }) */
}
// contactRequest = { name, email, subject, contactText }
function postContactRequest(requestBody, response) {
  // info@deservice.net
  // const CONTACT_EMAIL = process.env.CONTACT_EMAIL
  // const CONTACT_EMAIL = 'gia-lomidze@web.de'
  // const CONTACT_EMAIL = 'gialomidze62@gmail.com'
  const CONTACT_EMAIL = 'info@deservice.net'
  const contactRequest = requestBody
  // unda gadavagzavnot info@deservice.net-ze

  // const EmailService = require('../utils/email')
  const email = new EmailService() // senderEmail = 'carglimer@web.de'
  const subject = 'Contact request from user of deservice.net'
  // eslint-disable-next-line prettier/prettier
  const html = 'name: ' + contactRequest.name + '<br/>' + ' email: ' + contactRequest.email + '<br/>' + ' subject: ' + contactRequest.subject + '<br/>' + ' Text: ' + contactRequest.contactText
  const body = { to: CONTACT_EMAIL, subject, html }
  email
    .sendEmailHTML(body)
    .then(() => {
      $log.info('po Email sent: ')
      response && response.send('email succesfully sent')
    })
    .catch((err) => {
      $log.info('po send mail error ' + err)
      response && response.send('em send mail error ' + err)
    })
}

// const host = 'http://136.243.102.152:8080/api/qchat/'
// const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmcm9tIjoicWNoYXQifQ.bNS04SOgfLOB7qi3zhAcwOGWQsVsaqgybm3e8N5r9bM'
async function getDataById(requestBody, response) {
  // vekitxebit murmanis servers
  // cardId: [ 12, 14 ] is Array
  let subj
  let body
  if (requestBody.userId) {
    subj = 'users'
    body = { recId: requestBody.userId }
  } else if (requestBody.cardId) {
    subj = 'cards'
    body = { recId: requestBody.cardId } // card/details <- ამოვიღეთ, რადგან აღარ ვანსხვავებთ card და card/details
  } else if (requestBody.resellCardId) {
    subj = 'resellCards'
    body = { recId: requestBody.resellCardId }
  }
  // body = { recId: 5 } an { recId: [5, 6] }
  // https://api.check-first.net/api/qchat/cards
  $log.info('po cfBackData.host + subj =' + global.$cfBackData.host + subj)
  // $log.info('po body =' + JSON.stringify(body))
  try {
    const resp = await axios.post(global.$cfBackData.host + subj, body, {
      headers: {
        token: global.$cfBackData.token
        // Content-Type-ma aq macvala. ase token-s ver cnobs
        // 'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    // dabrunda murmanis bazidan data
    const data = resp.data
    $log.info('po data =' + JSON.stringify(data))
    response && response.send(data)
  } catch (err) {
    $log.info('getDataById po error: ' + err)
    response && response.send({ error: $etexts.getDataByIdFailed, err: err.toString() })
  }
}

/* function updateUserById(payload) {
  const user = payload.user
  $log.info('po user.meta= ' + user.meta)
  const coll = $mongo.getCollection(user.meta.collName)
  const query = { _id: ObjectId(user._id) }
  const setPart = { $set: { chatData: user.chatData } }
  return new Promise((resolve, reject) => {
    coll
      .update(query, setPart)
      // returns-> WriteResult({"nMatched":1,"nUpserted":1,"nModified":1})
      .then((writeResult) => {
        $log.info('po writeResult.result' + writeResult.result)
        if (payload.response) {
          payload.response.send(adjustUser({ result: writeResult.result }, user))
        }
        resolve(true)
      })
      .catch((err) => {
        $log.info('po Failed to find user:' + err)
        reject(err)
      })
  }).catch(() => {
    $log.info('po catch updateUserById UnhandledPromiseRejectionWarning')
  })
}
 */
/*
 function getLinkId(link) {
  let linkId
  if (!link) {
    return linkId
  }
  // ჯერ 3 ვარიანტი გვაქვს ebay-kleinanzeigen, mobile.de, autoscout24.de
  const patternEbayKleinanzeigen = /http[s]?.*?ebay-kleinanzeigen.*?(\d{10})/ // https://www.ebay-kleinanzeigen.de/s-anzeige/skoda-fabia-combi-ambition-ahk-tempomat-euro-6/1843952337-216-2452
  const patternMobile = /http[s]?.*?mobile.de.*?id=(\d{9})/ // https://suchen.mobile.de/fahrzeuge/details.html?id=328942887&damageUnrepaired=ALSO_DAMAGE_UNREPAIRED&isSearchRequest=true&pageNumber=1&scopeId=C&sfmr=false&searchId=742b336d-ef15-5819-a108-283ce6e8f36c
  const patternAutoscout = /http[s]?.*?autoscout24.*?searchId=(\d{10})/ // https://www.autoscout24.de/angebote/ford-transit-kombi-ft-300-l-1-hand-klima-rollstuhl-diesel-weiss-7eedf472-dec1-48c8-b147-6f4afb930624?&cldtidx=7&cldtsrc=listPage&searchId=1239112503
  if (link.match('ebay-kleinanzeigen')) {
    const res = patternEbayKleinanzeigen.exec(link)
    if (res && res[1]) {
      linkId = res[1]
    }
  } else if (link.match('mobile.de')) {
    const res = patternMobile.exec(link)
    if (res && res[1]) {
      linkId = res[1]
    }
  } else if (link.match('autoscout24')) {
    const res = patternAutoscout.exec(link)
    if (res && res[1]) {
      linkId = res[1]
    }
  }
  return linkId
}


 // request = request.body
function handleDownloadQFile(requestBody, response) {
  // /questionnaires/1611435327630_22566.txt
  let relPath = requestBody // invoiceData = { cardId: '12365', lang: 'de'}
  $log.info('po invoiceData ' + JSON.stringify(invoiceData))
  let filePath = '/my/file/path/...' // Or format the path using the `id` rest param
  let fileName = 'report.pdf' // The default name the browser will use

  res.download(filePath, fileName)
}
if (url === '/uploadMediaChat') {
    // $log.info('po uploadMedia request.body.cardId ' + request.body.cardId)
    // console.log('request.body ', JSON.stringify(request.body))
    indexData.request = request
    indexData.response = response
    uploadMediaChat(indexData)
  } else if (url === '/removeQMediaOnMongo') {
    // $log.info('po uploadMedia request.body.cardId ' + request.body.cardId)
    // console.log('request.body ', JSON.stringify(request.body))
    removeQMediaOnMongo(request.body, response)
  } else if (url === '/uploadMediaQuestionnaire') {
    // $log.info('po uploadMedia request.body.cardId ' + request.body.cardId)
    // console.log('request.body ', JSON.stringify(request.body))
    uploadMediaQuestionnaire({ request, response })
  } else if (url === '/downloadInvoice') {
    downloadInvoice(request.body, response)
  } else if (url === '/uploadInvoice') {
    // request.body = { inv, adminInv }
    const adminInv = request.body.adminInv
    if (adminInv) {
      getAdminInvoiceNr(adminInv).then((invoiceNrData) => {
        setAdminInvoiceNr(invoiceNrData).then(() => {
          uploadInvoice(request.body, response, invoiceNrData)
        })
      })
    } // else uploadInvoice(request.body, response) <- aq orive invoice ertad aris, amitom es agar gvchirdeba
  } else if (url === '/uploadQuestionnaire') {
    uploadQuestionnaire(request.body, response)
  } else if (url === '/uploadExampleQuestionnaire') {
    uploadExampleQuestionnaire(request.body, response)
  } else if (url === '/getDataById') {
    // request.body = { userId || cardId }
    getDataById(request.body, response)
  } else if (url === '/getQValuesByCardId') {
    getQValuesByCardId(request.body, response)
  } else if (url === '/getInvoiceByCardId') {
    getInvoiceByCardId(request.body, response)
  } else if (url === '/postContactRequest') {
    postContactRequest(request.body, response)
  } else if (url === '/translateText') {
    translateTextAndSend(request.body, response)
  } */
/*   uploadInvoice-ში იყო
 updateOne(data)
    .then((resp) => {
      $log.info('po resp ' + JSON.stringify(resp))
      // const questionnaireValues = res.ops[0]
      if (resp) {
        if (!resp.update) {
          $log.info('po resp.update ' + resp.update)
          // mashin insert moxda
          invoice._id = resp.insertedId
        }
        $log.info('po invoice._id ' + invoice._id)
        // satestod
         if (true) {
          response.send(resp)
          return
        }
        // gavagebinet mainBack-s rom invoice atvirtulia
        acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(invoice, $caller.invoice)
          .then(({ cardId }) => {
            $log.info('po acknowlidged MainBack cardId ' + cardId)
            // mivighet murmanisgan, rom chacera bazashi es invoice
            response.send(resp) // resp = { update: false||true, insertedId: upsertedId||null }
          })
          .catch((err) => {
            $log.info('p acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire error: ' + err)
            response.send({ error: $etexts.uploadInvoiceFailed, err: err.toString() })
          })
      } else {
        response.send({ error: $etexts.uploadInvoiceFailed })
      }
    })
    .catch((err) => {
      $log.info('po err: ' + err.toString())
      response.send({ error: $etexts.uploadInvoiceFailed, err: err.toString() })
    }) */
/* // $log.info('po questionnaireValues ' + JSON.stringify(questionnaireValues))
  if (questionnaireValues && questionnaireValues.toRemove && questionnaireValues.toRemove.length) {
    // mashin gvaqvs casashleli failebis array relPath-ebit
    for (const _relPath of questionnaireValues.toRemove) {
      const relPathAndFilename = getRelPathAndFilenameFromMediaUrl(_relPath)
      const relPath = relPathAndFilename.relPath
      const filename = relPathAndFilename.filename
      deleteFileIfExists(relPath + filename)
    }
  }
  let data = { collName: 'questionnaires' }
  let vin6 = null
  if (questionnaireValues.vin) {
    vin6 = questionnaireValues.vin.substring(questionnaireValues.vin.length - 6, questionnaireValues.vin.length)
  }
  data.query = { cardId: questionnaireValues.cardId }
  data.update = {
    $set: {
      // pirveli 2 property 1 doneze gamovitane,rata mosazebnad advili ikos
      cardId: questionnaireValues.cardId,
      vin6,
      vin17: questionnaireValues.vin,
      link: questionnaireValues.link
    },
    $push: {
      q: {
        $each: [questionnaireValues],
        $position: 0
      }
    } //,
    // $pull: { q: { $position: { $gt: 0 } } }
  }
  data.options = { upsert: true }
  // meta: { sender: userId, id:'', receiver : '101', status:'notSent', tsList:{}}
  updateOne(data)
    .then((res) => {
      $log.info('po res ' + JSON.stringify(res))
      // const questionnaireValues = res.ops[0]
      if (response) {
        if (!res.update) {
          // mashin insert moxda
          questionnaireValues._id = res.insertedId
        }
        response.send({ questionnaireId: questionnaireValues._id })
        // gavagebinet mainBack-s rom questionnaire atvirtulia da orderers sheatkobinos notification-it
        acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(questionnaireValues, $caller.questionnaire)
      }
    })
    .catch((err) => {
      $log.info('po err: ' + err.toString())
      response.send({ error: $etexts.uploadQuestionnaireFailed, err: err.toString() })
    }) */
