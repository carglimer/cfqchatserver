const { ObjectId } = require('mongodb')
const SHA256 = require('crypto-js/sha256')
const cryptoRandomString = require('crypto-random-string')
const { createJWT, verifyJWT, verifyAsyncJWT } = require('../utils/jwt')
const expire = 60 * 60
const expire_admin = 24 * 60 * 60
const httpErr = require('../utils/httpErrors')
// const { $log } = require('../utils/$log')
const decodeJWT = require('../utils/jwt').decodeJWT
const handleLoadAllItems = require('../utils/admin').handleLoadAllItems
const handleLoadUserAllOrders = require('../utils/admin').handleLoadUserAllOrders
// const uploadQuestionnaireFile_Multer = require('../utils/fileUploader').uploadQuestionnaireFile_Multer
/**
 * The function creates chatting opportunities
 *
 * @param {class} webSocket - The websokcet class instance
 */
module.exports = function loginService(url, request, response, emitter) {
  this.admin_pattern = require('../chatAuth').admin_pattern
  $log.info('this.admin_pattern ' + this.admin_pattern)
  this.emitter = emitter
  // this.serverData = serverData

  if (url === '/signup') {
    handleSignupData(request, response)
  } else if (url === '/login') {
    handleLoginData(request, response)
  } else if (url === '/updateUser') {
    let user = request
    let token = user.tokenInfo.token
    $log.info('lo user.data.admin ' + user.data.admin)
    let admin = user.data.admin === 'true'
    $log.info('lo admin ' + admin)
    let userId = user._id
    let result = checkToken({ token, admin, userId })
    $log.info('lo  result ' + JSON.stringify(result))
    if (result.result) {
      handleUpdateUser(request, response)
    } else {
      response.send({ result: 'token_error', errTxt: result.errTxt })
    }
  } else if (url === '/loadAllItems') {
    handleLoadAllItems({ message: request, response, find })
  } else if (url === '/loadUserOrders') {
    handleLoadUserAllOrders({ message: request, response, find })
  } /* else if (url === '/uploadFoto') {
    uploadQuestionnaireFile_Multer({ request, response })
  }  */ else if (url === '/saveOrder') {
    handleSaveOrder({ request, response })
  }
  $log.info('lo connected ')
}

function handleSaveOrder(payload) {
  let order = payload.request
  $log.info('lo order ' + JSON.stringify(order))
  order.meta = {}
  order.meta.collName = 'orders'
  order.meta.ts = Date.now()
  insertOne(order).then((order) => {
    // ვუგზავნით jwt token-ს
    $log.info('lo order ' + JSON.stringify(order))
    if (payload.response) {
      payload.response.send({ result: 'order_created', order })
    }
  })
}
function updateUserById(payload) {
  const user = payload.user
  $log.info('lo user.meta= ' + user.meta)
  const coll = $mongo.getCollection(user.meta.collName)
  const query = { _id: ObjectId(user._id) }
  const setPart = { $set: { chatData: user.chatData } }
  return new Promise((resolve, reject) => {
    coll
      .update(query, setPart)
      // returns-> WriteResult({"nMatched":1,"nUpserted":1,"nModified":1})
      .then((writeResult) => {
        $log.info('lo writeResult.result' + writeResult.result)
        if (payload.response) {
          payload.response.send(adjustUser({ result: writeResult.result }, user))
        }
        return resolve(true)
      })
      .catch((err) => {
        $log.error('lo Failed to find user:')
        return reject(err)
      })
  }).catch(() => {
    $log.info('lo catch updateUserById UnhandledPromiseRejectionWarning')
  })
}
// meta :{ collName:'users', type:'text',intent:'signup', ts:Date.now()},
// data : {originLang:'de', username: 'user1', email: 'test@web.de', password: 'paroli'}
function handleLoginData(request, response) {
  let user = request
  //return (user, response) => {
  $log.info('lo handleLoginData ' + user)
  if (user.meta.collName === 'users') {
    if (user.meta.intent === 'login') {
      const coll = $mongo.getCollection(user.meta.collName)
      const query = { 'data.email': user.data.email }
      coll
        .find(query)
        // .sort({ name: 1 })
        .toArray()
        .then((items) => {
          //console.log(`Successfully found ${items.length} users.`)
          if (items && items.length) {
            $log.info('lo items ' + items)
            let savedUser = items[0]
            // shevamocmot paroli da username
            if (!checkCredentials(user, savedUser)) {
              if (response) {
                // 'Username or password is incorrect'
                response.status(httpErr.authenticationError.status).send(httpErr.authenticationError)
              }
              return
            }
            if (response) {
              // es normaluri pasuxia
              response.send(adjustUserAndCreateJwt({ result: 'user_returned' }, savedUser))
            }
          } else {
            if (response) {
              response.status(httpErr.noAccountError.status).send(httpErr.noAccountError)
            }
          }
        })
        .catch((err) => {
          $log.error('lo Failed to find user:' + err)
          response.send(err)
        })
    }
  }
  //}
}
// meta :{ collName:'users', type:'text',intent:'signup', ts:Date.now()},
// data : {originLang:'de', username: 'user1', email: 'test@web.de', password: 'paroli'}
// chatData : { chatetName:'Herr Fischer', suggestions: ['erti','ori','sami'...]}
function handleUpdateUser(request, response) {
  const user = request
  $log.info('lo handleUpdateUser ' + user)
  if (user.meta.collName === 'users') {
    if (user.meta.intent === 'updateUser') {
      updateUserById({ user, response })
    }
  }
}
// meta :{ collName:'users', type:'text',intent:'signup', ts:Date.now()},
// data : {originLang:'de', username: 'user1', email: 'test@web.de', password: 'paroli'}
function handleSignupData(request, response) {
  const user = request
  $log.info('lo handleSignupData ' + user)
  if (user.meta.collName === 'users') {
    if (user.meta.intent === 'signup') {
      validateSignupData({ user, response })
      // mere usmens -> createUser()
    }
  }
}
function validateSignupData(payload) {
  $log.info('lo payload.user 1')
  findUserByMail(payload.user).then((items) => {
    // console.log('items.length ', items.length)
    if (items && !items.length) {
      $log.info('lo signup_true ')
      // chatAuth.test.js-istvis
      if (payload.user.meta.test) {
        if (payload.response) {
          payload.response.send(adjustUserAndCreateJwt({ result: 'user_created' }, payload.user))
        }
      } else {
        createUser(payload)
      }
    } else {
      $log.info('lo signup_false ')
      // emitter.emit('signup_false', payload)
      if (payload.response) {
        // payload.response.send(httpErr.signupError)
        payload.response.status(httpErr.signupError.status).send(httpErr.signupError)
      }
    }
  })
}
function checkCredentials(user, savedUser) {
  $log.info('lo savedUser ' + JSON.stringify(savedUser))
  const newHash = '' + SHA256(savedUser.data.salt + user.data.password)
  const usernameOk = user.data.username === savedUser.data.username
  const passwordOk = newHash === savedUser.data.password
  $log.info('lo usernameOk ' + usernameOk)
  return usernameOk && passwordOk
}
function createHashPassword(payload) {
  const salt = cryptoRandomString({ length: 10, characters: '1234567890' })
  const passw = payload.user.data.password
  const hash = '' + SHA256(salt + passw)
  payload.user.data.password = hash
  payload.user.data.salt = salt
}
function adjustUserAndCreateJwt(result, user) {
  // state -> user_created, user_returned
  let expiresIn = expire
  if (user.data.admin) {
    expiresIn = expire_admin
  }
  $log.info('lo expiresIn ' + expiresIn)
  const token = createJWT({ userId: user._id }, { expiresIn })
  // this.serverData aq vacvdit klientisatvis sachiro sxvafsxva informaciebs, misamartebs
  const tokenInfo = { expiresIn, token }
  user.tokenInfo = tokenInfo
  // renewToken(token)
  // return {user, tokenInfo}
  // return { [state]: true, user, tokenInfo }  /
  // return { ...adjustUser(result, user), serverData: $serverData }
  return { serverData: $serverData }
}
function adjustUser(result, user) {
  // state -> user_created, user_returned, user_updated
  // gavstringot _id
  user._id = user._id.toString()
  // movacilet paroli
  // const { password, ...userDataWithoutPassword } = user.data
  const { password, userDataWithoutPassword } = user.data
  user.data = userDataWithoutPassword
  // return {user_updated, user}
  // return { ...result, user }
  return { result, user }
}
/* // chatServer ikenebs
function renewToken(token) {  
  try {      
    const payload = verifyJWT(token)   
    const userId = payload.userId    
    // payload.exp-payload.iat aris expiresIn
    console.log('payload.userId:', payload.userId)    
    const newToken = createJWT({ userId}, { expiresIn: payload.exp-payload.iat})
    return  newToken  
  } catch (err) {
    console.log('renewToken error', err)    
  }
} */

function createUser(payload) {
  createHashPassword(payload)
  // aq vadgent user admin-ia tu ara, anu mail tu sheicavs admin_pattern-s
  if (payload && payload.user && payload.user.data && payload.user.data.email && payload.user.data.email.includes(this.admin_pattern)) {
    payload.user.data.admin = true
    $log.info('lo payload.user.data ' + JSON.stringify(payload.user.data))
  }
  insertOne(payload.user).then((user) => {
    // ვუგზავნით jwt token-ს
    $log.info('lo user ' + JSON.stringify(user))
    if (payload.response) {
      payload.response.send(adjustUserAndCreateJwt({ result: 'user_created' }, user))
    }
  })
}

function insertOne(user) {
  $log.info('lo saveIntoDB')
  const coll = $mongo.getCollection(user.meta.collName)
  // giorgim rom mirchia is varianti async await, ar mushaobs
  return new Promise((resolve, reject) => {
    coll.insertOne(user, (err, res) => {
      if (err) {
        reject(err)
      } else {
        $log.info('lo res.insertedId ' + res.insertedId)
        let user = res.ops[0]
        resolve(user)
      }
    })
  })
}
function findUserByMail(user) {
  // options = {collectionName:{'collectionName'}, query:{...}, sort:{...}, projection : { "_id": 0, name:1 რომელი ველები გვინდა-1 თუ არა-0}}
  $log.info('lo user.meta= ' + user.meta)
  const coll = $mongo.getCollection(user.meta.collName)
  const query = { 'data.email': user.data.email }
  return new Promise((resolve, reject) => {
    coll
      .find(query)
      // .sort({ name: 1 })
      .toArray()
      .then((items) => {
        $log.info('lo Successfully found ${items.length} users ' + items.length)
        resolve(items)
      })
      .catch((err) => {
        $log.info('lo Failed to find user:')
        reject(err)
      })
  }).catch(() => {
    $log.info('lo catch findUserByMail UnhandledPromiseRejectionWarning')
  })
}
function find(data) {
  // options = {collectionName:{'collectionName'}, query:{...}, sort:{...}, projection : { "id": 0, name:1 romeli velebi gvinda-1 tu ara-0}}
  $log.info('lo data.options= ' + data.options)
  const coll = $mongo.getCollection(data.meta.collName)
  return new Promise((resolve, reject) => {
    coll
      .find(data.options.query, data.options.projection)
      // .sort({ name: 1 })
      .toArray()
      .then((items) => {
        if (items.length) {
          $log.info('lo Successfully found ${items.length} documents ' + items.length)
        } else {
          $log.info('lo found no documents.')
        }
        //items.forEach(console.log)
        resolve(items)
      })
      .catch((err) => {
        console.error(`Failed to update document: ${err}`)
        reject(err)
      })
  }).catch(() => {
    $log.info('lo catch find UnhandledPromiseRejectionWarning')
  })
}
function checkToken(payload) {
  /*message asetia: relpath statusInfo-shi chaceris shemdeg daemateba tu file aris
    {
      meta: {type: 'text/file', ext: 'zip',size: '16297', collName: 'messages', tsFormat: 'DD.MM.YY HH:mm'},
      data: {author: 'me', text: 'ert', senderLang: 'de', username: 'misha', chatetName:'Herr Fisher', avatarSrc:'http..some.png'},
      statusInfo: {sender: '5d4b41f38b7a432c541ab3b4', msgId: 'afgadfgadfg', receiver: 'admin', status: 'notSent', tsList: {}},
      admin: false,
      tokenoff: false,
      sender: '5d4b41f38b7a432c541ab3b4',
      receiver: '5d4b41f38b7a432c541ölkökl',
      token: 'sadgagadgdfg'
    }
  */
  const token = payload.token
  const admin = payload.admin // === 'true'
  const tokenoff = payload.tokenoff // === 'true'
  const userId = payload.userId
  // admin = !!admin // if null, then false
  $log.info('lo data ->' + { token, admin, userId })
  //console.log('socket.handshake.query ->', socket.handshake.query)
  if (tokenoff) {
    return { result: true }
  }
  try {
    const decoded = verifyJWT(token)
    $log.info('lo into websocket.js! decoded.userId:' + decoded.userId)
    // socketMaps[socket.id] = socket
    // this.socketMaps[decoded.userId] = { socket, admin, busy: false }
    // console.log('socket.id:', socket.id)
    return { result: true }
  } catch (err) {
    $log.error('lo error tokenError' + err.message)
    // socket.emit('tokenError', httpErrs.incorrectTokenContentError)
    // const httpErr = httpErrs.incorrectTokenContentError({name: err.name, message: err.message})
    // const error = new Error(httpErr)
    // console.log('112 error ', JSON.stringify(error))
    // console.log('113 error ', error)
    // {"name":"token","category":"content","text":{"name":"TokenExpiredError","message":"jwt expired"},"status":400}
    let decoded = decodeJWT(token)
    const expiresAt = new Date(decoded.exp * 1000).toLocaleString()
    const errTxt = 'Error: token is expired at ' + expiresAt
    // console.log('expires at ', new Date(decoded.exp*1000).toLocaleString())
    $log.error('lo errTxt ' + errTxt)
    // next(httpErr)
    return { result: false, errTxt }
  }
}
