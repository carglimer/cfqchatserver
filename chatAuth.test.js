// npm testAuth -> qoqavs
const request = require('supertest')
const assert = require('assert')

var app = require('./chatAuth').app
// var imageSrcBase = 'http://localhost:4128/tfotos/'
var avatarSrcDefault = '/tfotos/avatar_default/avatar.png'
const testUserId = '5d6ae44c4f4c182674954eeb' // shesacvlelia realurit
/*
 login-ze  gaigzavna -> 
{"meta":{"collName":"users","type":"text","intent":"login","ts":1568142794740}
,"data":{"username":"ert","email":"gia-lomidze@web.de","password":"aaaaaa","name":"Fisher","salutation":"Herr","tel":"017794654678","whatsapp":"0177946522"},
"chatData":{"senderLang":"de"}}

  dabrunda bazidan { 'user_returned': true, user, tokenInfo }
  {"result":"user_returned",
  "user":{"_id":"5d6ae3b14f4c182674954eea",
  "meta":{"collName":"users","type":"text","intent":"signup","ts":1567286192871},
  "data":{"username":"ert","email":"gia-lomidze@web.de","name":"Fisher","salutation":"Herr","tel":"017794654678","whatsapp":"0177946522","salt":"9879504383"},
  "chatData":{"senderLang":"de","suggestions":["Guten Tag, was kann ich für Sie tun ?"],"avatarSrc":"http://localhost:4128/tfotos/5d6ae3b14f4c182674954eea/avatar/1567600555465_14285.png","chatetName":"Herr Lomidze ori"}},
  "tokenInfo":{"expiresIn":3600,"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZDZhZTNiMTRmNGMxODI2NzQ5NTRlZWEiLCJpYXQiOjE1NjgxNDE1OTYsImV4cCI6MTU2ODE0NTE5Nn0.4EDcPswtDG6dlH0xQ1SMFzGOGDmO6ECfVRJX6kRvmxQ","serverData":{"fileSendMode":"siofu","chatUrl":"http://localhost:4129","imageSrcBase":"http://localhost:4128/tfotos/","avatarSrcDefault":"http://localhost:4128/avatar_default/avatar.png","messageTsFormat":"HH:mm","uploadFileMaxSize":2,"uploadFileChunkSize":1024000,"fotosUploadUrl":"http://localhost:4128/uploadFoto","flagsImageBase":"http://localhost:4128/flags/"}}}
*/
describe('chatAuth Tests', function () {
  it('should return image file', function (done) {
    request(app).get(avatarSrcDefault).expect('Content-Type', 'image/png').expect(200).end(done)
  })
  it('should login and return user object', function (done) {
    this.timeout(15000)
    request(app)
      .post('/login')
      .send({
        meta: { test: true, collName: 'users', type: 'text', intent: 'login', ts: 1568142794740 },
        data: {
          username: 'ert',
          email: 'test1@web.de',
          password: 'aaaaaa',
          name: 'Fisher',
          salutation: 'Herr',
          tel: '017794654678',
          whatsapp: '0177946522'
        },
        chatData: { senderLang: 'de' }
      })
      .expect((res) => {
        console.log('res.body ', res.body)
        assert.equal(res.body.hasOwnProperty('result') || res.body.text.includes('password is incorrect'), true) // "result":"user_returned"
      })
      .end(done)
  })

  it('should signup and return user object', function (done) {
    request(app)
      .post('/signup')
      .send({
        _id: 'ar_aqvs_mnishvneloba_ra_ceria', // sinamdvileshi ar vqmnit users
        meta: { test: true, collName: 'users', type: 'text', intent: 'signup', ts: 1568142794740 },
        data: {
          username: 'ert',
          email: 'test132456@web.de',
          password: 'aaaaaa',
          name: 'Fisher',
          salutation: 'Herr',
          tel: '017794654678',
          whatsapp: '0177946522'
        },
        chatData: { senderLang: 'de' }
      })
      // .expect('Content-Type', 'image/png')
      /* .end(function(err, res) {        
        console.log('res.body ', res.body)
        res.body.hasOwnProperty ("result") // "result":"user_created"
        done() */
      .expect((res) => {
        // console.log('res.body ', res.body)
        assert.equal(res.body.hasOwnProperty('result'), true) // "result":"user_created"
      })
      .end(done)
  })
  it('should update user object', function (done) {
    request(app)
      .post('/updateUser')
      .send({
        _id: testUserId, // es mere shesacvleli iqneba test user-is _id-it
        meta: { test: true, collName: 'users', type: 'text', intent: 'updateUser', ts: 1568142794740 },
        data: {
          username: 'ert',
          email: 'test1@web.de',
          password: 'aaaaaa',
          name: 'Fisher',
          salutation: 'Herr',
          tel: '017794654678',
          whatsapp: '0177946522'
        },
        chatData: { senderLang: 'de' }
      })
      // .expect('Content-Type', 'image/png')
      .expect((res) => {
        // console.log('res.body ', res.body)
        assert.equal(res.body.hasOwnProperty('result'), true)
      })
      .end(done)
  })
  it('should load all users for admin', function (done) {
    /*     
      message = {
        meta: { type: 'text', collName: 'users', intent: 'downloadAllUsersForAdmin' },
        data: { author: 'admin' },
        admin: true
      }
      */
    request(app)
      .post('/loadAllUsers')
      .send({
        meta: { type: 'text', collName: 'users', intent: 'downloadAllUsersForAdmin' },
        data: { author: 'admin' },
        admin: true
      })
      // .expect('Content-Type', 'image/png')
      .expect((res) => {
        console.log('res.body ', res.body)
        assert.equal(res.body.hasOwnProperty('userList'), true)
      })
      .end(done)
  })
  it('should load all file-messages from user for admin', function (done) {
    request(app)
      .post('/loadUserFileMessages')
      .send({
        meta: { type: 'text', collName: 'messages', intent: 'downloadUserAllFileMessagesForAdmin' },
        data: { author: 'admin', userId: testUserId },
        admin: true
      })
      // .expect('Content-Type', 'image/png')
      .expect((res) => {
        console.log('res.body ', res.body)
        assert.equal(res.body.hasOwnProperty('messageList'), true)
      })
      .end(done)
  })
})
