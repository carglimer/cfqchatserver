const fieldsCategories = { syntax: 'syntax', unique: 'unique' }
const jwtCategories = { content: 'content', intent: 'intent', notAuth: 'notAuth' }
const types = { number: 'number', string: 'string', boolean: 'boolean' }

export const FieldsCategories = Object.freeze(fieldsCategories)
export const JWTCategories = Object.freeze(jwtCategories)
export const Types = Object.freeze(types)

export const getValidationBase = function (
  name,
  text = '',
  category = fieldsCategories.syntax,
  isRequired = true,
  type = types.string
) {
  return Object.assign(getBase(name, category, text, 400), { info: { isRequired, type } })
}

export function getBase(name, category, text, status) {
  return { name, category, text, status }
}
