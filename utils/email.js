// const nodemailer = require('nodemailer')
// const fs = require('fs-extra')
import nodemailer from 'nodemailer'
import fs from 'fs-extra'
const adminEmail = process.env.CONTACT_HOST_EMAIL || 'carglimer@web.de'

// email-shi gaaqtiurebuli unda ikos
const mailFromOptionsDefault = {
  email: adminEmail,
  pw: 'imereti%1',
  host: 'smtp.web.de',
  port: 587
}
export default class EmailService {
  constructor(mailFromOptions) {
    if (!mailFromOptions) {
      mailFromOptions = mailFromOptionsDefault
    }
    this.email = mailFromOptions.email
    let serverOptions = {
      auth: {
        user: mailFromOptions.email,
        pass: mailFromOptions.pw
      }
    }
    mailFromOptions.service && (serverOptions.service = mailFromOptions.service)
    mailFromOptions.host && (serverOptions.host = mailFromOptions.host)
    mailFromOptions.port && (serverOptions.port = mailFromOptions.port)

    this.transporter = nodemailer.createTransport(serverOptions)
  }
  // body=confirmation={ email: user.data.email, lang:user.data.originLang, subject: 'confirmation' }
  sendEmail(body, resp) {
    const emailReceiver = body.email
    // $log.info('em body ' + JSON.stringify(body))
    // const html = readTextfile(body.lang, 'order_received')
    // html teqstis cakitxvis shemdeg vagzavnit mails
    const lang = body.lang || 'de'
    const path = './utils/locales/' + lang + '/order_received.txt'
    $log.info('em filepath = ' + path)
    fs.readFile(path, 'utf8', function (err, data) {
      err && resp && resp.send('file read error:' + err)
      body.to = emailReceiver
      body.html = data
      sendEmailHTML(body)
        .then(() => {
          $log.info('em Email sent: ')
          resp && resp.send('email succesfully sent')
        })
        .catch((err) => {
          $log.error('em send mail error ', error)
          resp && resp.send('em send mail error ' + error)
        })
    })
  }
  // body={ email, lang, subject, text, html, attachments}
  sendEmailHTML(body) {
    const emailReceiver = body.email
    // $log.info('em body ' + JSON.stringify(body))
    const lang = body.lang || 'de'
    let transporter = this.transporter
    let email = this.email
    let mailOptions = {
      from: email
      // to: emailReceiver // array of receivers
      // subject: subject,
      /* text: 'Hello world?', */
      // html: data
      /* attachments: [
          { // Use a URL as an attachment
            filename: 'your-testla.png',
            path: 'https://media.gettyimages.com/photos/view-of-tesla-model-s-in-barcelona-spain-on-september-10-2018-picture-id1032050330?s=2048x2048'
          }
        ] */
    }
    if (body.to) {
      mailOptions.to = body.to
    }
    if (body.subject) {
      mailOptions.subject = body.subject
    }
    if (body.text) {
      mailOptions.text = body.text
    }
    if (body.html) {
      mailOptions.html = body.html
    }
    if (body.attachments) {
      mailOptions.html = body.attachments
    }
    $log.info('em mailOptions = ' + JSON.stringify(mailOptions))
    return new Promise((resolve, reject) => {
      transporter.sendMail(mailOptions, (error, info) => {
        // info: {"accepted":["info@deservice.net"],"rejected":[],"envelopeTime":99,"messageTime":103,"messageSize":393,"response":"250 Requested mail action okay,
        // completed: id=0Lp9kK-1l7qt61RPY-00es5f","envelope":{"from":"carglimer@web.de","to":["info@deservice.net"]},"messageId":"<5ebf0e57-4cc9-710c-872d-c898add08617@web.de>"}
        $log.info('em send info: ' + JSON.stringify(info))
        if (error) {
          $log.info('em send error: ' + error)
          reject(error)
        } else {
          $log.info('em Email sent: ')
          resolve()
        }
      })
    })
  }
}
