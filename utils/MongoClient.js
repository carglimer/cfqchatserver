import { MongoClient as Mongo_Client, MongoError } from 'mongodb'

// const url_centos = 'mongodb://admin:imereti1@136.243.102.152:27017/dbName?authSource=admin'
// const url_local = 'mongodb://admin:imereti1@localhost:27017/dbName?authSource=admin'
// mongodb 4.16.0
// api აქ არის -> https://mongodb.github.io/node-mongodb-native/4.16/
const testCollectionName = 'testCollection'
export class MongoClient {
  constructor(uri, dbName, emitter, user) {
    // uri = uri.replace('dbName', dbName)
    $log.info('mC Connecting to MongoDB uri = ' + JSON.stringify(uri))
    const client = new Mongo_Client(uri)
    client
      .connect()
      .then((db) => {
        $log.info('mC db = ' + db)
        this.dbn = client.db(dbName)
        if (db && user) {
          $log.info('mC createUser = ' + JSON.stringify(user))
          // mashin user jer ar sheqmnila
          this.createUser(client, dbName, emitter, user)
        } else {
          emitter.emit($events.mongo_connected)
          $log.info('mC Connected to MongoDB db = ' + this.dbn)
        }
      })
      .catch((err) => {
        // $log.info('mC error MongoDB err = ' + err)
        emitter.emit('mongo-error', err)
      })
  }
  getCollection(collectionName) {
    // console.log('this.dbn =', this.dbn !== undefined)
    return this.dbn.collection(collectionName)
  }
  async createUser(client, dbName, emitter, user) {
    $log.info('mc createUser')
    try {
      // await client.connect()
      const db = client.db(dbName)
      // console.log('testCollectionName =' + JSON.stringify(db.getCollectionnames()))
      const result = db.command(user)
      $log.info('mc user created = ' + user.createUser)
      emitter.emit($events.mongo_connected)
      // if (!db.collection(testCollectionName)) {
      try {
        $log.info('CollectionNames = ' + this.dbn.collection(testCollectionName))
      } catch (error) {
        $log.info('mc collection not exists!!! ')
        db.createCollection(testCollectionName)
        $log.info('mc collection created')
      }
    } catch (error) {
      emitter.emit('createUser-error = ', error)
    }
  }
  async listDatabases(client) {
    const databasesList = await client.db().admin().listDatabases()
    console.log('Databases:')
    databasesList.databases.forEach((db) => console.log(` - ${db.name}`))
  }
}

export function find(data) {
  // data = { collName: 'invoices', query: {...}, sort:{...}, projection : { "id": 0, name:1 romeli velebi gvinda-1 tu ara-0}}
  $log.info('mc mongo find data = ' + JSON.stringify(data))
  return new Promise((resolve, reject) => {
    if (!$mongo) {
      const errorMessage = 'mongo not initialzed'
      $log.info(errorMessage)
      reject(new Error(errorMessage))
    }
    const coll = $mongo.getCollection(data.collName)
    coll
      .find(data.query, data.projection)
      // .sort({ name: 1 })
      .toArray()
      .then((items) => {
        if (items.length) {
          $log.info('Successfully found ${items.length} documents: ' + items.length)
        } else {
          $log.info($etexts.noDocumentsFound)
        }
        //items.forEach(console.log)
        resolve(items)
      })
      .catch((err) => {
        $log.info($etexts.noDocumentsFound + ' err: ' + err)
        reject(err)
      })
  })
}
export function find2(data) {
  // data.meta = { collName: 'invoices'}
  // data.options = { query: {...}, sort:{...}, projection : { "id": 0, name:1 romeli velebi gvinda-1 tu ara-0}}
  $log.info('data.options= ' + JSON.stringify(data.options))
  const coll = $mongo.getCollection(data.meta.collName)
  return new Promise((resolve, reject) => {
    coll
      .find(data.options.query, data.options.projection)
      // .sort({ name: 1 })
      .toArray()
      .then((items) => {
        if (items.length) {
          $log.info('Successfully found ${items.length} documents: ' + items.length)
        } else {
          $log.info($etexts.noDocumentsFound)
        }
        //items.forEach(console.log)
        resolve(items)
      })
      .catch((err) => {
        $log.info($etexts.noDocumentsFound + ' err: ' + err)
        reject(err)
      })
  })
}

export function updateOne(data) {
  // $log.info('updateOne data = ' + JSON.stringify(data))
  const coll = $mongo.getCollection(data.collName)
  return new Promise((resolve, reject) => {
    coll
      .updateOne(data.query, data.update, data.options)
      .then((result) => {
        $log.info('mc updateOne result = ' + JSON.stringify(result))
        const { matchedCount, modifiedCount, upsertedId } = result
        if (matchedCount || upsertedId) {
          $log.info('mc Successfully updated a new review.')
          // emitter.emit('updated.one', data.id)...
          let resp = { update: false }
          if (matchedCount && modifiedCount) {
            // document gefunden und geändert
            resp.update = true
          } else if (upsertedId) {
            // document nicht gefunden und inserted
            // upsertedId = { index: 0, _id: '66345e8ed3bf603111ae0f9e' }
            let insertedId = upsertedId
            if (global.$methods.isObject(insertedId)) {
              insertedId = upsertedId._id
            }
            resp.update = false
            resp.insertedId = insertedId
          }
          // wenn document gefunden und nicht geändert, bleibt update: false
          resolve(resp)
        }
      })
      .catch((err) => {
        $log.info('mc updateOne err = ' + $etexts.updateDocumentFailed)
        reject(err)
      })
  })
}

export function updateMany(data) {
  // $log.info('updateOne data = ' + JSON.stringify(data))
  const coll = $mongo.getCollection(data.collName)
  return new Promise((resolve, reject) => {
    coll
      .updateMany(data.query, data.update, data.options)
      .then((result) => {
        $log.info('mc updateOne result = ' + JSON.stringify(result))
        const { matchedCount, modifiedCount, upsertedId } = result
        if (matchedCount || upsertedId) {
          $log.info('mc Successfully updateMany a new review.')
          // emitter.emit('updated.one', data.id)...
          let resp = { update: false }
          if (matchedCount && modifiedCount) {
            // document gefunden und geändert
            resp.update = true
          } else if (upsertedId) {
            // document nicht gefunden und inserted
            resp.update = false
            resp.insertedId = upsertedId._id
          }
          // wenn document gefunden und nicht geändert, bleibt update: false
          resolve(resp)
        }
      })
      .catch((err) => {
        $log.info('mc updateMany err = ' + $etexts.updateDocumentFailed)
        reject(err)
      })
  })
}

export function insertOne(data) {
  $log.info('saveIntoDB')
  const coll = $mongo.getCollection(data.meta.collName)
  // orjer rom ar chaceros ts unique aris
  // coll.createIndex({ ts: data.ts }, { unique: true })
  return new Promise((resolve, reject) => {
    coll
      .insertOne(data)
      .then((res) => {
        resolve(res.insertedId)
      })
      .catch((err) => {
        $log.info('mongo insertOne err ' + err.toString())
        reject(err)
      })
  })
}

export function deleteMany(data) {
  $log.info('removefromDB data = ' + JSON.stringify(data))
  const coll = $mongo.getCollection(data.collName)
  return new Promise((resolve, reject) => {
    coll
      .deleteMany(data.query)
      .then((res) => {
        // res = {"result":{"n":1,"ok":1},"connection":{"_events":{},"_eventsCount":4,"id":1,"address":"127.0.0.1:27017","bson":{},"socketTimeout":0,"host":"localhost","port":27017,
        // "monitorCommands":false,"closed":false,"destroyed":false,"lastIsMasterMS":5},"deletedCount":1,"n":1,"ok":1}
        $log.info('mongo res.result = ' + res.result)
        resolve(res.result)
      })
      .catch((err) => {
        $log.info('mongo remove err = ' + err.toString())
        reject(err)
      })
  })
}
