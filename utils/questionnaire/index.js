import { deleteFileIfExists, getRelPathAndFilenameFromMediaUrl } from '../help'
import { translateTextArray } from '../translator'
import { find, updateOne } from '../MongoClient'
import { acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire } from '../../chat/chat'
import { ObjectId } from 'mongodb'

async function getQValuesArr(requestBody) {
  // requestBody = { vin6 } || { link }
  // mongo-ში linkId-ებით ვეძებთ და არა link-ით
  let query
  if (requestBody.vin6) {
    query = { vin6: requestBody.vin6 }
  } else if (requestBody.link) {
    const linkId = $getLinkId(requestBody.link)
    if (linkId) {
      query = { linkId }
    }
  } else if (requestBody.cardId) {
    query = { cardId: requestBody.cardId }
  }
  if (!query) {
    return $etexts.questionnaireQueryRequestError
    // response && response.send($etexts.noQuestionnaireFound)
  }
  const data = { collName: 'questionnaires', query }
  try {
    const questionnaireValuesArr = await find(data)
    if (questionnaireValuesArr && questionnaireValuesArr.length) {
      // $log.info('po handler find questionnaireValuesArr[0] = ' + questionnaireValuesArr[0])
      const qValuesArr = []
      for (const questionnaireValues of questionnaireValuesArr) {
        const qValues = questionnaireValues // .q[0]
        // aq values aris JSON.stringify-d da ukan unda gardavqmnat
        if (qValues) {
          qValues._id = questionnaireValues._id
          qValues.values = JSON.parse(qValues.values)
          qValuesArr.push(qValues)
        }
      }
      return qValuesArr
    } else {
      // throw new Error($etexts.noQuestionnaireFound)
      return $etexts.noQuestionnaireFound
    }
  } catch (error) {
    console.error('qi getQValuesArr error: ', error)
    throw error
  }
}
export async function validationInfo(requestBody, response) {
  // როდესაც დამკვეთი ასაჩივრებს დაკვეთას, ადმინის გვერდი მეკითხება { cardId } და ვუგზავნი vin და 3 სურათს
  try {
    const qValuesArr = await getQValuesArr(requestBody)
    $log.info('qi validationInfo qValuesArr = ' + JSON.stringify(qValuesArr))
    let answer
    if (qValuesArr === $etexts.noQuestionnaireFound) {
      answer = { vin: qValuesArr }
    } else if (qValuesArr) {
      const medias = qValuesArr[0].checkerValidation && qValuesArr[0].checkerValidation.attachments && qValuesArr[0].checkerValidation.attachments.medias ? qValuesArr[0].checkerValidation.attachments.medias : []
      answer = { vin: qValuesArr[0].vin, medias }
    }
    if (response) {
      response.send(answer)
    }
  } catch (error) {
    $log.info('qi validationInfo error: ' + error.toString())
    response && response.send({ error: $etexts.getQuestionnaireFailed, err: error.toString() })
  }
}
export function getQStandsArrForCarlinkOrVin6(requestBody, response) {
  try {
    const qValuesArr = getQValuesArr(requestBody)
    if (response) {
      response.send(qValuesArr)
    }
  } catch (error) {
    response && response.send({ error: $etexts.getQuestionnaireFailed, err: error.toString() })
  }
}
// murman -> არსებული ანკეტების გაყიდვის მოდულისათვის შენგან დამჭირდება ასეთი ასინქრონული ფუნქცია: გადმოგეცემა vinLasts (ექვსი სიმბოლო), შენ მიბრუნებ მასივს შემდეგი ობიექტებისას:
// {cardId, vin (სრული), filled: {musts, mustFills, all, fills}}
export function getQStandsArrForCarlinkOrVin6_2(requestBody, response) {
  // requestBody = { vin6 } || { link }
  // mongo-ში linkId-ებით ვეძებთ და არა link-ით
  let query
  if (requestBody.vin6) {
    query = { vin6: requestBody.vin6 }
  } else if (requestBody.link) {
    const linkId = $getLinkId(requestBody.link)
    if (linkId) {
      query = { linkId }
    }
  }
  if (!query) {
    response && response.send($etexts.noQuestionnaireFound)
    return
  }
  const data = { collName: 'questionnaires', query }

  find(data)
    .then((questionnaireValuesArr) => {
      // $log.info('po handler questionnaireValuesArr = ' + questionnaireValuesArr)
      if (questionnaireValuesArr && questionnaireValuesArr.length) {
        // $log.info('po handler find questionnaireValuesArr[0] = ' + questionnaireValuesArr[0])
        if (response) {
          const qValuesArr = []
          for (const questionnaireValues of questionnaireValuesArr) {
            const qValues = questionnaireValues // .q[0]
            // aq values aris JSON.stringify-d da ukan unda gardavqmnat
            if (qValues) {
              qValues._id = questionnaireValues._id
              qValues.values = JSON.parse(qValues.values)
              qValuesArr.push(qValues)
            }
          }
          response.send(qValuesArr)
        }
      } else {
        response && response.send($etexts.noQuestionnaireFound)
      }
    })
    .catch((err) => {
      console.error('qi error: ', err)
      response && response.send({ error: $etexts.getQuestionnaireFailed, err: err.toString() })
    })
}
// aq sheizleba QValues array moitxovos
export async function getQValuesByCardId(requestBody, response) {
  // requestBody = { cardId: 124, locale }
  // arrays- shemtxvevashi -> requestBody = { cardIdArr: [ 124, 141, 185,.. ], locale }
  // let data = { meta: { collName: 'questionnaires' } }
  const data = { collName: 'questionnaires' }
  // let options = {}
  // options.query = { cardId: requestBody.cardId }
  const locale = requestBody.locale
  let isArray = false
  // sheizleba array movides -> cardId: [ 2, 15, 20 ]
  if (requestBody.cardId) {
    data.query = { cardId: requestBody.cardId }
  } else if (requestBody.cardIdArr) {
    if (global.$methods.isArray(requestBody.cardIdArr)) {
      data.query = { cardId: { $in: requestBody.cardId } }
      isArray = true
    }
  } else if (response) {
    response.send({ error: $etexts.requestError, requestBody })
    return
  }
  // data.options = options
  // $log.info('po requestBody = ' + JSON.stringify(requestBody))
  // $log.info('po options = ' + JSON.stringify(options))
  try {
    const questionnaireValuesArr = await find(data)
    // $log.info('po handler questionnaireValuesArr = ' + questionnaireValuesArr)
    if (questionnaireValuesArr && questionnaireValuesArr.length) {
      // $log.info('po handler find questionnaireValuesArr[0] = ' + questionnaireValuesArr[0])
      if (response) {
        if (!isArray && locale) {
          // $log.info('qi getQValuesByCardId locale = ' + locale)
          // mashin ertia da satargmnia
          const questionnaireValues = questionnaireValuesArr[0] // .q[0]
          if (questionnaireValues) {
            // aq values aris JSON.stringify-d da ukan unda gardavqmnat
            // questionnaireValues._id = questionnaireValuesArr[0]._id
            questionnaireValues.values = JSON.parse(questionnaireValues.values)
          }
          // $log.info('qi getQValuesByCardId questionnaireValues.lang = ' + questionnaireValues.lang)
          // $log.info('qi getQValuesByCardId questionnaireValues.cardId = ' + (+questionnaireValues.cardId + 20))
          // $log.info('qi getQValuesByCardId questionnaireValues.cardId > 0 = ' + (+questionnaireValues.cardId > 0))
          if (questionnaireValues.lang && questionnaireValues.lang !== locale && +questionnaireValues.cardId > 0) {
            // $log.info('qi getQValuesByCardId satargmnia locale = ' + locale)
            // mashin unda gadavtargmnot locale-s enaze da mxolod ert questionnaireValues gadavtargmnit
            translateAndSendQuestionnaireValues(response, questionnaireValues, locale)
          } else {
            // mashin pirdapir vagzavnit. Array unda gaigzavnos
            response.send([questionnaireValues])
          }
        } else {
          const qValuesArr = []
          for (const questionnaireValues of questionnaireValuesArr) {
            const qValues = questionnaireValues // .q[0]
            // aq values aris JSON.stringify-d da ukan unda gardavqmnat
            if (qValues) {
              // qValues._id = questionnaireValues._id
              qValues.values = JSON.parse(qValues.values)
              qValuesArr.push(qValues)
            }
          }
          response.send(qValuesArr)
        }
      }
    } else {
      response && response.send($etexts.noQuestionnaireFound)
    }
  } catch (err) {
    console.error('getQValuesByCardId po error: ', err)
    response && response.send({ error: $etexts.getQuestionnaireFailed, err: err.toString() })
  }
}
function getCommentTextsFromQValues(questionnaireValues) {
  const qValues = questionnaireValues.values // values -> aq gadmoparsulia
  const commentTexts = {}
  // mxolod imat amovkrebt, sadac chanacerebi arsebobs
  Object.keys(qValues).forEach((key) => {
    // $log.info('po key = ' + key)
    // tu questionItem-ia
    if (key.includes('.')) {
      const value = qValues[key]
      if (value.comment && value.comment.length) {
        $log.info('qi value.comment = ' + value.comment)
        commentTexts[key] = value.comment
      }
    }
  })
  return commentTexts
}
async function translateAndSendQuestionnaireValues(response, questionnaireValues, locale) {
  // aq values ukve gaparsulia
  const qValues = questionnaireValues.values
  // $log.info('po questionnaireValues = ' + JSON.stringify(questionnaireValues))
  const commentTexts = getCommentTextsFromQValues(questionnaireValues)
  // $log.info('qi commentTexts = ' + JSON.stringify(commentTexts))
  // tu arcerti comment ar aris
  if (Object.keys(commentTexts).length === 0) {
    response && response.send([questionnaireValues])
    return
  }
  try {
    const result = await translateTextArray(Object.values(commentTexts), questionnaireValues.lang, locale)
    // { translatedText, translationError }
    // result = {"translatedText":["მართლა კარგი მანქანაა","ის არის კერძო პირი და გვხვდება როგორც ძალიან პატიოსანი"]}
    // $log.info('qi result = ' + JSON.stringify(result))
    const commentTranslatedTexts = {}
    let i = 0
    if (result.translatedText) {
      Object.keys(commentTexts).forEach((key) => {
        commentTranslatedTexts[key] = result.translatedText[i]
        i++
      })
    }
    // $log.info('po commentTexts = ' + JSON.stringify(commentTexts))
    // $log.info('po commentTranslatedTexts = ' + JSON.stringify(commentTranslatedTexts))
    // chavceret gadatargmnili teqstebi
    Object.keys(qValues).forEach((key) => {
      if (commentTranslatedTexts[key]) {
        const value = qValues[key]
        value.translatedComment = { from: questionnaireValues.lang, text: commentTranslatedTexts[key] }
      }
    })
    // $log.info('po qValues = ' + JSON.stringify(qValues['1.1']))
    questionnaireValues.values = qValues
    // Array unda gaigzavnos
    response && response.send([questionnaireValues])
  } catch (err) {
    $log.info('qi translateAndSendQuestionnaireValues translateTextArray err: ' + err)
    response && response.send({ error: $etexts.translateQuestionnaireTextFailed, err })
  }
}

// request = requeuploadQuestionnairest.body
/*
radgan kitxvaris atvirtva multi sheizleba ikos da murmanis back-ma unda icodes namdvili mdgomareoba, shemovighet 4 safexuriani sistema:
  pirvel doneze vinaxavt shemdeg parametrebs: cardId, vin6, vin17, link.  q array-shi ki vinaxavt kvela mnishvnelobebs
1. upload-isas jer chavcer q array-shi q[0] adgilas axal atvirtuls
2. tu ver chavcere, aghar shevacuxeb murmanis back-s da error-s gavagzavni klienttan
3. tu chavcere da murmanistan error-ia, cavshli q-shi axal chanacers anu q[0]-s
4. tu chavcere  da murmanistan ok, cavshli q-shi pirvelis garda yvelas da amave dros caishleba toRemove siashi arsebuli media-ebic
*/
export async function uploadQuestionnaire(requestBody, response) {
  $log.info('qi uploadQuestionnaire ')
  let questionnaireValues = requestBody
  const { _id, ..._questionnaireValues } = questionnaireValues
  _questionnaireValues.vin6 = $methods.extractVin6FromVin(questionnaireValues.vin)
  _questionnaireValues.vin17 = questionnaireValues.vin

  let updateCondition = {
    $set: _questionnaireValues
  }
  if (questionnaireValues.link) {
    const linkId = $getLinkId(questionnaireValues.link)
    if (linkId) {
      updateCondition.$set.linkId = linkId
    }
  }
  try {
    await updateQ(questionnaireValues, { update: updateCondition })
    $log.info('qi $withoutMurmanBack = ' + global.$withoutMurmanBack)
    // chavcere da gavagebinet mainBack-s rom questionnaire atvirtulia da orderers sheatkobinos notification-it
    const respData = await acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(questionnaireValues, $caller.questionnaire)
    $log.info('qi $respData = ' + respData)
    try {
      $log.info('qi questionnaire upload all things are ok!')
      response.send({ questionnaireId: questionnaireValues._id })
    } catch (err) {
      $log.info('qi err2: ' + err.toString())
      response.send({ error: $etexts.writeQuestionnaireToMongoFailed, err: err.toString() })
    }
  } catch (err) {
    // ver chaicera mongo-shi da murmanis servers aghar vukavshirdebit
    $log.info('qi err4: ' + err.toString())
    response.send({ error: $etexts.writeQuestionnaireToMongoFailed, err: err.toString() })
  }
}

export function removeQMediaOnMongo(requestBody, response) {
  // requestBody = { toRemove: '/questionnaires/1620597851020_1874893.jpeg', qValues: { _id: '60942c64de5a355f3cdb344f', cardId: 17, ... values: '{"1":{},"2"..'}}
  $log.info('qi removeQMediaOnMongo ')
  let qValues = requestBody.qValues
  if (!qValues || !qValues._id || !requestBody.toRemove) {
    let reason
    if (!qValues) {
      reason = 'no qValues provided'
    } else if (!requestBody.toRemove) {
      reason = 'no toRemove provided'
    } else if (!qValues._id) {
      reason = 'no _id provided'
    }
    response.send({ error: $etexts.removeQmediaOnMongoFailed, reason })
    return
  }

  let { _id, ..._qValues } = requestBody.qValues
  _id = ObjectId(_id)
  // jer chavcert q[0]-shi
  const updateCondition = {
    $set: _qValues /* {
      // 'q.0': requestBody.qValues
      'q.0': requestBody.qValues
    } */
  }
  updateQ(qValues, { query: { _id }, update: updateCondition })
    .then(() => {
      if (requestBody.toRemove) {
        const relPathAndFilename = getRelPathAndFilenameFromMediaUrl(requestBody.toRemove)
        const relPath = relPathAndFilename.relPath
        const filename = relPathAndFilename.filename
        deleteFileIfExists(relPath + filename)
      }
      $log.info('qi removeQMediaOnMongo ok!')
      response.send({ success: _id })
    })
    .catch((err) => {
      $log.info('qi err5: ' + err.toString())
      response.send({ error: $etexts.removeQmediaOnMongoFailed, err: err.toString() })
    })
}
export async function uploadExampleQuestionnaire(requestBody, response) {
  $log.info('qi uploadExampleQuestionnaire ')
  let questionnaireValues = requestBody
  // $log.info('po questionnaireValues = ' + JSON.stringify(questionnaireValues))
  // თუ questionnaireExampleData არის, მაშინ ჯერ ყველა ენაზე უნდა ვთარგმნოთ
  if (questionnaireValues.cardId < 0) {
    // commentTexts = { '1.1': 'Guten Tag',...}
    // aq values aris JSON.stringify-d da ukan unda gardavqmnat
    questionnaireValues.values = JSON.parse(questionnaireValues.values)
    const commentTexts = getCommentTextsFromQValues(questionnaireValues)
    $log.info('qi commentTexts = ' + JSON.stringify(commentTexts))
    if (global.$translate && Object.keys(commentTexts).length) {
      const langs = process.env.EXAMPLE_QUESTIONNAIRE_LANGUAGES ? JSON.parse(process.env.EXAMPLE_QUESTIONNAIRE_LANGUAGES) : $availableLangs // ['de', 'en', 'ka', 'ru']
      langs.shift() // ["de","en","ka","ru"] -> ["en","ka","ru"]
      const translatePromises = []
      langs.forEach((lang) => {
        translatePromises.push(translateTextArray(Object.values(commentTexts), 'de', lang))
      })
      try {
        const results = await Promise.all(translatePromises)
        // [{ translatedText, translationError },...]
        // results = [{"translatedText":["Good night","Best deal"]},{"translatedText":["Ღამე მშვიდობისა","Საუკეთესო შეთანხმება"]},
        // {"translatedText":["Спокойной ночи","Лучшее предложение"]}]
        $log.info('qi results = ' + JSON.stringify(results))
        const commentTranslatedTexts = {}
        let j = 0
        results.forEach((result) => {
          // result = {"translatedText":["Good night","Best deal"]}
          // const langs = ["en","ka","ru"]
          let i = 0
          if (result.translatedText) {
            // commentTexts = {"1.1":"Gute Nacht","1.2":"Bestes Geschäft"}
            const langValObj = {}
            Object.keys(commentTexts).forEach((key) => {
              langValObj[key] = result.translatedText[i]
              // commentTranslatedTexts[langs[j]][key] = result.translatedText[i]
              i++
            })
            commentTranslatedTexts[langs[j]] = langValObj
            j++
          }
        })
        // commentTranslatedTexts = {"en":{"1.1":"Good night","1.2":"Best deal"},"ka":{"1.1":"Ღამე მშვიდობისა","1.2":"Საუკეთესო შეთანხმება"},"ru":{"1.1":"Спокойной ночи","1.2":"Лучшее предложение"}}
        $log.info('qi commentTranslatedTexts = ' + JSON.stringify(commentTranslatedTexts))
        // chavceret gadatargmnili teqstebi
        const qValues = questionnaireValues.values
        Object.keys(qValues).forEach((key) => {
          const value = qValues[key]
          const exampleTranslatedComment = {}
          langs.forEach((lang) => {
            if (commentTranslatedTexts[lang][key]) {
              exampleTranslatedComment[lang] = commentTranslatedTexts[lang][key]
            }
          })
          if (Object.keys(exampleTranslatedComment).length) {
            value.exampleTranslatedComment = exampleTranslatedComment
          }
          // "exampleTranslatedComment":{"en":"Good night","ka":"Ღამე მშვიდობისა","ru":"Спокойной ночи"}}
          // $log.info('po exampleTranslatedComment = ' + JSON.stringify(exampleTranslatedComment))
        })
        saveQExampleValuesOnMongo(questionnaireValues, response)
        // })
      } catch (err) {
        $log.info('qi uploadExampleQuestionnaire translateTextArray error: ' + err)
        response && response.send({ error: $etexts.uploadQuestionnaireFailed, err: err.toString() })
      }
    } else {
      // mashin ukomentarebod aris
      saveQExampleValuesOnMongo(questionnaireValues, response)
    }
  }
}
function jsonStringify(obj) {
  if (typeof obj === 'object') return JSON.stringify(obj)
  else {
    return obj
  }
}
function saveQExampleValuesOnMongo(questionnaireValues, response) {
  // ისევ გავსტრინგოდ questionnaireValues.values
  questionnaireValues.values = jsonStringify(questionnaireValues.values)
  $log.info('qi questionnaireValues.values = ' + questionnaireValues.values)
  // const { _id, ..._questionnaireValues } = questionnaireValues
  // jer chavcert q[0]-shi
  uploadQuestionnaire(questionnaireValues, response)
}

// chavceret q[0]-shi
function updateQ(questionnaireValues, { query, update, options }) {
  let data = { collName: 'questionnaires' }
  /* let vin6 = null
  if (questionnaireValues.vin) {
    vin6 = questionnaireValues.vin.substring(questionnaireValues.vin.length - 6, questionnaireValues.vin.length)
  } */
  data.query = { cardId: questionnaireValues.cardId }
  if (query) {
    data.query = query
  }
  data.update = update
  data.options = { upsert: true }
  if (options) {
    data.options = { ...data.options, ...options }
  }
  $log.info('po data.options: ' + JSON.stringify(data.options))
  return new Promise((resolve, reject) => {
    updateOne(data)
      .then((res) => {
        $log.info('qi res ' + JSON.stringify(res))
        // const questionnaireValues = res.ops[0]
        if (res) {
          if (!res.update) {
            // mashin insert moxda
            questionnaireValues._id = res.insertedId
          }
          resolve(res)
        }
      })
      .catch((err) => {
        // ver chavcere mongo-shi
        reject(err)
      })
  })
}
/* export function uploadQuestionnaire2(requestBody, response) {
  $log.info('qi uploadQuestionnaire ')
  let questionnaireValues = requestBody
  // const test = true
  const { _id, ..._questionnaireValues } = questionnaireValues
  // jer chavcert q[0]-shi
  const updateCondition = {
    $set: {
      // 8 property 1 დონეზე გამოვიტანე, რათა მოსაძებნად ადვილი იყოს
      ts: questionnaireValues.ts,
      cardId: questionnaireValues.cardId,
      vin6: $methods.extractVin6FromVin(questionnaireValues.vin),
      vin17: questionnaireValues.vin,
      link: questionnaireValues.link,
      filled: questionnaireValues.filled,
      ordererId: questionnaireValues.ordererId,
      checkerId: questionnaireValues.checkerId
    },
    // chavceret q array-shi 0 position-ze
    $push: { q: { $each: [_questionnaireValues], $position: 0 } }
  }
  if (questionnaireValues.link) {
    const linkId = $getLinkId(questionnaireValues.link)
    if (linkId) {
      updateCondition.$set.linkId = linkId
    }
  }
  updateQ(questionnaireValues, { update: updateCondition })
    .then((res) => {
      // chavcere da gavagebinet mainBack-s rom questionnaire atvirtulia da orderers sheatkobinos notification-it
      acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(questionnaireValues, $caller.questionnaire)
        .then((respData) => {
          $log.info('qi respData ', JSON.stringify(respData))
          // chavcere  da murmanistan ok, cavshli q-shi pirvelis garda yvelas da amave dros caishleba toRemove siashi arsebuli media-ebic
          const updateCondition = { $push: { q: { $each: [], $slice: 1 } } }
          const options = { multi: true }
          updateQ(questionnaireValues, { update: updateCondition, options })
            .then(() => {
              if (questionnaireValues && questionnaireValues.toRemove && questionnaireValues.toRemove.length) {
                // mashin gvaqvs casashleli failebis array relPath-ebit
                for (const _relPath of questionnaireValues.toRemove) {
                  const relPathAndFilename = getRelPathAndFilenameFromMediaUrl(_relPath)
                  const relPath = relPathAndFilename.relPath
                  const filename = relPathAndFilename.filename
                  deleteFileIfExists(relPath + filename)
                }
              }
              $log.info('po questionnaire upload all things are ok!')
              response.send({ questionnaireId: questionnaireValues._id })
            })
            .catch((err) => {
              $log.info('qi err2: ' + err.toString())
              response.send({ error: $etexts.writeQuestionnaireToMongoFailed, err: err.toString() })
            })
        })
        .catch((err) => {
          $log.info('qi err3: ' + err.toString())
          // chavcere da murmanistan error-ia, cavshli q-shi axal chanacers, anu q[0]-s
          const updateCondition = { $pop: { q: -1 } } // -1 -> to remove first elem, 1 -> last elem
          updateQ(questionnaireValues, { update: updateCondition }).then(() => {
            $log.info('qi mongo q[0] removed ')
          })
          response.send({ error: $etexts.sendQuestionnaireNotifToMainBackFailed, err: err.toString() })
        })
    })
    .catch((err) => {
      // ver chaicera mongo-shi da murmanis servers aghar vukavshirdebit
      $log.info('qi err4: ' + err.toString())
      response.send({ error: $etexts.writeQuestionnaireToMongoFailed, err: err.toString() })
    })
} */
/* updateQ(questionnaireValues, { update: updateCondition })
    .then((res) => {
      $log.info('po res ', JSON.stringify(res))
      // chavcere  da murmanistan ok, cavshli q-shi pirvelis garda yvelas da amave dros caishleba toRemove siashi arsebuli media-ebic
      const updateCondition = { $push: { q: { $each: [], $slice: 1 } } }
      const options = { multi: true }
      updateQ(questionnaireValues, { update: updateCondition, options })
        .then(() => {
          if (questionnaireValues && questionnaireValues.toRemove && questionnaireValues.toRemove.length) {
            // mashin gvaqvs casashleli failebis array relPath-ebit
            for (const _relPath of questionnaireValues.toRemove) {
              const relPathAndFilename = getRelPathAndFilenameFromMediaUrl(_relPath)
              const relPath = relPathAndFilename.relPath
              const filename = relPathAndFilename.filename
              deleteFileIfExists(relPath + filename)
            }
          }
          $log.info('po questionnaire upload all things are ok!')
          response.send({ questionnaireId: questionnaireValues._id })
        })
        .catch((err) => {
          $log.info('po err6: ' + err.toString())
          response.send({ error: $etexts.writeQuestionnaireToMongoFailed, err: err.toString() })
        })
      // chavcere da gavagebinet mainBack-s rom questionnaire atvirtulia da orderers sheatkobinos notification-it
    })
    .catch((err) => {
      // ver chaicera mongo-shi da murmanis servers aghar vukavshirdebit
      $log.info('po err7: ' + err.toString())
      response.send({ error: $etexts.writeQuestionnaireToMongoFailed, err: err.toString() })
    }) */
