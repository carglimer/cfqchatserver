// const io = require('socket.io-client')
// const { logger } = require('./logger')
import io from 'socket.io-client'
import { logger } from './logger'

// socket, callback, message, store, setSocket
// აქ არ მუშაობს. კლიენტის მხარესაა
export function initSocket(payload) {
  let socket = payload.socket
  const callback = payload.callback
  const message = payload.message
  const snackbar = payload.snackbar
  const initSocketListeners = payload.initSocketListeners
  const store = payload.store
  // console.log('12 socket ', socket)
  // console.log('data: ', JSON.stringify({socket:this.socket!==null,isAdmin:this.$store.getters.isAdmin,token:this.$store.getters.token,user:this.$store.getters.user}))
  if (socket && socket.connected) {
    logger.info('iS socket.connected')
    if (callback) {
      if (message) {
        callback(message)
      } else callback()
    }
    return
  }
  logger.info('iS socket disconnected')
  let queryText = 'userId=' + store.getters.user._id + '&' + 'admin=' + store.getters.isAdmin + '&' + 'token=' + store.getters.token
  // mxolod satestod, mashin token aghar schirdeba
  if (payload.message && payload.message.meta && payload.message.meta.tokenoff) {
    queryText += '&tokenoff=true'
  }
  logger.info('iS queryText ', queryText)
  const query = { query: queryText }
  logger.info('iS this.$store.getters.chatUrl ', store.getters.chatUrl)
  socket = io(store.getters.chatUrl, query)
  if (payload.setSocket) {
    payload.setSocket(socket)
  }
  // this.socket.connect() da mere callback()
  initSocketHandler({ socket, callback, message, snackbar, initSocketListeners })
}
/* function initSocketListeners(socket) {
  socket.off("tokenError")
  socket.on("tokenError", tokenError => {
    // error-ebia da unda achvenos snackbarText
    console.log('tokenError=', tokenError)
  })
} */
// socket, callback, message, snackbar, initSocketListeners
function initSocketHandler(payload) {
  const socket = payload.socket
  const callback = payload.callback
  const message = payload.message
  const snackbar = payload.snackbar
  const timeout = 2000
  // let showSnackbarInfo = this.showSnackbarInfo
  socket.on('connect', () => {
    logger.info('iS connect snackbar')
    if (payload.initSocketListeners) {
      payload.initSocketListeners(socket)
    }
    if (callback) {
      if (message) {
        callback(message)
      } else callback()
    }
    snackbar && snackbar.showSnackbar({ text: 'connected', timeout, color: 'success' })
  })
  socket.on('connecting', () => {
    logger.info('iS connecting')
    snackbar && snackbar.showSnackbar({ text: 'connecting', timeout, color: 'warning' })
  })
  socket.on('disconnect', () => {
    logger.info('iS disconnect')
    snackbar && snackbar.showSnackbar({ text: 'disconnected', timeout, color: 'error' })
  })
  socket.on('connect_failed', () => {
    logger.info('iS connect_failed')
    snackbar && snackbar.showSnackbar({ text: 'connect_failed', timeout, color: 'error' })
  })
  socket.on('error', (err) => {
    // {name: "token", category: "content", text: "Your token is not valid", status: 400}
    logger.error('iS error ', JSON.parse(err))
    snackbar && snackbar.showSnackbar({ text: JSON.parse(err), timeout: 7000, color: 'error' })
  })
  // Fired upon a connection error
  socket.on('connect_error', (err) => {
    logger.error('iS connect_error')
    snackbar && snackbar.showSnackbar({ text: 'connect_error', timeout, color: 'error' })
  })
  socket.on('message', () => {
    logger.info('iS message')
    snackbar && snackbar.showSnackbar({ text: 'message', timeout, color: 'success' })
  })
  socket.on('reconnect', () => {
    logger.info('iS reconnect')
    snackbar && snackbar.showSnackbar({ text: 'reconnected', timeout, color: 'success' })
  })
  socket.on('reconnecting', () => {
    logger.info('reconnecting')
    snackbar && snackbar.showSnackbar({ text: 'reconnecting', timeout, color: 'warning' })
  })
  socket.on('reconnect_failed', () => {
    logger.info('iS reconnect_failed')
    snackbar && snackbar.showSnackbar({ text: 'reconnect_failed', timeout, color: 'error' })
  })
}
