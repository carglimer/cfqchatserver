import { Router } from 'express'
const router = Router()
/*
murman-> მონიტორინგის აპლიკაციისათვის მჭირდება რომ შენმა qchatserver-მა უპასუხოს ჩემს სამ request-ს მისამართებზე:
1. https://[test.]qchat.deservice.net/.../self  (სამი წერტილის მაგივრად path-ის ნაწილი, შენ რაც გაწყობს ის)
პასუხი მინდა დამიბრუნო ასეთი { data: { works: true } }  <- აქ ვპასუხობ, რომ ჩემი სერვერი მუშაობს, ამიტომ სულ true
2. https://[test.]qchat.deservice.net/.../mongo (სამი წერტილი იგივე რაც ზევით)
პასუხი მინდა დამიბრუნო ასეთი { data: { works: true/false, domain: mongo-ს დომენი } }
3. https://[test.]qchat.deservice.net/.../storage (სამი წერტილი იგივე რაც ზევით)
პასუხი მინდა დამიბრუნო ასეთი { data: { works: true/false, domain: storage-ს დომენი } }
*/

// სპაციალურად monitor პროგრამისთვის არსებული Token შემოწმება
// token chatServer-ში ipFilter-ში მოწმდება
// aq movida -> /self an /mongo an /storage
router.get('/ping', (req, res, next) => {
  res.send('pong')
})
/* router.all('/*', async (req, res, next) => {
  const url = req.headers.origin || req.headers.referer
  $log.info('all req.url: ' + req.url)
  if (url) {
    next()
  } else {
    // mashin murmanis back-idan aris request
    const token = req.headers.token || '1.2.3'
    try {
      // const verified = jwt.verify(token, process.env.JWT_SECRET)
      const verified = token === process.env.CF_BACK_MG_TOKEN
      $log.info('verified 29 = ' + verified)
      if (!verified) {
        // $throwLapse('AD001', 'token was incorect')
        res.status(462).send(new Error('token was incorect'))
      } else {
        next()
      }
    } catch (err) {
      $log.info('err 476 /*: ' + err.stack)
      res.status(476).send(err)
    }
  }
}) */
const getMongoDomain = (req) => {
  return global.$mongoDomain === 'localhost' ? req.headers.host : global.$mongoDomain
}
const getStorageDomain = (req) => {
  return global.$storageDomain === 'localhost' ? req.headers.host : global.$storageDomain
}
const monitoringMongo = (req, res) => {
  try {
    const coll = $mongo.getCollection('invoicenrs')
    if (coll) {
      $isTest && $log.info('mongo works')
      res.send({ data: { works: true, domain: getMongoDomain(req) } })
    } else {
      // $log.info('mongo do not works')
      res.send({ data: { works: false, domain: getMongoDomain(req) } })
    }
  } catch (error) {
    res.send({ data: { works: false, domain: getMongoDomain(req) } })
  }
}
// url = /questionnaires/1599165318843_49098.jpeg -> ../cfmedias/questionnaires/1599165318843_49098.jpeg
/* const monitoringStorage = (res) => {
  res.send({ data: { works: true, domain: $storageDomain } })
} */
router.get('/self', (req, res) => {
  res.send({ data: { works: true } })
})
router.get('/mongo', (req, res) => {
  monitoringMongo(req, res)
})
router.get('/storage', (req, res) => {
  // $log.info('send storage' + { data: { works: true, domain: $storageDomain } })
  res.send({ data: { works: true, domain: getStorageDomain(req) } })
})
// sxva shemtxvevashi
router.get('/*', (req, res) => {
  res.send({ data: 'wrongRequest' })
})

export default router
