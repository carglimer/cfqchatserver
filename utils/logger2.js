const winston = require('winston')
const path_err = process.env.PATH_ERR
const path_out = process.env.PATH_OUT

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  // defaultMeta: { service: "user-service" },
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new winston.transports.File({ filename: path_err, level: 'error' }),
    new winston.transports.File({ filename: path_out })
  ]
})

// If we're not in productionn then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
// NODE_ENV = develop || production
if (process.env.LOGS && JSON.parse(process.env.LOGS)) {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple()
    })
  )
}
exports.logger = logger
