import fs from 'fs-extra'

export const promiseDeleteFileIfExists = (path) => {
  return new Promise((resolve, reject) => {
    try {
      fs.unlink(path, function (err) {
        if (err) reject(err)
        resolve('deleted')
      })
    } catch (error) {
      reject(error)
    }
  })
}
export const deleteFileIfExists = (path) => {
  try {
    fs.unlink(path, function (err) {
      if (err) throw err
      // if no error, file has been deleted successfully
      $log.info('h File deleted: ' + path)
    })
  } catch (error) {
    $log.info('h File delete error: ' + error)
  }
}
// url = /questionnaires/1599165318843_49098_image.jpeg -> ../cfmedias/questionnaires/1599165318843_49098_image.jpeg
export const getRelPathAndFilenameFromMediaUrl = (url) => {
  const filename = url.substring(url.lastIndexOf('/') + 1) // -> 1599165318843_49098_image.jpeg
  const fileDb = url.substring(0, url.lastIndexOf('/') + 1) // -> /questionnaires/
  const relPath = $mediaDirParams.base + fileDb // -> ../cfmedias/questionnaires/
  return { relPath, filename }
}
