export default {
  email: 'info@deservice.net',
  nickName: 'adminGlimer',
  recId: 0,
  // serviceNames -> G1, G2...
  prices: {
    G1: 20, // ramden %-s ვიღებთ მომსახურებისთვის G1-carcheck
    G2: 15,
    adminResell: 12, // 12 Euro vor resell
    checkerResell: 5 // 5 Euro für resell für checker
  },
  privateData: {
    isJur: true,
    descrip: 'Glimer Consulting GmbH',
    countryAlpha2: 'de'
  },
  invoiceData: {
    plz: '29223',
    place: 'Celle',
    address: 'Kampstr. 18',
    countryAlpha2: 'de',
    vatNumber: 'DE13265465',
    vatRate: '19'
  }
}
