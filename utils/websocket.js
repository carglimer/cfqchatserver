import io from 'socket.io'
// const io = require('socket.io')
const { verifyJWT, createJWT, decodeJWT } = require('./jwt') // .verifyJWT
// const createJWT = require('../utils/jwt').createJWT
// const decodeJWT = require('../utils/jwt').decodeJWT
// import { verifyJWT } from '../utils/jwt'
const httpErrs = require('./httpErrors')
// const { logger } = require('./logger')
export default class WebSocket {
  constructor(server, emitter, namespace) {
    this.socketMaps = {}
    this.ws = io(server, {
      cors: {
        origin: global.$validOrigins,
        methods: ['GET', 'POST'],
        credentials: false
      }
    }).of('/') // ('/' + namespace)
    // this.ws.origins(['http://localhost:3083/*'])
    this.namespace = namespace
    // this.ws.use(this.checkToken.bind(this))
    // console.log('this.socketMaps:', this.socketMaps)
    this.ws.on('connection', (socket) => {
      // $log.info('wS user connect socket.id = ' + socket.id)
      socket.on('error', (err) => {
        $log.error('wS err = ' + err)
      })
      emitter.emit($events.connection, socket)
      // const query = JSON.parse(socket.handshake.query)
      const userId = +socket.handshake.query.userId
      const cardId = +socket.handshake.query.cardId
      // $log.info('wS userId isNumber = ' + (typeof userId === 'number'))
      // $log.info('wS cardId isNumber = ' + (typeof cardId === 'number'))
      // const lang = socket.handshake.query.lang
      // $log.info('wS userId = ' + userId)
      // $log.info('wS cardId = ' + cardId)
      // $log.info('wS socket.handshake.query = ' + JSON.stringify(socket.handshake.query))
      // $log.info('wS lang = ' + lang)
      const oldSocket = this.getSocketByUserId(userId)
      if (oldSocket) {
        oldSocket.disconnect(true)
      }
      // this.socketMaps[userId] = { socket, lang }
      this.socketMaps[userId] = { cardId, socket }

      Object.keys(this.socketMaps).forEach((userId) => {
        const cardId = this.socketMaps[userId].cardId
        $log.info('this.socketMaps userId = ' + userId)
        $log.info('this.socketMaps cardId = ' + cardId)
        //use key and value here
      })
      // this.userIsConnected(userId)
    })
  }
  getSocketByUserId(userId) {
    userId = +userId
    if (this.socketMaps[userId]) {
      return this.socketMaps[userId].socket
    }
    return null
  }
  getSocketByUserIdAndCardId(userId, cardId) {
    userId = +userId
    cardId = +cardId
    if (this.socketMaps[userId] && this.socketMaps[userId].cardId === cardId && this.userIsConnected(userId, cardId)) {
      return this.socketMaps[userId].socket
    }
    return null
  }
  /* getConnectedSocketsKeys() {
    // console.log('this.getConnectedSocketsKeys() = ', this.getConnectedSocketsKeys())
    return Object.keys(this.ws.sockets)
  } */
  // cin  /chat# -s umatebs da unda movacilot
  cleanSocketId(id) {
    // $log.info('wS cleanSocketId id = ' + id)
    const repl = '/' + this.namespace + '#'
    if (!id) {
      return ''
    } else return id.replace(repl, '')
  }
  // აღარ ვხმარობთ, რადგან სერვერი მიმღების ენას აღარ გადაამოწმებს
  /* getReceiverLang(userId) {
    if (this.socketMaps[userId]) {
      return this.socketMaps[userId].lang
    }
    return null
  } */
  socketIsConnected(socketId) {
    // $log.info('wS socketIsConnected socketId = ' + socketId)
    if (!socketId) {
      return false
    }
    for (const [_, socket] of this.ws.sockets) {
      // $log.info('wS socket.id = ' + socket.id)
      if (socketId === socket.id && socket.connected) {
        // $log.info('wS socket.connected = ' + socket.connected)
        return true
      }
    }
    return false
  }
  userIsConnected(userId, cardId) {
    // $log.info('wS userIsConnected userId isNumber = ' + (typeof userId === 'number'))
    // $log.info('wS userIsConnected cardId isNumber = ' + (typeof cardId === 'number'))
    userId = +userId
    cardId = +cardId
    // $log.info('wS getConnectedSocketsKeys = ' + Object.keys(this.ws.sockets))
    /* if (this.socketMaps[userId]) {
      // console.log('ws userId:', userId)
      $log.info('wS (this.socketMaps[userId]).cardId = ' + userId + ' ' + this.socketMaps[userId].cardId)
      $log.info('wS (this.socketMaps[userId]).id = ' + this.socketMaps[userId].socket.id)
    } */
    if (!this.socketMaps[userId] || !this.socketMaps[userId].socket || this.socketMaps[userId].cardId !== cardId) {
      return false
    }
    // console.log('this.getConnectedSocketsKeys() = ', this.getConnectedSocketsKeys())
    // const socketId = this.cleanSocketId(this.socketMaps[userId].socket.id)
    const socketId = this.socketMaps[userId].socket.id
    const connected = this.socketIsConnected(socketId) // this.socketMaps[userId] && this.getConnectedSocketsKeys().includes(socketId)
    // $log.info('wS userId connected = ' + connected)
    return connected
  }
  // middleware
  checkToken(socket, next) {
    const token = socket.handshake.query.token
    // const admin = socket.handshake.query.admin === 'true'
    const tokenoff = socket.handshake.query.tokenoff === 'true'
    const userId = socket.handshake.query.userId
    // admin = !!admin // if null, then false
    $log.info('wS socket.handshake.query ->' + socket.handshake.query)
    $log.info('wS data ->' + { token, userId })
    // console.log('wS data ->', { token, admin, userId })
    // console.log('wS tokenoff ->', tokenoff)
    // console.log('wS admin ->', admin)
    // console.log('wS socket.handshake.query ->', socket.handshake.query)
    if (tokenoff) {
      // this.socketMaps[userId] = { socket, admin, busy: false }
      next()
      return
    }
    try {
      const decoded = verifyJWT(token)
      $log.info('wS into websocket.js! decoded.userId:' + decoded.userId)
      // socketMaps[socket.id] = socket
      // this.socketMaps[decoded.userId] = { socket, admin, busy: false }
      $log.info('wS socket.id:' + socket.id)
      next()
    } catch (err) {
      $log.info('wS error tokenError' + err.message)
      // socket.emit('tokenError', httpErrs.incorrectTokenContentError)
      // const httpErr = httpErrs.incorrectTokenContentError({name: err.name, message: err.message})
      // const error = new Error(httpErr)
      // console.log('112 error ', JSON.stringify(error))
      // console.log('113 error ', error)
      // {"name":"token","category":"content","text":{"name":"TokenExpiredError","message":"jwt expired"},"status":400}
      let decoded = decodeJWT(token)
      const expiresAt = new Date(decoded.exp * 1000).toLocaleString()
      const errTxt = 'Error: token is expired at ' + expiresAt
      // console.log('expires at ', new Date(decoded.exp*1000).toLocaleString())
      $log.info('wS errTxt ' + errTxt)
      // next(httpErr)
      next(new Error(errTxt))
    }
  }
  renewToken(token) {
    try {
      const payload = verifyJWT(token)
      const userId = payload.userId
      // payload.exp-payload.iat არის expiresIn
      $log.info('wS payload.userId:' + payload.userId)
      const newToken = createJWT({ userId }, { expiresIn: payload.exp - payload.iat })
      return newToken
    } catch (err) {
      $log.info('wS renewToken error' + err)
    }
  }
  /**
   * The method receives data on socket event and call cb function with it.
   *
   * @param {string} event - The event name
   * @param {socketHandler} cb - The callback function with parameter data
   */
  read(event, cb) {
    this.socket.on(event, cb)
  }
  /**
   * The method sends data on socket event
   *
   * @param {string} event - The event name
   * @param {Object} data - Data that must be send
   */
  write(event, data) {
    this.socket.emit(event, data)
  }
}

/*
setAdminSocketBusyStatus(userId, busy) {
    $log.info('wS setAdminSocketBusyStatus userId ' + userId)
    if (this.socketMaps[userId] && this.getConnectedSocketsKeys().includes(this.socketMaps[userId].socket.id)) {
      if (this.socketMaps[userId].admin) {
        $log.info('wS busy ' + busy)
        this.socketMaps[userId].busy = busy
      }
    }
    // let socketId =  (this.socketMaps[userId]).id
    let connected =
      this.socketMaps[userId] && this.getConnectedSocketsKeys().includes(this.socketMaps[userId].socket.id)
    $log.info('wS userId connected = ' + connected)
    return connected
  }

findConnectedAdminSocketEntity() {
    let entity = null
    Object.keys(this.socketMaps).forEach(key => {
      let value = this.socketMaps[key]
      $log.info('wS value.socket.id ' + value.socket.id)
      if (value.admin && !value.busy && this.getConnectedSocketsKeys().includes(value.socket.id)) {
        entity = { userId: key, socket: value.socket, admin: value.admin, busy: value.busy }
      }
    })
    if (entity) {
      $log.info('wS entity.userId ' + entity.userId)
    }
    return entity
  } */
