// import httpErrs from './httpErrors'
import fs from 'fs-extra'
import multer from 'multer'
import ffmpeg from 'ffmpeg'
import { insertOne } from './MongoClient'
// import { getPureNamefromFilename } from './methods'
import { acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire, sendMessageIfReceiverOnline, setSavedStatusAndTsAndRenewToken } from '../chat/chat'

/* message asetia: relpath chaceris shemdeg daemateba
    {"meta":{"type":"file","collName":"messages"},
    "data":{"author":"me","text":"ert", "senderLang":"de","username":"misha","chatetName":'Herr Fisher', avatarSrc:'http..some.png'},
    "meta":{relpath: 'senderId/dateDay/date.jpg', "sender":"5d4b41f38b7a432c541ab3b4","msgId":"afgadfgadfg","receiver":"admin","status":"notSent",tsList:{}},
    "admin":false}

    shevinaxot file bazashi am misamartze:
    imageDirBase = '../tfotos/' == 'https://kfz-soft.com/public/transporter/tfotos/'
    imageDirBase/'userId/2019-8-20/1566329755060.jpg
    shevqmnat fileDir if not exists
// imageDirBase = ../tfotos/1567446037439_16257.jpg  misamarts gadaaketebs /docs/1567446037439_16257.jpg misamartad
const mediaDirParams = {
  base: '../cfmedias/',
  chatDb: 'chats/',
  chatUrl: '/chats/',
  questionnaireDb: 'questionnaires/'
  questionnaireUrl: '/questionnaires/'
}
*/

// chat da questionnaire orive xmarobs
function adjustFileDirBaseToUrl(message) {
  // dbFilename -> 1599165318843_49098_image.jpeg
  // changeAudioToMp3 არ სჭირდება, რადგან ბრაუზერშივე იცვლება mp3-ად და ენიჭება სახელი filename
  const dbFilename = message.meta.filename // tsList[$status.notSent] + '_' + message.meta.size + '_' + message.meta.type + '.' + message.meta.ext
  let folder
  $log.info('fU message = ' + JSON.stringify(message))
  if (message.meta.caller === 'chat') {
    // dbFilename = message.meta.filename // message.meta.tsList[$status.notSent] + '_' + message.meta.size + '_' + message.meta.type + '.' + message.meta.ext
    folder = $mediaDirParams.chatUrl
  } else if (message.meta.caller === 'questionnaire') {
    folder = $mediaDirParams.questionnaireUrl
  } else {
    // message.meta.caller = exampleQuestionnaire
    // examplemediaUrl
    folder = $mediaDirParams.examplemediaUrl
  }
  const dbDir = $mediaDirParams.base + folder // ../cfmedias/chats/  chat ? getDbDirForChatMedias() : getDbDirForQuestionnaireMedias()
  message.meta.relPath = folder + dbFilename // -> /chats/1599165318843_49098_image.jpeg
  $log.info('fU dbDir = ' + JSON.stringify(dbDir))
  $log.info('fU dbFilename = ' + JSON.stringify(dbFilename))
  return { dbDir, dbFilename } // <-dbPath
}
// indexData:{socket, serverData: this.serverData, webSocket: this.webSocket, setSavedStatusAndTsAndRenewToken, updateAndSendMessageStatus, insertOne}

// chat xmarobs
async function insertFileMessage(message, response) {
  message.meta.status = $status.savedInDb
  // callback(message.meta)
  // console.log('in wrapper message ', JSON.stringify(message))
  try {
    let insertedId = await insertOne(message)
    if (insertedId) {
      insertedId = insertedId.toString()
      $log.info('fU ...insertedId ' + insertedId)
      message.meta.msgId = insertedId
      // gavagebinet gamomgzavns, rom mivighet message ->
      if (response) {
        response.json({ result: 'done', meta: message.meta }) // (message.meta)
      }
      // tu receiver xazzea, mashin gadavugzavnot es FileMessage
      sendMessageIfReceiverOnline(message)
    }
  } catch (err) {
    $log.info('fU chat err:' + err)
    if (response) {
      response.send('Error uploading chat file:' + err)
    }
  }
}

// uploadChatFile_Multer -> chat-is failebis atvirtvisas vxmarobt socket-stream-is alternativad
// indexData = { request, response }

// caller -> chat||questionnaire
export function uploadMediaChatQuestionnaire({ request, response }) {
  // $log.info('fU file1 = ' + JSON.stringify(request))
  let isChat, message, dbPath, isAudio
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      message = JSON.parse(req.body.message)
      isChat = message.meta.caller === 'chat'
      $log.info('fU message = ' + JSON.stringify(message))
      // $log.info('fU file2 = ' + JSON.stringify(file))
      dbPath = adjustFileDirBaseToUrl(message)
      isAudio = message.meta.type === 'audio'
      if (isChat) {
        setSavedStatusAndTsAndRenewToken(message.meta)
        acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(message, $caller.message)
      }
      cb(null, dbPath.dbDir)
    },
    filename: function (req, file, cb) {
      file.mimetype = 'audio'
      cb(null, dbPath.dbFilename)
    }
  })
  // uploadMulter(request, response, storage, true, message)
  const upload = multer({ storage })
  const upl = upload.single('blob')
  if (isAudio) {
    processFfmpeg(dbPath)
  }
  upl(request, response, (err) => {
    /* if (isAudio) {
      processFfmpeg(dbPath)
    } */
    if (err) {
      return response.end('Error uploading file:' + err)
    } else if (isChat) {
      insertFileMessage(message, response)
    } else {
      $log.info('fU questionnaire completed = ' + JSON.stringify(message))
      response.json({ result: 'done', meta: message.meta })
    }
  })
}
function processFfmpeg(dbPath) {
  try {
    const process = new ffmpeg(dbPath.dbDir + dbPath.dbFilename)
    process.then((audio) => {
      audio.fnExtractSoundToMP3(dbPath.dbDir + dbPath.dbFilename, (error, file2) => {
        if (error) {
          console.log('audio error = ', error)
        } else {
          console.log('audio file2 = ', file2)
        }
      })
    })
  } catch (err) {
    console.log('audio err code = ', err.code)
    console.log('audio err msg = ', err.msg)
  }
}
// ar vxmarobt magram rezervshia
export function handlerFileMessage_Stream() {
  return (stream, message, callback) => {
    $log.info('fU stream message = ' + JSON.stringify(message))
    stream.on('error', (error) => {
      $log.info('fU error' + error)
      callback({ error })
    })
    if (message.meta.collName === 'messages') {
      // meta:{sender : userId, id:'', receiver : '101', status:'notSent', tsList:{}}
      setSavedStatusAndTsAndRenewToken(message.meta)
      $log.info('fU message.meta.receiver' + message.meta.receiver)
      // asynchron shevqmnat fileDir if not exists
      const sender = message.meta.sender
      // const relDir = imageDirBase // es sheizleba varegulrot momavlisatvis
      // createFileDir(imageDirBase, sender, callback, (relDir, fileDirCreated) => {
      // ts_size.ext -> filename = 1567446037439_16257.jpg
      // const relPath = relDir + message.meta.tsList[status.notSent] + '_' + message.meta.size + '.' + message.meta.ext

      // $log.info('fU createDirIfNotExists  relPath ' + relPath)
      // message.meta.relPath = relPath.replace(imageDirBase, imageDirURl)
      // $log.info('fU message.meta.relPath = ' + message.meta.relPath)
      // shevinaxet file
      const dbPath = adjustFileDirBaseToUrl(message)
      $log.info('fU handlerFileMessage_Stream  dbPath ' + dbPath)
      stream.pipe(fs.createWriteStream(dbPath.dbDir + dbPath.dbFilename))
      // })
      stream.on('end', () => {
        // aq filepath sheqmnilia da file chacerilia
        $log.info('fU completed callback = ' + callback)
        insertFileMessage(message, callback)
      })
    }
  }
}
export function createDirIfNotExists(fileDir, error, success) {
  $log.info('fU fileDir ' + fileDir)
  // shevqmnat fileDir if not exists
  try {
    if (!fs.existsSync(fileDir)) {
      $log.info('fU fileDir not exists' + fileDir)
      fs.mkdir(
        fileDir,
        {
          recursive: true
        },
        (err) => {
          if (err) {
            $log.info('err' + err)
            if (error) {
              error({ error: err })
            }
          } else {
            // aq vagrzelebt
            if (success) {
              $log.info('fU fileDirCreated ' + fileDir)
              success(true)
            }
          }
        }
      )
    } else {
      if (success) {
        $log.info('fU fileDirExists ' + fileDir)
        success(false)
      }
    }
  } catch (err) {
    if (error) {
      error({ error: err })
    }
  }
}

/*
// uploadQuestionnaireFile_Multer -> questionnaire-is failebis atvirtvisas vxmarobt
export function uploadMediaQuestionnaire({ request, response }) {
  // const file = req.file
  let message, dbPath
  let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      message = JSON.parse(req.body.message)
      if (message.meta.type === 'audio') {
        file.mimetype = 'audio'
      }
      dbPath = adjustFileDirBaseToUrl(message)
      $log.info('fU dbPath.dbDir = ' + dbPath.dbDir)
      cb(null, dbPath.dbDir)
    },
    filename: function (req, file, cb) {
      $log.info('fU dbPath.dbFilename = ' + dbPath.dbFilename)
      cb(null, dbPath.dbFilename)
    }
  })
  let upload = multer({ storage })
  let upl = upload.single('blob')
  upl(request, response, (err) => {
    if (err) {
      response.end('Error uploading file:' + err)
    } else {
      response.json({ result: 'done', meta: message.meta })
    }
  })
}

export function uploadMediaChat({ request, response }) {
  const file = request.file
  // $log.info('fU file1 = ' + JSON.stringify(request))
  let message, dbPath, fieldname
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      message = JSON.parse(req.body.message)
      $log.info('fU message = ' + JSON.stringify(message))
      $log.info('fU file2 = ' + JSON.stringify(file))
      if (message.meta.type === 'audio') {
        file.mimetype = 'audio'
      }
      if (message.meta.collName === 'messages') {
        setSavedStatusAndTsAndRenewToken(message.meta)
        // const sender = message.meta.sender
        dbPath = adjustFileDirBaseToUrl(message)
        $log.info('fU dbPath.dbDir = ' + dbPath.dbDir)
        cb(null, dbPath.dbDir)
      }
    },
    filename: function (req, file, cb) {
      $log.info('fU dbPath.dbFilename = ' + dbPath.dbFilename)
      $log.info('fU file2 = ' + JSON.stringify(file))
      cb(null, dbPath.dbFilename)
    }
  })
  // uploadMulter(request, response, storage, true, message)
  const upload = multer({ storage })
  $log.info('fU fieldname = ' + fieldname)
  const upl = upload.single('blob')
  // const upl = upload.single(fieldname)
  upl(request, response, (err) => {
    if (err) {
      return response.end('Error uploading file:' + err)
    } else {
      $log.info('fU chat completed = ' + JSON.stringify(message))
      insertFileMessage(message, response)
    }
  })
}

function changeAudioToMp3(message) {
  let filename = message.meta.filename
  if (message.meta.type === 'audio') {
    filename = getPureNamefromFilename(filename) + 'mp3'
    message.meta.filename = filename
  }
  return filename
}
// es ar gamodis radgan message da request gvian igheben mniSvnelobas
function uploadMulter(response, storage, isChat, message) {
  changeAudioToMp3(message)
  let upload = multer({ storage })
  // $log.info('fU request.body = ' + JSON.stringify(request.body))
  $log.info('fU uploadMulter message = ' + message)
  // const fieldname = request.file.fieldname
  let upl = upload.single('blob')
  // let upl = upload.single(fieldname)
  upl(response, (err) => {
    // console.log('mlt req.body', indexData.request.body)
    if (err) {
      response.end('Error uploading file:' + err)
    } else {
      if (isChat) {
        $log.info('fU chat completed')
        insertFileMessage(message, response)
      } else {
        $log.info('fU questionnaire completed')
        response.json({ result: 'done', meta: message.meta })
      }
    }
  })
}
function createFileDir(imageDirBase, sender, avatar, error, success) {
  // shevinaxot file bazashi am misamartze:
  // imageDirBase = '../tfotos/' == 'https://kfz-soft.com/public/transporter/tfotos/'
  // imageDirBase/'userId/2019-8-20/1566329755060.jpg
  const dt = new Date()
  const dateday = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate()
  // let relDir = message.meta.sender + '/' + dateday + '/'
  let relDir = sender + '/' + dateday + '/'
  $log.info('fU avatar ' + avatar)
  if (avatar) {
    // message.meta.type === 'avatar'
    relDir = sender + '/avatar/'
    $log.info('fU relDir ' + relDir)
  }
  const fileDir = imageDirBase + relDir
  // shevqmnat fileDir if not exists
  createDirIfNotExists(fileDir, error, (fileDirCreated) => {
    // avatar-is shemtxvevashi clear
    if (avatar) {
      fs.emptyDirSync(fileDir)
    }
    success(relDir, fileDirCreated)
  })
}


// exports.createDirIfNotExists = createDirIfNotExists
function getExtension(file) {
  let parts = file.originalname.split('.')
  return parts.length > 1 ? parts.pop() : ''
}
// agar vxmarobt. chat xmarobda indexData = {socket, ss}
export function handlerFileDownload_Stream(indexData) {
  // stream = ss.createStream({hightWaterMark: 64 * 1024}); ar mushaobs
  return (message, callback) => {
    // relPath = /chats/1599498122935_2412702.jpeg
    let relPath = message.meta.relPath
    const _chatUrl = $mediaDirParams.chatUrl // -> /chats/
    const filename = relPath.replace(_chatUrl, '')
    // base = ../cfmedias/chats/
    const base = $mediaDirParams.base + $mediaDirParams.chatUrl
    const filepath = base + filename
    $log.info('fU filepath ' + filepath)
    if (!fs.existsSync(filepath)) {
      message.meta.error = 'File not exists! filepath = ' + filepath
      $log.info('fU message.meta.error ' + message.meta.error)
      callback(message)
      return
    } else {
      message.meta.type = $intents.DOWNLOADING
      callback(message)
    }
    let stream = indexData.ss.createStream()
    // mew mgon aq message-is gagzavna data-s saxit zedmetia, radgan zevit ukve gagzavnilia
    indexData.ss(indexData.socket).emit($events.downloadFileFromBack, stream, message, (callback) => {
      $log.info('fU ack' + callback)
    })
    // console.log(filepath);
    fs.createReadStream(filepath).pipe(stream)
  }
}
// ar vxmarobt

 */
