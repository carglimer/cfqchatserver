// const { getBase, JWTCategories, getValidationBase } = require('./httpErrorsBase')
import { getBase, JWTCategories, getValidationBase } from './httpErrorsBase'
function getIncorrectTokenContentError(payload) {
  let errTxt = 'Your token is not valid'
  if (payload) {
    errTxt = payload
  }
  return getBase('token', JWTCategories.content, errTxt, 400)
}
function getAdminIsOfflineError() {
  return getBase('contact', 'chat', 'Chatservice is offline. Please try again later', 400)
}
function getSignupError() {
  return getBase('auth', 'signup', 'This email already exists', 400)
}
function getAuthenticationError() {
  return getBase('auth', 'login', 'Email or password is incorrect', 400)
}
function getNoAccountError() {
  return getBase('auth', 'login', 'This account does not exist', 400)
}

function getRoleError() {
  return getValidationBase('role', 'Role is not valid')
}

function getUnknownProviderTokenError() {
  return getBase('token', 'provider', 'DeService is not own of your login provider token.', 400)
}

function getIncorrectProviderTokenError() {
  return getBase('token', 'provider', 'Your login provider token is incorrect', 400)
}

function getUnknownProviderError() {
  return getBase('token', 'provider', 'Unknown provider', 400)
}

export default {
  adminIsOfflineError: getAdminIsOfflineError(),
  signupError: getSignupError(),
  incorrectTokenContentError: getIncorrectTokenContentError,
  noAccountError: getNoAccountError(),
  authenticationError: getAuthenticationError(),
  roleError: getRoleError(),
  unknownProviderTokenError: getUnknownProviderTokenError(),
  incorrectProviderTokenError: getIncorrectProviderTokenError(),
  unknownProviderError: getUnknownProviderError()
}
