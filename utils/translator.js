// Anleitung -> https://cloud.google.com/translate/docs/reference/rest/v2/translate
// Imports the Google Cloud client library
// const { Translate } = require('@google-cloud/translate').v2
import tr from '@google-cloud/translate'
const { Translate } = tr.v2

// Creates a client. giorgim keyFilename sadme sxvagan unda shevinaxoto
// const translate = new Translate({ keyFilename: './utils/chat-translator-283515-b4d75095a01f.json' })
let translate /* = new Translate({
  // projectId: 'chat-translator-283515',
  // key: process.env.GOOGLE_TRANSLATE_API_KEY
  key: global.$googleTranslateApiKey // 'AIzaSyC1-qZlzliUx96RYmMtVuWY3r4IiHcbRwo'
}) */
//const text = 'Hello, world!' // the text to translate
// const target = 'ru' // target language
function initTranslate() {
  if (!translate) {
    translate = new Translate({
      key: process.env.GOOGLE_TRANSLATE_API_KEY
    })
  }
}
export const translateText = async function (text, from, to) {
  initTranslate()
  // await timeout(110)
  // Translates the text into the target language. "text" can be a string for translating a single piece of text, or an array of strings for translating multiple texts.
  const options = { from, to }
  // console.log('t options = ', options)
  // $log.info('t options = ' + options)
  try {
    // aq array modis da [translation]-shi translation stringia
    let [translatedText] = await translate.translate(text, { from, to })
    // $log.info('t translatedText:' + translatedText)
    return { translatedText }
  } catch (error) {
    $log.info('t translater error = ', error)
    // Promise.reject({ translationError: error })
    return { translationError: error }
  }
}
function timeout(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}
export const translateTextArray = async function (text, from, to) {
  initTranslate()
  // await timeout(110)
  try {
    let translations = await translate.translate(text, { from, to })
    // $log.info('translations = ' + JSON.stringify(translations[0]))
    translations = Array.isArray(translations) ? translations : [translations]
    // ეს მოდის -> [["Კარგი დღე","Რამდენი წლის ხარ?"],{"data":{"translations":[{"translatedText":"Კარგი დღე"},{"translatedText":"Რამდენი წლის ხარ?"}]}}]
    return { translatedText: translations[0] }
  } catch (error) {
    $log.info('t translater error = ', error)
    throw new Error(error)
    // return { translationError: error }
  }
}

/* translateText('Hello, world!', 'ru').then(result => {
  console.log('result = ', result)
})
function translate(data) {
  $log.info('translate fn')
  if (data.type !== $enums.dataTypes.text) {
    return Promise.resolve(data)
  } else {
    return _translateService(data.text, data.lang.from, data.lang.to)
  }
}
function _translateService(text, from, to) {
  return Promise.resolve(`[${to}] ${text}`)
}


*/
