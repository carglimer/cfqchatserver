// import winston from 'winston'
// const winston = require('winston')
// require('winston-daily-rotate-file')
// import winston from 'winston'
import * as winston from 'winston'
import 'winston-daily-rotate-file'

const { combine, timestamp, printf } = winston.format

const logFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`
})

var transport = new winston.transports.DailyRotateFile({
  filename: 'log-%DATE%',
  datePattern: 'YY.MM.DD',
  dirname: process.env.LOG_DIRECTORY, // directory where file will be saved
  extension: '.log', // file extension
  zippedArchive: false, // zip log files to avoid space problems
  maxSize: '5m', // no log file has more than 5 megabyte
  maxFiles: '10d' // delete a log after 10 days
})

const consoleTransport = new winston.transports.Console({ format: combine(timestamp(), logFormat) })
const exceptionTransports = process.env.NODE_ENV !== 'production' ? [transport, consoleTransport] : [transport]

export const logger = winston.createLogger({
  format: combine(timestamp(), logFormat),
  transports: exceptionTransports, // Write to all logs with to above transport
  exceptionHandlers: exceptionTransports, // handle all uncaught exceptions in separate file
  exitOnError: false
})
