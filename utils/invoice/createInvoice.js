// import { uploadInvoice, downloadInvoice } from '../helpers/axiosMethods'
import moment from 'moment'
import { invoiceParams } from '../enums'
import { formatDate } from './invoice'
import { stp, toFixed2, readLangJSonfile } from '../methods'
import { uploadInvoice } from './index'
import { find, updateOne } from '../MongoClient'
// murmanData
// {"recId":70,"orderDetails":{"recId":70,"plz":"01067","place":"Dresden","descrip":"basic","price":50,
//         "orderer": { "recId": 53, "nickname": "kkkk", "email": "k@gmail.com", "phone": "46845534" },
// "checker":{"recId":41,"nickname":"Marie","email":"b@gmail.com","phone":"+  41531325",
// "invoiceData":{"isJur":true,"firstName":"fgff ფდფგ","lastName":"","plz":"01108","place":"Dresden","address":"გრფგ","vatNumber":"45436543"}},
//                "admin": { "vatPerc": 19, "commissionPerc": 20 }}}
// $adminData ცალკეა გატანილი adminData.js-ში
const buyer = {}
// requestBody = murmanData
export async function create2Invoices(murmanData, response) {
  const checker = murmanData.orderDetails.checker // invoiceData = checker.invoiceData
  const orderer = murmanData.orderDetails.orderer
  const adminPercents = murmanData.orderDetails.admin
  // აქტუალიზება გავუკეთოთ პროცენტებს
  $adminData.invoiceData.vatRate = murmanData.orderDetails.admin.vatPerc
  $adminData.prices.G1 = murmanData.orderDetails.admin.commissionPerc

  // ჯერ ვქმნით ადმინის ინვოისს
  const adminInv = createOneInvoice(murmanData, true)
  // მერე ვქმნით ჩექერის ინვოისს
  const inv = createOneInvoice(murmanData, false)
  try {
    const invoiceNrData = await getAdminInvoiceNr(adminInv)
    await setAdminInvoiceNr(invoiceNrData)
    uploadInvoice({ inv, adminInv }, response, invoiceNrData)
  } catch (err) {
    $log.info('inv create2Invoices err = ' + err)
  }
  /* createOneInvoice(murmanData, true)
  if (adminInv) {
    getAdminInvoiceNr(adminInv).then((invoiceNrData) => {
      setAdminInvoiceNr(invoiceNrData).then(() => {
        uploadInvoice(request.body, response, invoiceNrData)
      })
    })
  } else uploadInvoice(request.body, response) // <- aq marto checkerIvoice aris */
}

function createOneInvoice(murmanData, admin) {
  $log.info('inv createInvoice admin = ' + admin)
  const serviceType = invoiceParams.check
  // const cardDetails = {}
  const issuerAndReceiver = createIssuerAndReceiver(admin, serviceType, murmanData)
  const issuer = issuerAndReceiver.issuer // invoice emmitent
  const receiver = issuerAndReceiver.receiver // invoice receiver
  // const itemValues = getItemValues(serviceType, murmanData)
  // const date = moment.unix(Date.now() / 1000).format('DD.MM.YY') // '08/12/2020',
  // const dueDate = moment.unix(Date.now() / 1000).format('DD.MM.YY') // '18/12/2020',
  const ts = Date.now()
  const date = ts
  const dueDate = ts
  const nr = admin ? null : createInvoiceNr(issuer.recId)
  const vat = issuer.invoiceData.vatRate
  let checkerPrice
  if (serviceType === invoiceParams.check) {
    checkerPrice = murmanData.orderDetails.price // itemValues.price
  } /* else if (serviceType === invoiceParams.resell) {
    checkerPrice = toFixed2(cardDetails.priceChecker)
  } */
  const price = admin ? getAdminPrice(serviceType, murmanData) : checkerPrice
  $log.info('inv checkerPrice = ' + checkerPrice)
  $log.info('inv price = ' + price)
  const level = murmanData.orderDetails.descrip
  const invoice = {
    cardId: murmanData.recId,
    admin: !!admin, // : issuer.admin
    serviceType: serviceType,
    ts,
    date,
    dueDate,
    nr,
    price,
    level,
    from: {
      nickname: { value: issuer.nickname, show: true },
      customerNr: { value: issuer.recId, show: false },
      vatCode: { value: stp(issuer.invoiceData.vatNumber), show: issuer.invoiceData.isJur },
      name: { value: getName(issuer.invoiceData), show: false },
      address: { value: stp(issuer.invoiceData.address), show: false },
      zipLocation: {
        value: stp(issuer.invoiceData.plz) + ' ' + stp(issuer.invoiceData.place),
        show: true
      },
      country: { value: getCountryFromCountryAlpha2(issuer.invoiceData.countryAlpha2), show: true }
    },
    to: {
      nickname: { value: receiver.nickname, show: true },
      customerNr: { value: receiver.recId, show: admin },
      vatCode: { value: stp(receiver.invoiceData.vatNumber), show: receiver.invoiceData.isJur },
      name: { value: getName(receiver.invoiceData), show: false },
      address: { value: stp(receiver.invoiceData.address), show: false },
      zipLocation: {
        value: receiver.invoiceData ? stp(receiver.invoiceData.plz) + ' ' + stp(receiver.invoiceData.place) : '',
        show: true
      },
      country: { value: receiver.invoiceData ? stp(getCountryFromCountryAlpha2(receiver.invoiceData.countryAlpha2)) : '', show: true }
    },
    items: [
      {
        serviceName: invoiceParams.G1, // itemValues.serviceName,
        description: { description: invoiceParams.check, level }, // itemValues.description,
        // speed: itemValues.speed,
        // level: itemValues.level,
        // quantity: 1,
        price
      }
    ],
    data: {
      agreed: false, // tu daetanxma gamgzavni kvela pirobas sakutari chascorebebis gatvaliscinebit
      nr,
      date: moment.unix(date / 1000).format('DD/MM/YY'), // '08/12/2020',
      dueDate: moment.unix(dueDate / 1000).format('DD/MM/YY'), // '18/12/2020',
      paymentType: 'paypal',
      paid: 0,
      currency: '€',
      juridical: issuer.invoiceData.isJur,
      vat, // : checker.invoiceData.vatRate, // '16',
      total: price,
      notes: {
        vatNoteValue: ''
      }
    }
  }
  addHeaders(invoice)
  addTexts(invoice)
  // $log.info('inv invoice = ' + JSON.stringify(invoice))
  // მურმან -> შენ რომ გამოიძახებ qchat/resellCards-ს და მიიღებ ინფორმაციას ამ resellCard-ის შესახებ და როდესაც გამოსაგზავნ ობიექტში issuer.role="admin",
  // მაშინ admin: {}-ის დონეზე ჩამიდე კიდევ ერთი ელემენტ-ობიექტი: inform: { checkerId: checker.recId, price: priceChecker }
  /* if (serviceType === invoiceParams.resell) {
    invoice.checkerId = cardDetails.checker.recId
    invoice.priceChecker = toFixed2(cardDetails.priceChecker)
  } */
  // this.invoice = invoice
  // this.editable && this.initEditableData()
  // this.initInvoiceItems()
  // this.initInvoiceValues()
  return invoice
}

// murmanData
// {"recId":70,"orderDetails":{"recId":70,"plz":"01067","place":"Dresden","descrip":"basic","price":50,
//         "orderer": { "recId": 53, "nickname": "kkkk", "email": "k@gmail.com", "phone": "46845534" },
// "checker":{"recId":41,"nickname":"Marie","email":"b@gmail.com","phone":"+  41531325",
// "invoiceData":{"isJur":true,"firstName":"fgff ფდფგ","lastName":"","plz":"01108","place":"Dresden","address":"გრფგ","vatNumber":"45436543"}},
//                "admin": { "vatPerc": 19, "commissionPerc": 20 }}}
function createIssuerAndReceiver(admin, serviceType, murmanData) {
  let issuer = {}
  let receiver = {}
  if (serviceType === invoiceParams.check) {
    if (admin) {
      // admin to checker
      issuer = $adminData
      // ამას invoiceData მოჰყვება
      receiver = murmanData.orderDetails.checker
    } else {
      // checker to orderer
      issuer = murmanData.orderDetails.checker
      if (issuer.invoiceData.isJur) {
        // glimer vatRate არის სხვებისთვისაც და აღარ ვანსხვავებთ
        issuer.invoiceData.vatRate = $adminData.invoiceData.vatRate
        issuer.invoiceData.countryAlpha2 = 'de'
      }
      // orderer-ს invoiceData არ მოჰყვება და ამიტომ დავამატოთ
      receiver = murmanData.orderDetails.orderer
      receiver.invoiceData = {
        isJur: false,
        countryAlpha2: 'de',
        firstName: 'de',
        lastName: 'de',
        plz: '',
        vatNumber: '',
        address: '',
        place: ''
      }
    }
  } /* else if (serviceType === invoiceParams.resell) {
    if (admin) {
      // admin to buyer -> admin amocers 12-evroian invoice-s
      issuer = adminData
      receiver = cardDetails.buyer
    } else {
      // checker to admin  -> checker amoucers admin 6-evroian invoice-s
      issuer = cardDetails.checker
      receiver = adminData
    }
  } */
  return { issuer, receiver }
}

function getItemValues(serviceType, cardDetails) {
  let serviceName, description // description object არის, საიდანაც მიიღება -> Fahrzeugcheck-Normal-Allgemein
  const speed = cardDetails.speed
  const level = cardDetails.level
  const price = toFixed2(cardDetails.price)

  switch (serviceType) {
    case invoiceParams.check:
      serviceName = invoiceParams.G1
      description = { description: invoiceParams.check, speed, level }
      break
    case invoiceParams.resell:
      serviceName = invoiceParams.G2
      description = { description: invoiceParams.resell, level }
      break
    default:
      serviceName = invoiceParams.G1
      description = { description: invoiceParams.check, level }
      break
  }
  return {
    serviceName,
    description, // speed, level,
    price
  }
}
// client invoiceNr
function createInvoiceNr(customerId) {
  // Rechnung-Nr: 3-250221.174533
  const formatedTs = moment.unix(Date.now() / 1000).format('DDMMYY.HHmmss') // 'DD.MM.YY HH:mm:ss'
  return customerId + '-' + formatedTs
}
function getName(invoiceData) {
  let name
  if (invoiceData.isJur) {
    name = invoiceData.firstName
  } else name = invoiceData.firstName + ' ' + invoiceData.lastName
  return name
}
function getAdminPrice(serviceType, murmanData) {
  // const serviceName = invoiceParams.G1
  // dasamushavebelia
  // const prices = ctx.gdata.adminData.prices
  // const adminPrice = (cardDetails.price * prices[serviceName]) / 100
  let adminPrice
  if (serviceType === invoiceParams.check) {
    // const ourComissionRate = user && user.ourComission && user.ourComission.otherall && user.ourComission.otherall.rate ? user.ourComission.otherall.rate : prices[serviceName]
    // adminPrice = (murmanData.orderDetails.price * murmanData.orderDetails.admin.commissionPerc) / 100
    adminPrice = (murmanData.orderDetails.price * $adminData.prices.G1) / 100
  } /* else if (serviceType === invoiceParams.resell) {
    // mashin cardDetails.price igive adminis price aris
    adminPrice = cardDetails.priceBuyer // asec sheizleba -> prices.adminResell
  } */
  $log.info('inv adminPrice = ' + adminPrice)
  return adminPrice
}

export function getAdminInvoiceNr(invoice) {
  // kovel dges axlidan vnomravt
  // $log.info('inv invoice = ' + invoice)
  // $log.info('inv invoice.admin = ' + invoice.admin)
  if (invoice.admin) {
    // aq mxolod erti chanaceri iqneba da camovighot
    const data = { collName: 'invoicenrs', query: {} }
    return new Promise((resolve, reject) => {
      find(data)
        .then((invoicenrArr) => {
          // $log.info('po handler questionnaireValuesArr = ' + questionnaireValuesArr)
          const now = Date.now()
          const day = formatDate(now) // moment.unix(Date.now() / 1000).format('DD.MM.YY')
          let nr = 1
          let _id
          if (invoicenrArr && invoicenrArr.length) {
            const invoicenr = invoicenrArr[0]
            _id = invoicenr._id
            // invoicenr = { day: '12.05.21', nr:3 }
            $log.info('inv today ' + day)
            // const day = today
            if (invoicenr.day === day) {
              nr = +invoicenr.nr + 1
            }
          }
          // adminInvNr -> 120921-9
          resolve({ day, nr, _id, adminInvNr: moment.unix(now / 1000).format('DDMMYY') + '-' + nr })
        })
        .catch((err) => {
          $log.info('inv err = ' + err)
          reject(err)
        })
    })
  }
}
// invoiceNrData = { day, nr, _id }
export function setAdminInvoiceNr(invoiceNrData) {
  let data = { collName: 'invoicenrs' }
  // data.query = { _id: -2 } // ar aqvs mnishvneloba _id ras udris. mtavaria aseti ar ikos, rom upsert-ma imushaos
  if (invoiceNrData._id) {
    data.query = { _id: invoiceNrData._id }
  } else data.query = {}
  data.update = { $set: { day: invoiceNrData.day, nr: invoiceNrData.nr, ts: Date.now() } }
  data.options = { upsert: true }
  return new Promise((resolve, reject) => {
    updateOne(data)
      .then((resp) => {
        $log.info('po resp ' + JSON.stringify(resp))
        // const questionnaireValues = res.ops[0]
        if (resp) {
          if (!resp.update) {
            $log.info('po resp.update ' + resp.update)
            // mashin insert moxda
          }
          resolve()
        }
      })
      .catch((err) => {
        $log.info('po err = ' + err)
        reject(err)
      })
  })
}
function getCountryFromCountryAlpha2(code) {
  return 'Deutschland' // ctx.readCountryFromCountryCodesJSonfile(code)
}
function addHeaders(inv) {
  inv.headers = [
    { name: 'serviceName', header: 'service_name', text: '', width: '10%' },
    { name: 'description', header: 'description', text: '', width: '75%' }, // editable: false
    // { name: 'speed', header: 'speed', text: '', width: '20%' },
    // { name: 'level', header: 'level', text: '', width: '20%' },
    { name: 'bruttoValue', header: 'price', text: 0, width: '15%' }
  ]
}
function addTexts(inv) {
  const textsDe = readLangJSonfile('de').qchat.invoice
  const textsEn = readLangJSonfile('en').qchat.invoice
  const texts = { de: textsDe, en: textsEn }
  inv.texts = texts
}
