import { downloadInvoicePdf } from './invoice'
import { create2Invoices } from './createInvoice'
// import { acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire } from '../../chat/chat'
import { find, updateOne } from '../MongoClient'

export function createInvoice(requestBody, response) {
  Object.keys(requestBody).forEach((key) => {
    const value = requestBody[key]
    $log.info('po createInvoice key = ' + key)
    $log.info('po createInvoice value = ' + value)
  })

  $log.info('po createInvoice recId = ' + Object.keys(requestBody).length)
  $log.info('po createInvoice murmanData = ' + JSON.stringify(requestBody))
  create2Invoices(requestBody, response)
  // response.send('create invoice request received')
}

// request = request.body
export async function downloadInvoice(requestBody, response) {
  let invoiceData = requestBody // invoiceData = { cardId: '12365', lang: 'de', admin: true||false }
  $log.info('po invoiceData = ' + JSON.stringify(invoiceData))
  // let data = { meta: { collName: 'invoices' } }
  const data = {
    collName: 'invoices',
    query: { $and: [{ cardId: invoiceData.cardId }, { admin: !!invoiceData.admin }] }
  }
  // let options = { options: {} }
  // options.query = { cardId: invoiceData.cardId }
  /* if (invoiceData.admin) {
    data.query = { $and: [{ cardId: invoiceData.cardId }, { admin: invoiceData.admin }] }
  } */
  // data.options = options
  try {
    const invoices = await find(data)
    if (invoices && invoices.length) {
      $log.info('po downloadInvoice find invoices.length = ' + invoices.length)
      // $log.info('po downloadInvoice invoice = ' + JSON.stringify(invoices[0]))
      const invoice = invoices[0]
      downloadInvoicePdf(response, { invoice, invoiceData })
    } else if (response) {
      response && response.setHeader('Content-Type', 'application/json')
      // response.send({ error: $etexts.downloadInvoiceFailed })
      response && response.send({ error: $errors.no_invoice })
    }
  } catch (err) {
    $log.info('ii downloadInvoice err: ' + err.toString())
    response && response.send({ error: $errors.failed, err: err.toString() })
  }
}
function initMongoInvoiceUpdateParams(inv) {
  return {
    ...initMongoInvoiceFindParams(inv),
    update: { $set: inv },
    options: { upsert: true }
  }
}
function initMongoInvoiceFindParams(inv) {
  return {
    collName: 'invoices',
    query: { $and: [{ cardId: inv.cardId }, { serviceType: inv.serviceType }, { admin: inv.admin }] }
  }
}
function addInvoiceLang(inv, invData) {
  // push notification-istvis ena gavarkviot
  if (inv.to && inv.to && inv.to.country && inv.to.country.value && inv.to.country.value === 'Deutschland') {
    invData.lang = 'de'
  } else {
    invData.lang = 'en'
  }
}
// Murman: როდესაც ვიძახებ DesInvoice კომპონენტს, გადავცემ პარამეტრს :create. true - საჭიროა შექმნა, false - მხოლოდ არსებულის ჩვენება.
// შექმნის და ატვირთვის მერე განათავსებ რა mongo-ში ინვოისს, დაუკავშირდები back-ს POST-ით url-ზე: api.check-first.net/api/qchat/invoice
// პარამეტრად ობიექტი: {cardId: cardId}. უნდა დაგიბრუნოს უკან რესპონსი res, სადაც res.data = cardId.
// request.body = invoice   invoiceNrData = { day, nr, _id }
// აქ მოდის ორი ინვოისი ერთდროულად { inv, adminInv } ან { inv } ან { adminInv }
export async function uploadInvoice(requestBody, response, invoiceNrData) {
  // const cfBackData = $cfBackData.host // https://api.deservice.net/api/qchat/
  let inv = requestBody.inv
  let adminInv = requestBody.adminInv
  if (!inv && !adminInv) {
    response.send({ error: $etexts.noInvoiceDataInRequestObject })
    return
  }
  const updateInvoicePromises = []
  // $log.info('ii inv = ' + JSON.stringify(inv))
  // $log.info('ii adminInv = ' + JSON.stringify(adminInv))
  if (inv) {
    updateInvoicePromises.push(updateOne(initMongoInvoiceUpdateParams(inv)))
  }
  if (adminInv) {
    if (invoiceNrData) {
      // invoiceNrData -> { day, nr, _id, adminInvNr }
      // Nr -> 120921-9
      adminInv.nr = invoiceNrData.adminInvNr
    } else {
      response.send({ error: $etexts.invoiceNrFailed })
      return
    }
    updateInvoicePromises.push(updateOne(initMongoInvoiceUpdateParams(adminInv)))
  }
  try {
    const respArr = await Promise.all(updateInvoicePromises)
    if (respArr) {
      if (inv && adminInv) {
        if (respArr[0] && !respArr[0].update) {
          // mashin insert moxda
          inv._id = respArr[0].insertedId
        }
        if (respArr[1] && !respArr[1].update) {
          adminInv._id = respArr[1].insertedId
        }
      } else if (inv && respArr[0] && !respArr[0].update) {
        inv._id = respArr[0].insertedId
      } else if (adminInv && respArr[0] && !respArr[0].update) {
        adminInv._id = respArr[0].insertedId
      }

      // gavagebinet mainBack-s rom invoice atvirtulia
      // მურმან -> როდესაც ინვოისს ქმნი და იძახებ back-ის qchat/invoice-ს გადმომცემ პარამეტრად cardId-ს
      // გადმომეცი აქვე amount: ინვოისის თანხა, our_commission: ჩვენი საკომისიოს თანხა. ფასების დამრგვალებით
      // { cardId, main: { price, vat }, admin: { price, vat } }.
      const invData = {}
      invData.cardId = inv ? inv.cardId : adminInv.cardId
      invData.serviceType = inv ? inv.serviceType : adminInv.serviceType
      if (inv) {
        invData.main = { price: inv.price, vat: inv.data.vat }
        invData.issuer = { recId: inv.from.customerNr.value, role: 'checker', nickname: inv.from.nickname.value }
        // push notification-istvis ena gavarkviot
        addInvoiceLang(inv, invData)
      }
      if (adminInv) {
        invData.admin = { price: adminInv.price, vat: adminInv.data.vat }
        if (!inv) {
          invData.issuer = { role: 'admin' }
          // მურმან -> შენ რომ გამოიძახებ qchat/resellCards-ს და მიიღებ ინფორმაციას ამ resellCard-ის შესახებ და როდესაც გამოსაგზავნ ობიექტში issuer.role="admin",
          // მაშინ admin: {}-ის დონეზე ჩამიდე კიდევ ერთი ელემენტ-ობიექტი: inform: { checkerId: checker.recId, price: priceChecker }
          invData.inform = { checkerId: adminInv.checkerId, price: adminInv.priceChecker }
        }
        // push notification-istvis ena gavarkviot
        addInvoiceLang(adminInv, invData)
      }
      // invData = {"cardId":70,"serviceType":"check","main":{"price":50},"issuer":{"recId":41,"role":"checker","nickname":"Marie"},"lang":"de","admin":{"vat":19}}
      $log.info('ii invData = ' + JSON.stringify(invData))
      const resp = { cardId: invData.cardId }
      if (inv) {
        resp.amount = inv.price
      }
      if (adminInv) {
        resp.adminAmount = adminInv.price
      }
      $log.info('ii resp = ' + JSON.stringify(resp))
      response && response.send(resp)

      /* acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire(invData, $caller.invoice)
          .then((respData) => {
            $log.info('po acknowlidged MainBack respData ' + respData)
            // mivighet murmanisgan, rom chacera bazashi es invoice -> respData = "Mark as invoiced and Posted for notification!"
            // response.send(respArr) // resp = { update: false||true, insertedId: upsertedId||null }
            response.send({ cardId: invData.cardId, created: true })
          })
          .catch((err) => {
            $log.info('i acknowlidgeMainBackAboutInvoiceMessageOrQuestionnaire error: ' + err)
            // თუ შეცდომა მოხდა მურმანისთან, მაშინ უნდა წავშალოთ რაც ჩავწერეთ
            const removeInvoicePromises = []
            if (inv) {
              removeInvoicePromises.push(deleteMany(initMongoInvoiceFindParams(inv)))
            }
            if (adminInv) {
              removeInvoicePromises.push(deleteMany(initMongoInvoiceFindParams(adminInv)))
            }
            Promise.all(removeInvoicePromises)
              .then((respArr) => {
                $log.info('po removeInvoicePromises respArr: ' + JSON.stringify(respArr)) // respArr: [{"n":1,"ok":1},{"n":1,"ok":1}]
                response.send({ error: $etexts.uploadInvoiceFailed, err: err.toString() })
              })
              .catch((err) => {
                $log.info('po removeInvoicePromises err: ' + err.toString())
                response.send({ error: $etexts.uploadInvoiceFailed, err: err.toString() })
              })
          }) */
    } else {
      response && response.send({ error: $etexts.uploadInvoiceFailed })
    }
  } catch (err) {
    $log.info('po err1: ' + err.toString())
    response && response.send({ error: $etexts.uploadInvoiceFailed, err: err.toString() })
  }
}
export function getInvoiceByCardIdAndServiceType(requestBody, response) {
  // requestBody = { cardId: 124, serviceType }  serviceType = check|resell
  // ert cardId-ze aris 2 invoice checkeris da adminis
  // to requestBody.admin === undefined, mashin orives camoighebs
  // const data = { collName: 'invoices', query: { cardId: requestBody.cardId } }
  const data = { collName: 'invoices', query: { $and: [{ cardId: requestBody.cardId }, { serviceType: requestBody.serviceType }] } }
  if (requestBody.admin !== undefined) {
    data.query = { $and: [{ cardId: requestBody.cardId }, { serviceType: requestBody.serviceType }, { admin: requestBody.admin }] }
  }
  // $log.info('po getInvoiceByCardIdAndServiceType data = ' + data)
  find(data)
    .then((invoiceArr) => {
      // $log.info('po handler invoiceArr = ' + invoiceArr)
      if (invoiceArr && invoiceArr.length) {
        $log.info('po handler find invoiceArr[0] = ' + invoiceArr[0])
        response && response.send(invoiceArr)
      } else {
        response && response.send($etexts.noInvoiceFound)
      }
    })
    .catch((err) => {
      console.error('getInvoiceByCardIdAndServiceType po error: ', err)
      response && response.send({ error: $etexts.getInvoiceFailed, err: err.toString() })
    })
}
