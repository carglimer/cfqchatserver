// const { find } = require('./MongoClient')
import { find } from './MongoClient'

// es tvirtavs users admistvis da makers klientistvis
export function handleLoadAllItems(indexData) {
  $log.info('ad indexData.message ' + JSON.stringify(indexData.message))
  let permittedCollnames = ['users', 'makers']
  // marto dasashvebi collection-ebis chamotvirtva unda sheizlebodes
  let permitted = false
  for (const el of permittedCollnames) {
    $log.info('ad el ' + el)
    if (indexData.message.meta.collName === el) {
      permitted = true
      break
    }
  }
  if (!permitted) {
    indexData.response.send({ itemList: [] })
    return
  }
  if (indexData.message.collName === 'users') {
    // marto adminma unda shezlos user-ebis chamotvirtva
    if (indexData.message.admin !== 'true') {
      indexData.response.send({ itemList: [] })
      return
    }
  }
  let data = { meta: { collName: indexData.message.meta.collName } } // users
  let options = {}
  // statusInfo:{sender : userId, id:'', receiver : '101', status:'notSent', tsList:{}}
  // options.query = { 'statusInfo.receiver': receiverId, 'statusInfo.status': { $ne: status.received } }
  if (indexData.message.meta.query) {
    options.query = indexData.message.meta.query
  } else {
    options.query = {}
  }
  data.options = options
  find(data, $mongo)
    .then((items) => {
      if (items && items.length) {
        $log.info('ad items.length ' + items.length)
        // gavugzavnet mimghebs mistvis gankutvnili items
        indexData.response.send({ itemList: items })
        // callback(users)
      } else {
        indexData.response.send({ itemList: [] })
      }
    })
    .catch((err) => {
      $log.error('ad Failed to find items ' + err)
      indexData.response.send(err)
    })
}
export function handleLoadAllUsers(indexData) {
  $log.info('ad indexData.message ' + JSON.stringify(indexData.message))
  let data = { meta: { collName: indexData.message.meta.collName } } // users
  let options = {}
  // statusInfo:{sender : userId, id:'', receiver : '101', status:'notSent', tsList:{}}
  // options.query = { 'statusInfo.receiver': receiverId, 'statusInfo.status': { $ne: status.received } }
  options.query = {}
  data.options = options
  find(data, $mongo)
    .then((users) => {
      if (users && users.length) {
        $log.info('ad users ' + users)
        // gavugzavnet mimghebs mistvis gankutvnili users
        indexData.response.send({ userList: users })
        // callback(users)
      } else {
        indexData.response.send({ userList: [] })
      }
    })
    .catch((err) => {
      $log.info('ad Failed to find users: ' + err)
      indexData.response.send(err)
    })
}
export function handleLoadUserAllOrders(indexData) {
  $log.info('ad indexData.message ' + JSON.stringify(indexData.message))
  let data = { meta: { collName: indexData.message.meta.collName } } // orders
  let options = {}
  /* message asetia:
  {
    meta: { type: 'text', collName: 'orders', intent:'loadUserOrdersForAdmin' },
    data: { author: 'admin', userId: user_id },
    admin: true
  } */
  // statusInfo:{sender : userId, id:'', receiver : '101', status:'notSent', tsList:{}}
  // options.query = { 'statusInfo.receiver': receiverId, 'statusInfo.status': { $ne: status.received } }
  // options.query = {sender: { $in: [senderId, receiverId] },receiver: { $in: [senderId, receiverId] }}
  options.query = { userId: indexData.message.data.userId }
  data.options = options
  find(data, $mongo)
    .then((orders) => {
      if (orders && orders.length) {
        $log.info('ad orders ' + orders)
        // gavugzavnet mimghebs mistvis gankutvnili messages
        indexData.response.send({ orderList: orders })
      } else {
        indexData.response.send({ orderList: [] })
      }
    })
    .catch((err) => {
      $log.info('ad Failed to find user orders: ')
      indexData.response.send(err)
    })
}
