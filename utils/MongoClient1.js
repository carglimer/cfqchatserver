import { MongoClient as Mongo_Client } from 'mongodb'

// const url_centos = 'mongodb://admin:imereti1@136.243.102.152:27017/dbName?authSource=admin'
// const url_local = 'mongodb://admin:imereti1@localhost:27017/dbName?authSource=admin'
// mongodb 4.16.0
// api აქ არის -> https://mongodb.github.io/node-mongodb-native/4.16/
export class MongoClient {
  constructor(url, dbName, emitter, user) {
    // url = url.replace('dbName', dbName)
    $log.info('mC Connecting to MongoDB url = ' + JSON.stringify(url))
    const client = new Mongo_Client(url, {})
    client
      .connect()
      .then((db) => {
        $log.info('mC db = ' + db)
        if (db && user) {
          // mashin user jer ar sheqmnila
          db.createUser(user)
        }
        this.dbn = client.db(dbName) // esec sheizleba
        // this.dbn = db.db(dbName) // esec sheizleba
        emitter.emit($events.mongo_connected)
        $log.info('mC Connected to MongoDB db = ' + this.dbn)
      })
      .catch((err) => {
        emitter.emit('mongo-error', err)
        // throw err
      })

    /* Mongo_Client.connect(
      url,
      {
        native_parser: true
      },
      (err, db) => {
        if (err) {
          $log.error('mc Mongo DB Connection Error2: err.message = ' + err.message)
        } else {
          $log.info('mC Connected to MongoDB db = ' + db.collection)
          this.dbn = db
          emitter.emit($events.mongo_connected)
        }
      }
    )*/
    /* .then((err, db) => { */
    /*   $log.info('mC Connected to MongoDB db = ' + db) */
    /*   $log.info('mC db.db(dbName) = ' + db.db(dbName)) */
    /*   // Set db constants */
    /*   this.dbn = db.db(dbName) */
    /*   emitter.emit($events.mongo_connected) */
    /* }) */
    /* .catch((err) => { */
    /*   $log.info('mC Mongo DB Connection Error2: err.message ' + err.message) */
    /*   $log.error('mc Mongo DB Connection Error2: err.message = ' + err.message) */
    /*   // throw err */
    /* }) */
  }
  getCollection(collectionName) {
    // console.log('this.dbn =', this.dbn !== undefined)
    return this.dbn.collection(collectionName)
  }
}

export function find(data) {
  // data = { collName: 'invoices', query: {...}, sort:{...}, projection : { "id": 0, name:1 romeli velebi gvinda-1 tu ara-0}}
  $log.info('mc mongo find data = ' + JSON.stringify(data))
  return new Promise((resolve, reject) => {
    if (!$mongo) {
      const errorMessage = 'mongo not initialzed'
      $log.info(errorMessage)
      reject(new Error(errorMessage))
    }
    const coll = $mongo.getCollection(data.collName)
    coll
      .find(data.query, data.projection)
      // .sort({ name: 1 })
      .toArray()
      .then((items) => {
        if (items.length) {
          $log.info('Successfully found ${items.length} documents: ' + items.length)
        } else {
          $log.info($etexts.noDocumentsFound)
        }
        //items.forEach(console.log)
        resolve(items)
      })
      .catch((err) => {
        $log.info($etexts.noDocumentsFound + ' err: ' + err)
        reject(err)
      })
  })
}
export function find2(data) {
  // data.meta = { collName: 'invoices'}
  // data.options = { query: {...}, sort:{...}, projection : { "id": 0, name:1 romeli velebi gvinda-1 tu ara-0}}
  $log.info('data.options= ' + JSON.stringify(data.options))
  const coll = $mongo.getCollection(data.meta.collName)
  return new Promise((resolve, reject) => {
    coll
      .find(data.options.query, data.options.projection)
      // .sort({ name: 1 })
      .toArray()
      .then((items) => {
        if (items.length) {
          $log.info('Successfully found ${items.length} documents: ' + items.length)
        } else {
          $log.info($etexts.noDocumentsFound)
        }
        //items.forEach(console.log)
        resolve(items)
      })
      .catch((err) => {
        $log.info($etexts.noDocumentsFound + ' err: ' + err)
        reject(err)
      })
  })
}

export function updateOne(data) {
  // $log.info('updateOne data = ' + JSON.stringify(data))
  const coll = $mongo.getCollection(data.collName)
  return new Promise((resolve, reject) => {
    coll
      .updateOne(data.query, data.update, data.options)
      .then((result) => {
        $log.info('mc updateOne result = ' + JSON.stringify(result))
        const { matchedCount, modifiedCount, upsertedId } = result
        if (matchedCount || upsertedId) {
          $log.info('mc Successfully updated a new review.')
          // emitter.emit('updated.one', data.id)...
          let resp = { update: false }
          if (matchedCount && modifiedCount) {
            // document gefunden und geändert
            resp.update = true
          } else if (upsertedId) {
            // document nicht gefunden und inserted
            // upsertedId = { index: 0, _id: '66345e8ed3bf603111ae0f9e' }
            let insertedId = upsertedId
            if (global.$methods.isObject(insertedId)) {
              insertedId = upsertedId._id
            }
            resp.update = false
            resp.insertedId = insertedId
          }
          // wenn document gefunden und nicht geändert, bleibt update: false
          resolve(resp)
        }
      })
      .catch((err) => {
        $log.info('mc updateOne err = ' + $etexts.updateDocumentFailed)
        reject(err)
      })
  })
}

export function updateMany(data) {
  // $log.info('updateOne data = ' + JSON.stringify(data))
  const coll = $mongo.getCollection(data.collName)
  return new Promise((resolve, reject) => {
    coll
      .updateMany(data.query, data.update, data.options)
      .then((result) => {
        $log.info('mc updateOne result = ' + JSON.stringify(result))
        const { matchedCount, modifiedCount, upsertedId } = result
        if (matchedCount || upsertedId) {
          $log.info('mc Successfully updateMany a new review.')
          // emitter.emit('updated.one', data.id)...
          let resp = { update: false }
          if (matchedCount && modifiedCount) {
            // document gefunden und geändert
            resp.update = true
          } else if (upsertedId) {
            // document nicht gefunden und inserted
            resp.update = false
            resp.insertedId = upsertedId._id
          }
          // wenn document gefunden und nicht geändert, bleibt update: false
          resolve(resp)
        }
      })
      .catch((err) => {
        $log.info('mc updateMany err = ' + $etexts.updateDocumentFailed)
        reject(err)
      })
  })
}

export function insertOne(data) {
  $log.info('saveIntoDB')
  const coll = $mongo.getCollection(data.meta.collName)
  // orjer rom ar chaceros ts unique aris
  // coll.createIndex({ ts: data.ts }, { unique: true })
  return new Promise((resolve, reject) => {
    coll
      .insertOne(data)
      .then((res) => {
        resolve(res.insertedId)
      })
      .catch((err) => {
        $log.info('mongo insertOne err ' + err.toString())
        reject(err)
      })
  })
}

export function deleteMany(data) {
  $log.info('removefromDB data = ' + JSON.stringify(data))
  const coll = $mongo.getCollection(data.collName)
  return new Promise((resolve, reject) => {
    coll
      .deleteMany(data.query)
      .then((res) => {
        // res = {"result":{"n":1,"ok":1},"connection":{"_events":{},"_eventsCount":4,"id":1,"address":"127.0.0.1:27017","bson":{},"socketTimeout":0,"host":"localhost","port":27017,
        // "monitorCommands":false,"closed":false,"destroyed":false,"lastIsMasterMS":5},"deletedCount":1,"n":1,"ok":1}
        $log.info('mongo res.result = ' + res.result)
        resolve(res.result)
      })
      .catch((err) => {
        $log.info('mongo remove err = ' + err.toString())
        reject(err)
      })
  })
}
