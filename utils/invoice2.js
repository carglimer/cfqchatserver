import fs from 'fs-extra'
import moment from 'moment'
import PDFDocument from 'pdfkit'
import { find, updateOne } from './MongoClient'

let doc
let inv
let texts
let x = 0
let y = 0
const step = 15
const left = { align: 'left' }
const right = { align: 'right' }
const center = { align: 'center' }
// https://stackoverflow.com/questions/58090447/expressjs-and-pdfkit-generate-a-pdf-in-memory-and-send-to-client-for-download
export function downloadInvoicePdf(res, { invoice, invoiceData }) {
  // console.log('inv = ', JSON.stringify(inv))
  inv = invoice
  texts = invoice.texts[invoiceData.lang]
  // console.log('texts = ', JSON.stringify(texts))
  // const fs = require('fs-extra')
  // var PDFDocument = require('pdfkit')
  doc = new PDFDocument({ size: 'A4', margin: 50 })
  try {
    createInvoicePdf()
    // gavugzavnet klients
    res.setHeader('Content-Type', invoice.nr + '/pdf')
    // res.setHeader('Set-Cookie', ['testnr'])
    doc.pipe(res) // doc.pipe(res) ar aqvs mnishvneloba sad davcert aq tu createInvoicePdf()-mde
    doc.end()
  } catch (error) {
    $log.info('inv error = ' + error)
    res.send({ error: error })
    return
  }
  /* if (res.test2) {
    const date = new Date().getTime()
    const filename = 'C:\\Users\\Andreas\\Desktop\\My Scanns\\' + date + '.pdf'
    $log.info('inv filename = ' + filename)
    doc.pipe(fs.createWriteStream(filename))
  } else  */
  // console.log('doc.end()')
}

function createInvoicePdf() {
  generateUserHeader()
  generateCustomerInformation()
  generateInvoiceData()
  generateInvoiceHeader()
  generateInvoiceTable()
  generateVatNote()
  generatePaymentType()
  generateInvoiceNote()
  // generateFooter(doc)
}
function sr(prop) {
  if (!inv) {
    return null
  }
  const keyArr = prop.split('.')
  let val = inv
  for (const key of keyArr) {
    if (val) {
      val = val[key]
    }
  }
  if (!val && prop.includes('value')) {
    return ''
  }
  return val
}
// from = user
function generateUserHeader() {
  x = 0
  y = 50
  const from = inv.from
  $log.info('inv inv.from = ' + JSON.stringify(inv.from))
  if (inv.admin) {
    // doc.image('car.jpg', 50, 45, { width: 50 })
    // favicon.ico -> deslogo aris
    doc.image('favicon.jpg', 50, 45, { width: 50 })
  }
  doc
    .fillColor('#444444') // teqstis feri
    .fontSize(10)
  // .text(texts.customer_nr + ': ' + sr('from.customerNr.value'), x, y, right)
  if (from.customerNr.value) {
    doc.text(texts.customer_nr + ': ' + sr('from.customerNr.value'), x, y, right)
    y += step
  } else {
    // mashin adminia da ar vcert
  }
  // y += step
  // doc.text(texts.nickname + ': ' + sr('from.nickname.value'), x, y, right)

  if (!from.nickname || from.nickname.value === 'adminGlimer') {
    // mashin adminia da dasaxelebas vcert
    doc.text(sr('from.name.value'), x, y, right)
  } else if (from.nickname.value) {
    doc.text(sr('from.nickname.value'), x, y, right)
  }
  if (from.vatCode && from.vatCode.value && from.vatCode.show) {
    y += step
    doc.text(texts.vatcode + from.vatCode.value, x, y, right)
  }
  // doc.font('Times-Roman')
  if (from.editedName && from.editedName.value) {
    y += step
    doc.text(from.editedName.value, x, y, right)
  }
  if (from.editedAdress && from.editedAdress.value) {
    y += step
    doc.text(from.editedAdress.value, x, y, right)
  }
  y += step
  doc.text(sr('from.zipLocation.value'), x, y, right)
  y += step
  doc.text(sr('from.country.value'), x, y, right)
  doc.moveDown()
}
function toFixed2(amount) {
  if (!amount) {
    return 0
  }
  return Number.parseFloat(amount).toFixed(2)
}
function formatDate(date) {
  if (!date) {
    return 0
  }
  return moment.unix(date / 1000).format('DD.MM.YY')
}
// to = customer
function generateCustomerInformation() {
  $log.info('inv inv.to = ' + JSON.stringify(inv.to))
  x = 50
  y = 100
  const arr = ['nickname', 'name', 'address', 'zipLocation', 'country']
  for (const item of arr) {
    if (item === 'nickname' && inv.to.nickname.value === 'adminGlimer') {
      // mashin admin-ia
      doc.text(sr('to.name.value'), x, y, left)
      y += step
    } else if (sr('to.' + item + '.show')) {
      doc.text(sr('to.' + item + '.value'), x, y, left)
      y += step
    }
  }
  /* doc.text(sr('to.name.value'), x, y, left)
  y += step
  doc.text(sr('to.address.value'), x, y, left)
  y += step
  doc.text(sr('to.zipLocation.value'), x, y, left)
  y += step
  doc.text(sr('to.country.value'), x, y, left) */
  doc.moveDown()
}
function generateInvoiceData() {
  x = 0
  y = 150
  const invNr = inv.admin ? inv.nr : sr('data.nr')
  doc.text(texts.data.nr + ': ' + invNr, x, y, right)
  y += step
  doc.text(texts.data.date + ': ' + formatDate(sr('date')), x, y, right)
  y += step
  doc.text(texts.data.dueDate + ': ' + formatDate(sr('dueDate')), x, y, right)
  y += step
  doc.text(texts.data.amount + ': ' + toFixed2(inv.data.total) + ' ' + sr('data.currency'), x, y, right)
  doc.moveDown()
}
function generateInvoiceHeader() {
  x = 0
  y = 250
  doc.fontSize(14).text(texts.header, x, y, { align: 'center', width: 500 })
  doc.moveDown()
}
/* function generateTableRow(y, c1, c2, c3, c4, c5) {
  doc.fontSize(10).text(c1, 50, y).text(c2, 150, y).text(c3, 280, y, { width: 90, align: 'right' }).text(c4, 370, y, { width: 90, align: 'right' }).text(c5, 0, y, { align: 'right' })
} */
function generateTableRow(y, c1, c2, c3) {
  doc.fontSize(10).text(c1, 50, y).text(c2, 150, y).text(c3, 0, y, { align: 'right' })
}
function generateInvoiceTable() {
  $log.info('inv generateInvoiceTable start ')
  let i
  y += 2 * step
  const headers = []
  inv.headers.forEach((it) => {
    headers.push(texts.headers[it.header])
  })
  generateHr()
  y += step
  generateTableRow(y, ...headers)
  y += step
  generateHr()
  $log.info('inv generateInvoiceTable middle ')
  for (i = 0; i < inv.items.length; i++) {
    const item = inv.items[i]
    // item.description = texts.descriptions[item.description] // check
    // item.speed = texts.speed[item.speed]
    // item.level = texts.level[item.level]
    let descr = '' // = this.$g.t('qchat.invoice.descriptions.' + invoice.items[i].description)
    // description = object { description, speed, level }
    if (item.description && $methods.isObject(item.description)) {
      Object.keys(item.description).forEach((key) => {
        const value = item.description[key]
        // item.description += this.$g.t('qchat.invoice.' + key + '.' + value) + '-'
        descr += texts[key][value] + '-'
      })
      item.description = $methods.removeLastCharacterFromString(descr)
    }
    $log.info('inv item.description = ' + item.description)
    item.price = toFixed2(inv.data.total) + ' ' + sr('data.currency')
    // $log.info('inv item = ' + JSON.stringify(item))
    const position = y + (i + 1) * step
    generateTableRow(position, item.serviceName, item.description, item.price) // item.speed, item.level,
    y += 2 * step
    generateHr()
    $log.info('inv generateInvoiceTable end ')
  }
  //generate vat row
  y += step
  const vatText = texts.items.vat_text
  const brutto = inv.data.total
  let vat = inv.data.vat
  // Gesamt: 50 €
  let nettoText = texts.items.total_text
  const netto = (+brutto * 100) / (100 + +vat)
  if (vat) {
    // Netto: 50 €
    nettoText = texts.items.netto_text
    vat = vat + ' %'
    const vatValue = toFixed2(brutto - netto) + ' ' + sr('data.currency')
    // generateTableRow(y, '', '', vatText, vat, vatValue)
    generateTableRow(y, vatText, vat, vatValue)
  }
  y += step
  generateHr()
  //generate netto row
  y += step
  // const nettoText = texts.items.netto_text
  const nettoValue = toFixed2(netto) + ' ' + sr('data.currency')
  // generateTableRow(y, '', '', '', nettoText, nettoValue)
  generateTableRow(y, '', nettoText, nettoValue)
}
function generateVatNote() {
  if (inv.data.vatNoteValue) {
    y += 2 * step
    doc.text(inv.data.vatNoteValue, 50, y)
  }
}
function generateInvoiceNote() {
  y += 2 * step
  const invNote = texts.notes.invoice_note + ' ' + texts.notes.invoice_note_text
  doc.text(invNote, 50, y)
}
function generatePaymentType() {
  if (inv.data.paymentType) {
    y += 2 * step
    const paymentText = texts.paymant_details + ' ' + sr('data.paymentType')
    doc.text(paymentText, 50, y)
  }
}
function generateHr() {
  doc.strokeColor('#aaaaaa').lineWidth(1).moveTo(50, y).lineTo(550, y).stroke()
}

export function getAdminInvoiceNr(invoice) {
  // kovel dges axlidan vnomravt
  // $log.info('inv invoice = ' + invoice)
  // $log.info('inv invoice.admin = ' + invoice.admin)
  if (invoice.admin) {
    // aq mxolod erti chanaceri iqneba da camovighot
    const data = { collName: 'invoicenrs', query: {} }
    return new Promise((resolve, reject) => {
      find(data)
        .then((invoicenrArr) => {
          // $log.info('po handler questionnaireValuesArr = ' + questionnaireValuesArr)
          const now = Date.now()
          const day = formatDate(now) // moment.unix(Date.now() / 1000).format('DD.MM.YY')
          let nr = 1
          let _id
          if (invoicenrArr && invoicenrArr.length) {
            const invoicenr = invoicenrArr[0]
            _id = invoicenr._id
            // invoicenr = { day: '12.05.21', nr:3 }
            $log.info('inv today ' + day)
            // const day = today
            if (invoicenr.day === day) {
              nr = +invoicenr.nr + 1
            }
          }
          // adminInvNr -> 120921-9
          resolve({ day, nr, _id, adminInvNr: moment.unix(now / 1000).format('DDMMYY') + '-' + nr })
        })
        .catch((err) => {
          $log.info('inv err = ' + err)
          reject(err)
        })
    })
  }
}
// invoiceNrData = { day, nr, _id }
export function setAdminInvoiceNr(invoiceNrData) {
  let data = { collName: 'invoicenrs' }
  // data.query = { _id: -2 } // ar aqvs mnishvneloba _id ras udris. mtavaria aseti ar ikos, rom upsert-ma imushaos
  if (invoiceNrData._id) {
    data.query = { _id: invoiceNrData._id }
  } else data.query = {}
  data.update = { $set: { day: invoiceNrData.day, nr: invoiceNrData.nr, ts: Date.now() } }
  data.options = { upsert: true }
  return new Promise((resolve, reject) => {
    updateOne(data)
      .then((resp) => {
        $log.info('po resp ' + JSON.stringify(resp))
        // const questionnaireValues = res.ops[0]
        if (resp) {
          if (!resp.update) {
            $log.info('po resp.update ' + resp.update)
            // mashin insert moxda
          }
          resolve()
        }
      })
      .catch((err) => {
        reject(err)
      })
  })
}
