// const jwt = require('jsonwebtoken')
// const { logger } = require('./logger')
import jwt from 'jsonwebtoken'
import { logger } from './logger'
const secret = 'dev_jwt_secret' // process.env.AUTH_JWT_SECRET
/**
 * The function creates jwt
 *
 * @param {Object} payload - Any data would been added in jwt
 * @param {Object} options - Extra parameters for jwt
 */
export function createJWT(payload, options = {}) {
  const token = jwt.sign(payload, secret, options)
  const decoded = decodeJWT(token)
  logger.info('jwt userId = ' + decoded.userId + ' expires at ' + new Date(decoded.exp * 1000).toLocaleString())
  return token
}

/**
 * The function verifies jwt
 *
 * @param {string} token - Token that need identify
 */
export function verifyJWT2(token) {
  // export const verifyJWT = (token) => {
  const verify = jwt.verify(token, 'secret')
  // console.log('verify = ', verify)
  return verify
}

/*
ქვედა მეთოდის იმპლემენტირება
try {
  const verify = verifyJWT(token)
}
catch (e) {
  console.log('80 e.message = ', e.message)
}
*/

export function verifyJWT(token) {
  return jwt.verify(token, secret)
}

/*
ქვედა მეთოდის იმპლემენტირება
verifyAsyncJWT(token)
  .then(res => {
      console.log('81 res = ', res)
  })
  .catch(err => {
     console.log('81 err = ', err.message)
  }) */
export const verifyAsyncJWT = async function (token) {
  return new Promise(function (resolve, reject) {
    jwt.verify(token, secret, (err, decoded) => {
      if (err) {
        // console.log('56 err = ', err.message)
        reject(err)
        return
      }
      // console.log('60 decoded = ', decoded)
      resolve(decoded)
    })
  })
}

// izleva -> let decoded = {"userId":"12345","test":"ori","iat":1568918995,"exp":1568918998}
export function decodeJWT(token) {
  return jwt.decode(token)
}

// an zeda 4 xazi asec sheizleba davcerot
// exports.decodeJWT = token => jwt.decode(token)

/* izleva -> let decoded = {"header":{"alg":"HS256","typ":"JWT"},
  "payload":{"userId":"12345","test":"ori","iat":1568918995,"exp":1568918998},
  "signature":"PCq_6HZBzq9YYDgPzHncKRwqZ9YAcNneRRdjRhcF1oo"}
*/
export function decodeFull_JWT(token) {
  return jwt.decode(token, { complete: true })
}
