exports.events = {
  acknowlidgeServerAboutGotAllUserMessages: 'acknowlidgeServerAboutGotAllUserMessages',
  connection: 'connection',
  disconnect: 'disconnect',
  mongo_connected: 'mongo-connected',
  chatToBack: 'chatToBack', // toBack
  chatFromBack: 'chatFromBack', //fromBack
  chatNotifFromBack: 'chatNotifFromBack', // rodesac servers sheatkobines da is mimghebs gadascems
  chatNotifToBack: 'chatNotifToBack', // rodesac servers sheatkobines da is mimghebs gadascems da mimghebi upasuxebs
  updated: 'updated',
  typingToBack: 'typingToBack',
  typingFromBack: 'typingFromBack',
  fileToBack: 'fileToBack',
  fileStreamToBack: 'fileStreamToBack',
  fileSiofuToBack: 'fileSiofuToBack',
  fileFromBack: 'fileFromBack',
  tokenError: 'tokenError',
  downloadFileFromBack: 'downloadFileFromBack',
  downloadUserAllMessages: 'downloadUserAllMessages',
  downloadAllUsersForAdmin: 'downloadAllUsersForAdmin',
  loadUserOrdersForAdmin: 'loadUserOrdersForAdmin'
}
exports.etexts = {
  noQuestionnaireFound: 'no questionnaire found!',
  getQuestionnaireFailed: 'get questionnaire failed!',
  uploadQuestionnaireFailed: 'questionnaire upload failed!',
  getDataByIdFailed: 'getDataById failed!',
  translateTextFailed: 'translate text failed!',
  noDocumentsFound: 'found no documents!',
  updateDocumentFailed: 'failed to update document!'
}
exports.status = {
  notSent: 'notSent',
  sent: 'sent',
  savedInDb: 'savedInDb',
  received: 'received',
  read: 'read'
}

exports.dataTypes = {
  text: 'text',
  file: 'file',
  video: 'video',
  audio: 'audio',
  image: 'image'
}
exports.intents = {
  chat: 'chat',
  questionnaire: 'questionnaire',
  chatFilemessage: 'chatFilemessage',
  uploadQuestionnaire: 'uploadQuestionnaire',
  DOWNLOAD: 'download',
  DOWNLOADING: 'downloading',
  SIGNUP: 'signup',
  ANSWER: 'login',
  UPDATE_USER: 'updateUser',
  ADMIN_EXITED: 'adminExited'
}
