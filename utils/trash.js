// ar vxmarobt
/* export function handlerFileMessage_Siofu(indexData) {
  return (message, callback) => {
    const uploader = indexData.uploaderSiofu
    $log.info('fU handlerMessage ' + JSON.stringify(message))
    // let uploader = new siofu()
    // uploader.listen(indexData.socket)
    // uploader.maxFileSize = 1000000
    // uploader.chunkSize = 1024 * 1000
    if ($serverData.maxFileSize) {
      uploader.maxFileSize = $serverData.maxFileSize
    }
    if ($serverData.chunkSize) {
      uploader.chunkSize = $serverData.chunkSize
    }

    uploader.on('error', (event) => {
      $log.error('fU error' + event.error)
      callback({ error })
    })
    const imageDirBase = $mediaDirParams.base
    const sender = message.meta.sender
    const avatar = message.meta.type === 'avatar'
    createFileDir(imageDirBase, sender, avatar, callback, (relDir, fileDirCreated) => {
      let newName = message.meta.tsList[$status.notSent] + '_' + message.meta.size + '_' + message.meta.type + '.' + message.meta.ext
      // if (message.meta.type === 'avatar') {
      //  newName = Date.now() + '_' + message.meta.size + '_avatar.' + message.meta.ext
      // }

      const relPath = relDir + newName
      const newPath = imageDirBase + relpath
      message.meta.relPath = relPath
      $log.info('createDirIfNotExists  relPath ' + relPath)
      uploader.dir = imageDirBase + relDir
      uploader.on('start', function (event) {
        event.file.name = newName
        // console.log('51 event.file', event.file)
      })
      // shevinaxet file
      uploader.on('saved', (event) => {
        // console.log('fU event', event)
        // !event.file.success nishnavs, rom file max size exceeded
        if (!event.file.success) {
          callback({ error: `file max size ${$serverData.maxFileSize} exceeded ` + error })
          return
        }
        // rename file to newName
        $log.info('fU event.file.pathName ' + event.file.pathName)

        // ok da vagrzelebt darchenil procesebs
        message.meta.status = $status.savedInDb
        if (!indexData.checkAdmin($webSocket, message, callback)) {
          // admin is offline
          return
        }
        setSavedStatusAndTsAndRenewToken(message.meta)
        if (message.meta.collName === 'messages') {
          // meta:{sender : userId, msgId:'', receiver : '101', status:'notSent', tsList:{}}
          $log.info('fU completed')
          // message.meta.status = $status.savedInDb
          // callback(message.meta)
          // console.log('in wrapper message ', JSON.stringify(message))
          insertFileMessage(message, indexData, callback)
        }
      })
    })
  }
} */

/*
// invoice-is chamotvirtva
app.post('/downloadInvoice', (request, response, next) => {
  $log.info('cs downloadInvoice request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/downloadInvoice', request, response)
})
// invoice-is atvirtva
app.post('/uploadInvoice', (request, response, next) => {
  $log.info('cs uploadInvoice request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/uploadInvoice', request, response)
})
// questionnaireValues gamozaxeba
app.post('/getQValuesByCardId', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/getQValuesByCardId', request, response)
})
// data aris an user an card
app.post('/getDataById', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/getDataById', request, response)
})
app.post('/sendPendingMessage', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/sendPendingMessage', request, response)
})
app.post('/uploadMediaChat', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/uploadMediaChat', request, response)
})
app.post('/uploadMediaQuestionnaire', (request, response, next) => {
  $log.info('cs request.body ' + request.body)
  if (!request.body) return response.sendStatus(400)
  postService('/uploadMediaQuestionnaire', request, response)
})
app.post('/uploadQuestionnaire', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  postService('/uploadQuestionnaire', request, response)
})
app.post('/uploadExampleQuestionnaire', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  postService('/uploadExampleQuestionnaire', request, response)
}) 

app.get('/unsubscribe2/*', (req, res) => {
  unsubscribeEmail2(res, req.path)
})
function unsubscribeEmail2(res, path) {
  // path =  /unsubscribe/test@web.de
  const email = path.replace('/unsubscribe/', '')
  console.log('email = ', email)
  let html = fs.readFileSync('./sendEmails/toDealers.html', 'utf8')
  // http://192.168.178.20:4129/unsubscribe/test@web.de
  html = html.replace(/#hrefunsubscribe#/g, 'http://192.168.178.20:4129/unsubscribe2/' + email)
  res.send(html)
} 
// handlerFileDownload_Stream, handlerFileMessage_Siofu
// const { events, status, dataTypes, etexts } = require('../utils/enums')
// const handlerFileDownload_Stream = require('../utils/fileUploader').handlerFileDownload_Stream
// const handlerFileMessage_Siofu = require('../utils/fileUploader').handlerFileMessage_Siofu
// const handlerLoadAllUsers = require('../utils/admin').handlerLoadAllUsers
// const handlerLoadUserAllFileMessages = require('../utils/admin').handlerLoadUserAllFileMessages
// const siofu = require('socketio-file-upload')

// am dros klientidan modis message -> meta.type=downloadFile da meta.filepath
    // socket.on($enums.events.downloadFileFromBack, handlerFileDownload_Stream({ socket, ss }))
    // socket.on($enums.events.downloadAllUsersForAdmin, handlerLoadAllUsers({socket, mongo, find})) -> chatAuth-shia
    // socket.on($enums.events.downloadUserAllFileMessagesForAdmin, handlerLoadUserAllFileMessages({socket, mongo, find})) -> chatAuth-shia
    let uploaderSiofu = new siofu()
    uploaderSiofu.listen(indexData.socket)
    indexData.uploaderSiofu = uploaderSiofu
    socket.on($enums.events.fileSiofuToBack, handlerFileMessage_Siofu(indexData)) // file  siofu

if (message.statusInfo.receiver === 'admin') {
        let admin = this.webSocket.findConnectedAdminSocketEntity()
        if (admin && message.statusInfo.receiver !== admin.userId) {
          console.log('admin.userId ', admin.userId)
          message.statusInfo.receiver = admin.userId
        } else {
          // admin is offline
          message.statusInfo.error = httpErrs.adminIsOfflineError
          callback(message.statusInfo)
          console.log('chatOfflineError ')
          return
        }
      }

function create_FileDir(message, callback, fileDirSuccessed) {
  createFileDir(this.serverData.imageDirBase, message.statusInfo.sender, err =>{
    callback(err)
  }, (relDir, fileDirCreated) => {
    fileDirSuccessed(relDir, fileDirCreated)
  } )
}
// translate(data).then(res => saveIntoDB(res)).then(res => send(res, socket)).catch(err => console.log('err:', err))
function handlerFileMessage_Stream2(socket) {
   message asetia: relpath chaceris shemdeg daemateba
    {"meta":{"type":"file","collName":"messages"},
    "data":{"author":"me","text":"ert", "senderLang":"de","username":"misha","chatetName":'Herr Fisher', avatarSrc:'http..some.png'},
    "statusInfo":{relpath: '/senderId/dateDay/date.jpg', "sender":"5d4b41f38b7a432c541ab3b4","msgId":"afgadfgadfg","receiver":"admin","status":"notSent","tsList":{notSent:1565264823436}},
    "admin":false}

    shevinaxot file bazashi am misamartze:
    imageDirBase = '../../tfotos/' == 'https://kfz-soft.com/public/transporter/tfotos/'
    imageDirBase/'userId/2019-8-20/1566329755060.jpg
    shevqmnat fileDir if not exists

 return (stream, message, callback) => {
  console.log('handlerMessage ', JSON.stringify(message))
  stream.on('error', error => {
    console.log('error', error)
    callback(error)
  })
  if (message.meta.collName === 'messages') {
    // statusInfo:{sender : userId, msgId:'', receiver : '101', status:'notSent', tsList:{}}
    setSavedStatusAndTsAndRenewToken(message.statusInfo)
    if (message.statusInfo.receiver === 'admin') {
      let admin = this.webSocket.findConnectedAdminSocketEntity()
      if (admin) {
        console.log('entity.userId ', admin.userId)
        message.statusInfo.receiver = admin.userId
      } else {
        // admin is offline
        message.statusInfo.error = httpErrs.adminIsOfflineError
        callback(message.statusInfo)
        console.log('chatOfflineError ')
        return
      }
    }
    stream.on('end', () => {
      console.log('completed')
      message.statusInfo.status = status.savedInDb
      callback(message.statusInfo)
      console.log('in wrapper message ', JSON.stringify(message))
      insertOne(message)
        .then(insertedId => {
          if (insertedId) {
            insertedId = insertedId.toString()
            console.log('...insertedId ', insertedId)
            // gavagebinet gamomgzavns, rom mivighet message
            message.statusInfo.msgId = insertedId
            // asynchron shevqmnat fileDir if not exists
            create_FileDir(message, callback, (relDir, fileDirCreated) => {
              const relpath = relDir + message.statusInfo.tsList[status.notSent] + '.' + message.meta.ext
              message.statusInfo.relpath = relpath
              console.log('createDirIfNotExists  relpath ', relpath)
              // shevinaxet file
              stream.pipe(fs.createWriteStream(this.serverData.imageDirBase + relpath))
              // aq filepath sheqmnilia da file chacerilia
              // tu receiver xazzea, mashin gadavugzavnot es file
              const receiverId = message.statusInfo.receiver
              if (this.webSocket.userIsConnected(receiverId)) {
                const receiverSocket = this.webSocket.socketMaps[receiverId].socket
                console.log('receiverSocket ', receiverSocket.id)
                // const stream = ss.createStream()
                // ss(receiverSocket).emit($enums.events.fileFromBack, stream, message, resp_statusInfo => {
                receiverSocket.emit($enums.events.fileFromBack, message, respArr_statusInfo => {
                  // mimghebidan pasuxia, rom miigho statusInfo ...status : status.received
                  console.log('resp_statusInfo ', JSON.stringify(respArr_statusInfo))
                  updateAndSendMessageStatus({ statusInfo: respArr_statusInfo, socket })
                })
                // stream-is gagzavnas ar vaketebt, mxolod public url
                // fs.createReadStream(this.serverData.imageDirBase + message.statusInfo.relpath).pipe(stream)
                // receiverSocket.emit($enums.events.typingFromBack, message)
              } else {
                // tu mimghebi xazze a aris, gagzavnis gareshe davimaxsovrot, rac gvaqvs
                updateAndSendMessageStatus({ statusInfo: message.statusInfo })
              }
            })
          }
        })
        .catch(err => {
          console.log('err:', err)
          callback(err)
        })
    })
  }
}
}
async function insertMany(data) {
  console.log('saveIntoDB')
  const coll = this.mongo.getCollection(data.options.collectionName)
  await coll.insertOne(data.data, (err, res) => {
    if (err) {
      return Promise.reject(err)
    } else {
      console.log('res.insertedId ', res.insertedId)
      return Promise.resolve(res.insertedId)
    }
  })
}

else if (data.type === 'find') {
     find(data, 'users').then(res => send(res, socket)).catch(err => console.log('err:', err))
   } else if (data.type === 'insertOne') {
     insertOne(data, 'users').then(res => send(res, socket)).catch(err => console.log('err:', err))

   } else if (data.type === 'insertMany') {
     insertMany(data, 'users').then(res => send(res, socket)).catch(err => console.log('err:', err))

   } else if (data.type === 'updateOne') {
     updateOne(data, 'users').then(res => send(res, socket)).catch(err => console.log('err:', err))
   }

function addUpdateParameter(data) {
  data.options.query = { 'messageId': data }
  data.options.options = { "upsert": false }
  data.options.update = {
  "$set": {
    "sent": {status:'received', ts:Date.now()}
  }
}

coll.find(data.options.query, data.options.projection)
    // .sort({ name: 1 })
    .toArray()
    .then(items => {
      console.log(`Successfully found ${items.length} documents.`)

      return Promise.resolve(items)
    })
    .catch(err => {
      console.error(`Failed to find documents: ${err}`)
      return Promise.reject(err)
    })

    //console.log('s',s)
  // return Promise.resolve({id:s})
  //return s
return new Promise((resolve, reject) => {
  coll.insertOne(data, (err,res)=> {
    if (err) {
      reject(err)
    } else {
      console.log('res.insertedId ',res.insertedId)
      resolve(res.insertedId)
    }
  })
}) */
