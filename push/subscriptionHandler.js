const subscriptions = {}
// var crypto = require('crypto')
// const webpush = require('web-push')
import crypto from 'crypto'
import axiwebpushos from 'web-push'
const vapidEmail = 'www.deservice.net'
// VAPID keys should be generated only once.
// const vapidKeys1 = webpush.generateVAPIDKeys()
// console.log('vapidKeys1 = ', vapidKeys1)
const vapidKeys = {
  privateKey: 'DQ0udeRikzHnivSX0YldTeJEaK9iiawHm4c3ddur7SI',
  publicKey: 'BJyem4fs7_L-GfzSUAOYs8IjStk-D8valCWNv_N2XgOxmJriqWADaI11t3L9qVCyKmMugc7E1jCsspgkTtkxaOg'
}

webpush.setVapidDetails('mailto:' + vapidEmail, vapidKeys.publicKey, vapidKeys.privateKey)

function createHash(input) {
  const md5sum = crypto.createHash('md5')
  md5sum.update(Buffer.from(input))
  return md5sum.digest('hex')
}
// req.body = { subscription, userId }
export function handlePushNotificationSubscription(req, res) {
  const subscriptionRequest = req.body.subscription
  const userId = req.body.userId
  console.log('20 req.body ', req.body)
  const subscriptionId = createHash(JSON.stringify(subscriptionRequest))
  subscriptions[subscriptionId] = subscriptionRequest
  res.status(201).json({ id: subscriptionId })
}

export function sendPushNotification(req, res) {
  // console.log('req.params ', req.params)
  const subscriptionId = req.params.id
  const pushSubscription = subscriptions[subscriptionId]
  console.log('pushSubscription ', pushSubscription)
  if (!pushSubscription) {
    console.log('pushSubscription not exists ', pushSubscription)
    return
  }
  webpush
    .sendNotification(
      pushSubscription,
      JSON.stringify({
        role: 'orderer',
        nick: 'car12345',
        title: '_you_have_note_from', // 'New Notification!', // უნდა ითარგმნებოდეს
        body: '_time_of_recieve_note', // 'Your have an order from orderer car12345.', // უნდა ითარგმნებოდეს
        badge: 'desicon.png', // 'https://spyna.it/icons/android-icon-192x192.png', // static folder-იდან ან imageUrl -> მობილურში პატარა აიკონი, როცა ადგილი არ არის
        image: 'main.png', // static folder-იდან ან imageUrl -> ძირითადი დიდი სურათი notification-ში
        icon: 'desicon.png', // static folder-იდან ან imageUrl -> deservice icon -> 192x192 px
        tag: 'car12345', //  An ID for a given notification that allows you to find, replace, or remove the notification using a script if necessary.
        data: '/', // data ჰქვია, ისე url-ია თუ საიტზე საით გადაამისამართოს. მაგ. '/user' თუ home-ია, მაშინ -> '/'
        vibrate: [200, 100, 200],
        actions: [{ action: 'Detail', title: 'View', icon: 'https://via.placeholder.com/128/ff0000' }]
      })
    )
    .catch((err) => {
      console.log(err)
    })

  res.status(202).json({})
}
