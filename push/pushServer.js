// const express = require('express')
// const subscriptionHandler = require('./subscriptionHandler')
import express from 'express'
import subscriptionHandler from './subscriptionHandler'

const app = express()

const host = 'localhost'
const port = '4129'

const server = app.listen(port, () => {
  console.log('cS Server is listening on port ' + port)
})

app.use(express.urlencoded({ extended: true }), express.json())
app.use((req, res, next) => {
  // console.log('req.body = ', req.body)
  res.append('Set-Cookie', 'cross-site-cookie=name; SameSite=None; Secure')
  res.append('Access-Control-Allow-Origin', ['*'])
  res.append('Access-Control-Allow-Headers', 'Content-Type')
  next()
})
// push
app.get('/subscription/:id', subscriptionHandler.sendPushNotification)
app.post('/subscription', subscriptionHandler.handlePushNotificationSubscription)

exports.app = app
