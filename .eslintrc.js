module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es2021: true
  },
  parserOptions: {
    // parser: 'babel-eslint',
    ecmaVersion: 9,
    sourceType: 'module'
  },
  extends: ['prettier', 'plugin:prettier/recommended', 'plugin:json/recommended'],
  plugins: ['prettier', 'json', 'html'],
  // add your custom rules here
  rules: {
    // 'nuxt/no-cjs-in-config': 'off',
    'no-console': 0
  }
}
