require('dotenv').config()
const { ObjectId } = require('mongodb')
const fs = require('fs-extra')
// axiosMethods, methods sheizleba momavalshi gamovikenot
import { enums } from 'helpers'
// const enums = require('helpers')
const cors = require('cors')
const express = require('express')
const WebSocket = require('./utils/websocket')
const path = require('path')
const bodyParser = require('body-parser')
const EventEmitter2 = require('eventemitter2').EventEmitter2
const postService = require('./chat/post')
const createDirIfNotExists = require('./utils/fileUploader').createDirIfNotExists
const createChatService = require('./chat/chat')
const { MongoClient } = require('./utils/MongoClient')
const { logger } = require('./utils/logger')
const { getRelPathAndFilenameFromMediaUrl } = require('./utils/help')
global.$log = logger
global.$enums = enums
const dbName = 'desmongo' // socketchat

testMongo()
function testMongo() {
  const emitter = new EventEmitter2({
    newListener: false,
    wildcard: true
  })
  global.$mongo = new MongoClient(process.env.MONGO_URL, dbName, emitter)
  emitter.on('mongo-connected', () => {
    $log.info('mongo-connected')
  })
}
const app = express()
const port = process.env.PORT
const server = app.listen(port, () => {
  $log.info('cS Server is listening on port ' + port)
})

app.use(cors())
app.use((req, res, next) => {
  $log.info('cS req.url ' + req.url)
  // hh movacilet cin '/qchat'   /qchat/login -> /login
  req.url = req.url.replace('/qchat', '')
  // res.header('Access-Control-Allow-Origin', '*')
  // res.header('Access-Control-Allow-Origin', req.header('origin') )
  // res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, foo')
  next()
})

exports.app = app
