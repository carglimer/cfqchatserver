// iqoqeba ase -> node -r esm chatServer
// განახლების დროს ჯერ ავტვირთავ staging-ს, მერე github desktop-ს გადავრთავ master-ზე და იქ ზევით
// brunch-ში -> merge unto current brunch -> მერე ავირჩევთ staging-ს და დავკლიკავთ create a merge commit
// ამით რა ცვლილებებიც გაკეთდა staging-ში, გადავა master-შიც
// staging არის QchatServerTest-ისთვის

// axiosMethods, methods sheizleba momavalshi gamovikenot
// sifrtxilea sachiro axiosMethods-is im methodebis xmareba ar sheizleba, sadac ctx-ia gamokenebuli

// const { enums, methods } = require('helpers')
// const { gmBackMethods } = require('commonbackmethods')
// const fs = require('fs-extra')
// const cors = require('cors')
// const express = require('express')
// const WebSocket = require('./utils/websocket')
// require('dotenv').config()
// const path = require('path')
// const bodyParser = require('body-parser')
// const EventEmitter2 = require('eventemitter2').EventEmitter2
// const postService = require('./chat/post')
// const createDirIfNotExists = require('./utils/fileUploader').createDirIfNotExists
// const createChatService = require('./chat/chat')
// const { MongoClient } = require('./utils/MongoClient')
// const { logger } = require('./utils/logger')
// const { unsubscribeEmail } = require('./sendEmails/unsubscribe')
// const { getRelPathAndFilenameFromMediaUrl } = require('./utils/help')
// const monitoring = require('./utils/monitoring')
import dotenv from 'dotenv'
import ffmpegStatic from 'ffmpeg-static'
import ffmpeg from 'fluent-ffmpeg'
dotenv.config()
import * as enums from './utils/enums'
import * as methods from './utils/methods'
import adminData from './utils/adminData'
import { gmBackMethods } from 'commonbackmethods' // ჩემი და მურმანის საერთო მეთოდები, რომელსაც ჩვენი ბექები ხმარობენ
import cors from 'cors'
import express from 'express'
import eventemitter2 from 'eventemitter2'
const EventEmitter2 = eventemitter2.EventEmitter2
import WebSocket from './utils/websocket'
import postService from './chat/post'
import { createDirIfNotExists } from './utils/fileUploader'
import createChatService from './chat/chat'
import errorCodes from './utils/errorCodes'

import { MongoClient } from './utils/MongoClient'

import { logger } from './utils/logger'
import { unsubscribeEmail } from './sendEmails/unsubscribe'
import { getRelPathAndFilenameFromMediaUrl } from './utils/help'
import monitoring from './utils/monitoring'
ffmpeg.setFfmpegPath(ffmpegStatic)

// global.$availableLangs = ['de', 'en', 'ka', 'uk', 'ru']
global.$availableLangs = process.env.AVAILABLE_LANGS.split(',')
// console.log('global.$availableLangs = ', global.$availableLangs)
global.$mongoDomain = 'localhost' // <- es shesacvlelia namdvili domain saxelit
global.$storageDomain = 'localhost' // <- es shesacvlelia namdvili domain saxelit
global.$log = logger
global.$getLinkId = gmBackMethods.getLinkId
global.$enums = enums
global.$events = enums.events
global.$caller = enums.caller
global.$etexts = enums.etexts
global.$status = enums.status
global.$errors = enums.errors
global.$intents = enums.intents
global.$typingState = enums.typingState
global.$methods = methods
global.$adminData = adminData
global.$ffmpeg = ffmpeg
/* const link1 = 'https://suchen.mobile.de/fahrzeuge/details.html?id=333164230&cn=DE&searchId=ec225aca-155c-505a-5c55-59f1496d5e99'
const link2 = 'https://suchen.mobile.de/auto-inserat/volkswagen-polo/333164230.html?coId=f881b725-39fc-45ab-9070-eb063030fbef&action=vip404SingleAdReco'
console.log('t $getLinkId1 = ', $getLinkId(link1))
console.log('t $getLinkId2 = ', $getLinkId(link2)) */
global.$isTest = process.env.G_IS_TEST && JSON.parse(process.env.G_IS_TEST) // true
const dbName = global.$isTest ? process.env.MONGO_DB_NAME_TEST : process.env.MONGO_DB_NAME // cfmongotest 'cfmongo'
const namespace = 'chat'
// es folder imageDirBase sheqmnili unda gvqondes. live-shi sheqmna ar aris kargioo
// const imageDirBase = '../cfmedias/' amass sxva modulebs pirdapir exports.imageDirBase-it ver gadavcemt, radgan gviandeba
// rasac klienti ikenebs suratebis gamosazaxeblad -> http://localhost:4129/docs/1599165318843_49098.jpeg
// const imageDirURl = '/docs/'
const mediaDirParams = {
  base: process.env.CFMEDIAS_DIR, // '../cfmedias', საწყისი საერთო ფოლდერი
  chatUrl: '/chats/', // cmedias რასაც კლიენტი გამოიძახებს url-ში -> /chats/1599165318843_49098.jpeg
  questionnaireUrl: '/questionnaires/', // qmedias რასაც კლიენტი გამოიძახებს url-ში -> /questionnaires/1599165318843_49098.jpeg
  examplemediaUrl: '/examplemedias/' // qmedias რასაც კლიენტი გამოიძახებს url-ში -> /examplefotos/1599165318843_49098.jpeg
}

$log.info('global.$googleTranslateApiKey = ' + global.$googleTranslateApiKey)
if (process.env.API_BASE_URL && process.env.API_BASE_URL.includes('test')) {
  $isTest = true
}
global.$withoutMurmanBack = process.env.G_WITHOUT_MURMAN_BACK && JSON.parse(process.env.G_WITHOUT_MURMAN_BACK)
global.$translate = process.env.TRANSLATE && JSON.parse(process.env.TRANSLATE)
const isLocalhost = process.env.G_LOCALHOST && JSON.parse(process.env.G_LOCALHOST)
// API_BASE_URL_TEMP droebit aris. unda ikos -> API_BASE_URL
// const API_BASE_URL = isLocalhost ? process.env.API_BASE_URL_LOCALHOST : process.env.API_BASE_URL
const API_BASE_URL = $isTest ? process.env.API_BASE_URL_TEST : process.env.API_BASE_URL
const cfBackHost = API_BASE_URL + process.env.CFBACK_HOST_URL // https://api.check-first.net/qchat/
// console.log('desbackHost = ', desbackHost)
$log.info('cfBackHost = ' + cfBackHost)
const cfBackData = {
  host: cfBackHost,
  token: process.env.CFBACK_TOKEN,
  adminToken: process.env.CF_BACK_ADMIN_TOKEN
}
const dirArray = [mediaDirParams.chatUrl, mediaDirParams.questionnaireUrl, mediaDirParams.examplemediaUrl]
dirArray.forEach((element) => {
  createDirIfNotExists(mediaDirParams.base + element, null, (success) => {
    $log.info('cS dir created' + mediaDirParams.base + element + ' success = ' + success)
  })
})

// serverData-shi sxvadasxva const-ebs chavcert, romlebic momavalshi sheizleba dagvchirdes
// serverData.uploadType = 'stream', 'siofu', 'axios'
const maxFileSize = 2000000
const chunkSize = 1024 * 1000
const serverData = { maxFileSize, chunkSize }

global.$mediaDirParams = mediaDirParams
global.$cfBackData = cfBackData
global.$serverData = serverData

const app = express()
const port = process.env.PORT // production-shi port=80 'http://136.243.102.152:4129/chat'

// app.use(makeInjection, express.urlencoded({extended: true}), express.json(), cookieParser())
// app.use(express.urlencoded({ limit: '20mb', extended: true }), express.json())
app.use(express.urlencoded({ limit: '100mb', extended: true }), express.json())
// eslint-disable-next-line no-unused-vars
/* const errorHandler = (err, _req, res, _next) => {
  const code = err.status || 400
  res.status(code).json(err)
} */

const server = app.listen(port, () => {
  $log.info('cS Server is listening on port ' + port)
})

const emitter = new EventEmitter2({
  newListener: true,
  wildcard: true
})
// jenkinsis .env-shi ceria -> MONGO_URL=mongodb://admin:imereti1@mongo:27017/dbName?authSource=admin
const user = {
  // user: process.env.MONGO_DB_USER,
  createUser: process.env.MONGO_DB_USER,
  pwd: process.env.MONGO_DB_PWD,
  roles: [
    {
      role: 'readWrite',
      db: dbName
    }
  ]
}
const mongoUri = process.env.MONGO_URI.replace('dbName', dbName).replace('user', user.createUser).replace('pwd', user.pwd)
global.$mongo = new MongoClient(mongoUri, dbName, emitter)

emitter.on('mongo-connected', () => {
  $log.info('createChatService')
  global.$webSocket = new WebSocket(server, emitter, namespace)
  createChatService(emitter)
  // postService('/getQValuesByCardId', { body: { cardId: 124 } }, null, emitter, serverData)
})
emitter.on('mongo-error', (err) => {
  // mashin mongo jer ar aris sheqmnili
  $log.info('mC Mongo DB Connection Error2: err.message ' + err.message)
  // $log.error('mc Mongo DB Connection Error2: err.message = ' + err.message)
  // if (err.message === 'no_user') {
  global.$mongo = new MongoClient(process.env.MONGO_URI_PLAIN.replace('dbName', dbName), dbName, emitter, user)
  // }
})
/*
g.monitor.deservice.net
g.test.monitor.deservice.net
m.monitor.deservice.net
m.test.monitor.deservice.net
*/
global.$validOrigins = [
  process.env.MAIN_HOST_URL,
  process.env.MAIN_HOST_URL.replace('www.', ''),
  process.env.MEDIAS_HOST_URL,
  process.env.MEDIAS_HOST_URL.replace('www.', ''),
  process.env.TEST_HOST_URL,
  process.env.TEST_HOST_URL.replace('www.', ''),
  process.env.TEST_MEDIAS_HOST_URL,
  process.env.TEST_MEDIAS_HOST_URL.replace('www.', ''),
  /.*\.monitor\.deservice\.net/
]
const validTestOrigins = [/http:\/\/192\.168.*/, /http:\/\/localhost:.*/, /localhost:.*/, /http:\/\/127.0.0.1:.*/]
if ($isTest) {
  global.$validOrigins.push(validTestOrigins)
}
const ipFilter = (req, res, next) => {
  // "origin":"https://www.test.deservice.net","referer":"https://www.test.deservice.net/infos/questionnaireExampleBasic"
  const url = req.headers.origin || req.headers.referer
  let token = req.headers.token || '1.2.3'
  let verified = false
  if (token && token === process.env.CF_BACK_MG_TOKEN) {
    verified = true
  }
  // es satestod mchirdeba
  let gtoken = req.headers.gtoken
  if (gtoken && gtoken === process.env.G_TOKEN) {
    $isTest = true
  }
  $log.info('cS $isTest = ' + $isTest)
  if ($isTest) {
    verified = true
    $log.info('cS verified = ' + verified)
    $log.info('cS req.headers = ' + JSON.stringify(req.headers))
    $log.info('cS req.url = ' + req.url)
    $log.info('cS origin = ' + url)
  }
  // $log.info('cS req.headers ' + JSON.stringify(req.headers))
  const atLeastOneMatches = global.$validOrigins.some((e) => {
    if (e instanceof RegExp) {
      return e.test(url)
    } else {
      return url ? url.includes(e) : false
    }
  })
  $log.info('cS atLeastOneMatches = ' + atLeastOneMatches)
  if (atLeastOneMatches || verified) {
    // IP is ok, so go on
    $isTest && $log.info('URL ok ' + url)
    next()
  } else {
    // 'no valide origin!'
    res.status(341).json(errorCodes[341])
  }
}
app.use(ipFilter)
// app.use(cors({credentials: true, origin: true}));
// tu test-s vamocmebt, mashin aq iqneba origin: true. production-shi locahostebi unda amovighot
// const origin = app.use(...)
app.use(
  cors({
    credentials: false,
    origin: global.$validOrigins // true
  })
)
// app.use(cors())
// es murmanis da datos konstruqciaa: monitoring -> app.router
// /monitoring/self-idan aq mocildeba /monitoring da router-shi gadava /self
app.use('/monitoring', monitoring)
app.use((req, res, next) => {
  // $log.info('use keys = ' + JSON.stringify(Object.keys(req)))
  // console.log('use req = ', req)
  // hh movacilet cin '/qchat'   /qchat/login -> /login
  // req.url = req.url.replace('/qchat', '') უკვე მოაცილა nuxt.config-ის proxy-მ -> pathRewrite: { '^/qchat': '' }
  // test-ის დროს ყველა ip-დან უნდა მივიღოთ
  if ($isTest || (req.body.test && req.body.test === 'saidumlo')) {
    res.header('Access-Control-Allow-Origin', '*')
    if (req.body.test) {
      delete req.body.test
    }
  }
  // res.header('Access-Control-Allow-Origin', req.header('origin') )
  // res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  // res.header('Content-Type', '*/*')
  res.header('Accept-Ranges', 'bytes')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, foo')
  next()
})

// unsubscribe sareklamo mailebi modis -> /unsubscribe/user@mail.de
app.get('/unsubscribe/*', (req, res) => {
  unsubscribeEmail(res, req.path)
})
app.get('/general/*', (req, res) => {
  $log.info('relPath req = general')
})
// chat-istvis req.path -> /chats/1599165318843_49098.jpeg
app.get('/chats/*', (req, res) => {
  getAndSendFile(res, req.path)
})
// questionnaire-stvis req.path -> /questionnaires/1599165318843_49098_image.jpeg
app.get('/questionnaires/*', (req, res) => {
  getAndSendFile(res, req.path)
})
// example foto-ebistvis req.path -> /examplefotos/1599165318843_49098_image.jpeg
app.get('/examplemedias/*', (req, res) => {
  getAndSendFile(res, req.path)
})
// questionnaire-is da chat-is file-is chamotvirtva ->  req.body = {"relPath":"/questionnaires/1611527943463_241760.pdf"}
app.post('/downloadFile', (req, res, next) => {
  getAndSendFile(res, req.body.relPath)
})

function getAndSendFile(res, url) {
  const relPathAndFilename = getRelPathAndFilenameFromMediaUrl(url)
  const relPath = relPathAndFilename.relPath
  const filename = relPathAndFilename.filename
  $log.info('relPath ' + relPath)
  $log.info('filename ' + filename)
  // orive movsinjo
  res.sendFile(filename, { root: relPath })
  // res.download(filePath, fileName)
}
const postUrlArray = [
  'getQStandsArrForCarlinkOrVin6', // არსებული ანკეტების გაყიდვის მოდულისათვის
  'translateText', // raimes calke gadaTargmna tu gvinda -> { text, from, to }
  'postContactRequest', // contact-is kopmonentidan user-ebis mier gamogzavnisi zaprosebi da survilebi
  'downloadInvoice', // invoice-is chamotvirtva
  'uploadInvoice', // invoice-is atvirtva
  'getQValuesByCardId', // questionnaireValues gamozaxeba
  'getDataById', // data aris an user an card
  'sendPendingMessage',
  'getInvoiceByCardIdAndServiceType',
  'uploadMediaChat',
  'uploadMediaQuestionnaire',
  'uploadQuestionnaire',
  'uploadExampleQuestionnaire',
  'removeQMediaOnMongo',
  'createInvoice', // modis murmanisgan
  'validationInfo' // როდესაც დამკვეთი ასაჩივრებს დაკვეთას, ადმინის გვერდი მეკითხება { cardId } და ვუგზავნი vin და 3 სურათს
]
postUrlArray.forEach((postUrl) => {
  app.post('/' + postUrl, (request, response, next) => {
    $log.info('cs postUrl = ' + postUrl)
    $log.info('cs request.body = ' + request.body)
    // $log.info('cs downloadInvoice request.body ' + request.body)
    if (!request.body) return response.sendStatus(400)
    postService('/' + postUrl, request, response)
  })
})
// exports.app = app
export { app }
