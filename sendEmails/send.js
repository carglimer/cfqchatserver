// node sendEmails/send
// require('dotenv').config()
// const fs = require('fs-extra')
// const MongoClient = require('../utils/MongoClient')

// const { MongoClient, find, updateOne, insertOne } = require('../utils/MongoClient')
// const EmailService = require('../utils/email')
// const EventEmitter2 = require('eventemitter2').EventEmitter2
// const { logger } = require('../utils/logger')
// const enums = require('../utils/enums2')
import dotenv from 'dotenv'
dotenv.config()
import fs from 'fs-extra'
import { MongoClient, find, updateOne } from '../utils/MongoClient'
import EmailService from '../utils/email'
import eventemitter2 from 'eventemitter2'
const EventEmitter2 = eventemitter2.EventEmitter2
import { logger } from '../utils/logger'
import enums from '../utils/enums2'

// const deserviceUrl = 'https://test.deservice.net/'
const deserviceUrl = 'http://192.168.178.20:3083/'
const unsubscribeUrl = 'http://192.168.178.20:4129/unsubscribe/'
const adminEmail = 'carglimer@web.de' // sender
const testEmail = 'gia-lomidze@web.de' // receiver
// const testEmail = 'giorgilomidze97@googlemail.com'
const hrefdeservice = '#hrefdeservice#'
const hrefunsubscribe = '#hrefunsubscribe#'

// email-shi gaaqtiurebuli unda ikos
const mailFromOptions = {
  email: adminEmail,
  pw: 'imereti%1',
  host: 'smtp.web.de',
  port: 587
}
/* const mailFromOptions = {
  email:'gialomidze62@gmail.com',
  pw:'imereti%1',
  service: 'gmail'  // host: 'smtp.gmail.com' port: 465
} */
global.$log = logger
global.$enums = enums
const dbName = 'desmongo'
const dealersCollection = 'dealers'

const emitter = new EventEmitter2({
  newListener: false,
  wildcard: true
})
// jenkinsis .env-shi ceria -> MONGO_URL=mongodb://admin:imereti1@mongo:27017/dbName?authSource=admin
global.$mongo = new MongoClient(process.env.MONGO_URL, dbName, emitter)
emitter.on('mongo-connected', () => {
  $log.info('mongo-connected')
  sendEmails()
})
let sentEmailOnce = false
function sendEmails() {
  const email = new EmailService(mailFromOptions)
  const subject = 'An deutsche Autohändler'
  // const text = 'test text'
  // jer cavikitxot PLZ-ebis array
  // const zuriaData = { options: { query: { name: 'Zuria' } }, meta: { collName: 'zips' } }
  const zuriaData = { collName: 'zips', query: { name: 'Zuria' } }
  find(zuriaData).then((res) => {
    const plzs = res[0].zips
    console.log('plzs ', plzs)
    /* if (plzs2) {
      return
    } */
    readFileText('./sendEmails/templates/de/toDealers.html')
      .then((html) => {
        // console.log('html ', html)
        // es kvelas shecvlis
        html = html.replace(new RegExp(hrefdeservice, 'g'), deserviceUrl)
        // html = html.replace(new RegExp(hrefunsubscribe, 'g'), unsubscribeUrl)
        //  const body={ email, subject, text, html, attachments}
        const body = { subject, html }
        for (const plz of plzs) {
          const query = { PLZ: plz }
          const projection = {}
          // const options = { query, projection }
          // const data = { options, meta: { collName: dealersCollection } }
          const data = { collName: dealersCollection, query, projection }
          find(data).then((dealersInOnePLZ) => {
            console.log('dealersInOnePLZ ', dealersInOnePLZ)
            const sentStates = {}
            for (const dealer of dealersInOnePLZ) {
              if (dealer.unsubscribed) {
                continue
              }
              sentStates[dealer.Email] = dealer.sent ? dealer.sent.length : 0 // { sent: dealer.sent, sentNumber: dealer.sent ? dealer.sent.lemgth : 0 }
            }
            // am jgufshi ramdenia minimalurad gagzavnili mailebis raodenoba
            const sentMinLength = Math.min(...Object.values(sentStates))
            for (const dealer of dealersInOnePLZ) {
              if (dealer.unsubscribed) {
                continue
              }
              let emailReceiver = dealer.Email
              body.html = html.replace(new RegExp(hrefunsubscribe, 'g'), unsubscribeUrl + emailReceiver)
              // satestod chemtan gavagzavno, realobashi gamoirtveba
              emailReceiver = testEmail // <-caishleba
              // console.log('emailReceiver ', emailReceiver)
              body.to = emailReceiver
              if (emailReceiver) {
                // gavcxrilot dilerebi raime algoritmit
                // mag. jer gaugzavneli, gagzavnilebshi kvelaze cotajer da a.sh
                const sentLength = dealer.sent ? dealer.sent.length : 0
                if (sentLength === sentMinLength) {
                  // droebit shevzgudot gagzavnili mailebis raodenoba
                  // realobashi shemdegi 4 xazi caishleba
                  if (!sentEmailOnce) {
                    email.sendEmailHTML(body)
                  }
                  sentEmailOnce = true
                  // droebit gamovrtot mailis gagzavna
                  // email.sendEmailHTML(body)
                  console.log('dealer.Email ', dealer.Email)
                  // chavcerot mongoshi rom mas gaegzavna mail am dros
                  updateOne(createDealerUpdateData(dealer))
                    .then((updated) => {
                      // updated -> { update: true|false, upsertedId }
                      console.log('updated ', updated)
                    })
                    .catch((err) => {
                      $log.info('update err ' + err)
                    })
                  // da am jgufidan mets ar vagzavnit
                  break
                }
              }
            }
          })
        }
      })
      .catch((err) => {
        $log.info('readfile err ' + err)
      })
  })
}
function createDealerUpdateData(item) {
  let sent = item.sent
  if (!sent) {
    sent = [Date.now()]
  } else {
    sent = [Date.now(), ...sent]
  }
  console.log('sent ', sent)
  const query = { _id: item._id }
  const update = {
    $set: {
      sent: sent
    }
  }
  const options = {}
  return { query, update, options, collName: dealersCollection }
}
// asynchron
function readFileText(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, 'utf8', (err, data) => {
      if (err) reject(err)
      // ai aq aris shedegi
      resolve(data)
    })
  })
}
