// const fs = require('fs-extra')
// const { find, updateOne } = require('../utils/MongoClient')
import fs from 'fs-extra'
import { find, updateOne } from '../utils/MongoClient'

export function unsubscribeEmail(res, path) {
  // path =  /unsubscribe/test@web.de
  const email = path.replace('/unsubscribe/', '')
  console.log('email = ', email)
  const query = { Email: email }
  const projection = {}
  const collName = 'dealers'
  const data = { collName, query, projection }
  find(data)
    .then(([dealer]) => {
      if (dealer && !dealer.unsubscribed) {
        const unsubscribed = true
        const options = {}
        const update = {
          $set: {
            unsubscribed: unsubscribed
          }
        }
        updateOne({ collName, query, update, options })
          .then((updated) => {
            // updated -> { update: true|false, upsertedId }
            // console.log('updated ', updated)
            $log.info('updated ' + updated)
            sendFile(res, email)
          })
          .catch((err) => {
            $log.info('update err ' + err)
            sendErrorFile(res)
          })
      } else if (!dealer) {
        sendNoEmailErrorFile(res, email)
      } else {
        sendAlreadyUnsubscribedFile(res, email)
      }
    })
    .catch((err) => {
      console.error($enums.etexts.noDocumentsFound + ' err: ' + err)
      sendNoEmailErrorFile(res, email)
    })
}
function sendFile(res, email) {
  let html = fs.readFileSync('./sendEmails/templates/de/unsubscribed.html', 'utf8')
  html = html.replace(/#email#/g, email)
  res.send(html)
}
function sendAlreadyUnsubscribedFile(res, email) {
  let html = fs.readFileSync('./sendEmails/templates/de/unsubscribedAlready.html', 'utf8')
  html = html.replace(/#email#/g, email)
  res.send(html)
}
function sendErrorFile(res) {
  let html = fs.readFileSync('./sendEmails/templates/de/unsubscribedError.html', 'utf8')
  res.send(html)
}
function sendNoEmailErrorFile(res, email) {
  let html = fs.readFileSync('./sendEmails/templates/de/unsubscribedErrorNoEmail.html', 'utf8')
  html = html.replace(/#email#/g, email)
  res.send(html)
}
