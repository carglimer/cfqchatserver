// caikitxavs 'dealers.xlsx' failidan da chacers mongoshi 'dealers' collection-shi
// node sendEmails/saveindb
require('dotenv').config()
const xlsxFile = require('read-excel-file/node')
const { MongoClient, find, insertOne } = require('../utils/MongoClient')
const EventEmitter2 = require('eventemitter2').EventEmitter2
const { logger } = require('../utils/logger')
const enums = require('../utils/enums2')
global.$log = logger
global.$enums = enums
const dbName = 'desmongo'
const dealersCollection = 'dealers'
const zipsCollection = 'zips'
const excelPath = './sendEmails/dealers.xlsx'

const emitter = new EventEmitter2({
  newListener: false,
  wildcard: true
})
const mongo = new MongoClient(process.env.MONGO_URL, dbName, emitter)
emitter.on('mongo-connected', () => {
  $log.info('mongo-connected')
  readExcelFile(excelPath)
    .then((items) => {
      console.log('items ', items)
      const AllZips = items.map((item) => item.PLZ)
      // get only unique PLZ values
      const zips = [...new Set(AllZips)]
      const zuriaZips = { name: 'Zuria', zips }
      console.log('zips ', zips)
      insertMany(items)
        .then((insertedIds) => {
          console.log('insertedIds ', insertedIds)
          insertOne(zuriaZips)
        })
        .catch((err) => {
          console.log('insertedIds err ', err)
        })
    })
    .catch((err) => {
      console.log('insertMany err ', err)
    })
})
console.log('excelPath ', excelPath)

function readExcelFile(excelPath) {
  const items = []
  return new Promise((resolve, reject) => {
    xlsxFile(excelPath)
      .then((rows) => {
        const columnNames = rows.shift() // Separate first row with column names
        let items = rows.map((row) => {
          // Map the rest of the rows into objects
          const obj = {} // Create object literal for current row
          row.forEach((cell, i) => {
            obj[columnNames[i]] = cell // Use index from current cell to get column name, add current cell to new object
          })
          // console.log('obj ', obj)
          if (obj.Email) {
            return obj
          }
        })
        // ამოვფილტროთ ისინი, სადაც undefined- ar aris
        items = items.filter((item) => !!item)
        resolve(items)
      })
      .catch((err) => {
        $log.error('send err:' + err)
        reject(err)
      })
  })
}
// items = [  { item: "card", qty: 15 },  { item: "envelope", qty: 20 },  { item: "stamps" , qty: 30 }]
function insertMany(items) {
  console.log('saveIntoDB')
  const coll = mongo.getCollection(dealersCollection)
  return new Promise((resolve, reject) => {
    coll
      .insertMany(items)
      .then((res) => {
        resolve(res.insertedIds)
      })
      .catch((err) => {
        // console.log('err ', err)
        reject(err)
      })
  })
}
function insertOne2(item) {
  console.log('saveIntoDB')
  const coll = mongo.getCollection(zipsCollection)
  return new Promise((resolve, reject) => {
    coll
      .insertOne(item)
      .then((res) => {
        resolve(res.insertedId)
      })
      .catch((err) => {
        // console.log('err ', err)
        reject(err)
      })
  })
}
function updateOne(item) {
  console.log('saveIntoDB')
  const coll = mongo.getCollection(dealersCollection)
  const query = { Email: item.Email }
  const update = { $set: item }
  const options = { upsert: true }
  const data = { query, update, options }
  return new Promise((resolve, reject) => {
    coll
      .updateOne(data)
      .then((res) => {
        resolve(res.upsertedId)
      })
      .catch((err) => {
        // console.log('err ', err)
        reject(err)
      })
  })
}
