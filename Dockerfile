FROM node:lts

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install https://gitlab.com/carglimer/helpers.git

RUN npm install --only=production
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

RUN mkdir /usr/src/cfmedias
COPY testUpload.jpg /usr/src/cfmedias/
EXPOSE 80
CMD [ "sh", "-c", "node -r esm chatServer.js" ]