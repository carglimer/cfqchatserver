const express = require('express')
// const https = require('https')
// klientebs am emailidan egzavnebat shekvetis dadastureba
const adminEmail = 'carglimer@web.de'
// klientebs am emailidan egzavnebat shekvetis dadastureba
// const adminEmail = 'carglimer@web.de'
// const bodyParser= require('body-parser')
const EventEmitter2 = require('eventemitter2').EventEmitter2
const loginService = require('./chat/login')
const { logger } = require('./utils/logger')
const MongoClient = require('./utils/MongoClient')
const EmailService = require('./utils/email')
// email-shi gaaqtiurebuli unda ikos  email -> einstellungen -> POP3 IMAP abruf -> POP3 und IMAP Zugriff erlauben
const mailFromOptions = {
  email: adminEmail,
  pw: 'imereti%1',
  host: 'smtp.web.de',
  port: 587
}
/*
 // email-shi gaaqtiurebuli unda ikos
 // gmail-is shemtxvevashi
 const mailFromOptions = {
  email:'gialomidze62@gmail.com',
  pw:'imereti%1',
  service: 'gmail'  // host: 'smtp.gmail.com' port: 465
} */

const dbName = 'socketchat'
// serverData -> rasac serveri ugzavnis klients
const messageTsFormat = 'HH:mm' // 14:27   DD.MM.YY HH:mm
// const systemMessageTsFormat = 'DD.MM.YY HH:mm' // 14 JUNI 2019, heute, gestern, vorgestern
const uploadFileMaxSize = 2 // 2mb
const uploadFileChunkSize = 1024 * 1000
const local = false
exports.admin_pattern = 'admin~1'
// loginUrl: 'http://136.243.102.152:4128'
// loginUrl: 'http://localhost:4128'

let mongoUrl, chatUrl, fotosUploadUrl, imageSrcBase, avatarSrcDefault, flagsImageBase, fileSendMode
const imageDirBase = '../tfotos/'
fileSendMode = 'siofu' // 'siofu' an 'stream'
avatarSrcDefault = '/avatar/1.jpg' //1.jpg
imageSrcBase = '/tfotos/'
flagsImageBase = '/flags/'
mongoUrl = 'mongodb://admin:imereti1@localhost:27017/dbName?authSource=admin'
chatUrl = 'http://136.243.102.152:4129/chat'
if (local) {
  // mongoUrl = 'mongodb://admin:imereti1@localhost:27017/dbName?authSource=admin'
  chatUrl = 'http://localhost:4129/chat'
}
const serverData = {
  fileSendMode,
  chatUrl,
  imageDirBase,
  imageSrcBase,
  avatarSrcDefault,
  messageTsFormat,
  uploadFileMaxSize,
  uploadFileChunkSize,
  fotosUploadUrl,
  flagsImageBase
}
global.$serverData = serverData
const app = express()
const host = 'localhost'
const port = '4128'

// app.use(express.urlencoded({ extended: true }), express.json())
app.use(express.urlencoded({ limit: '50mb', extended: true }), express.json({ limit: '50mb' }))
// app.use(bodyParser.urlencoded({extended: true}))
// app.use(bodyParser.json())

// eslint-disable-next-line no-unused-vars
const errorHandler = (err, _req, res, _next) => {
  const code = err.status || 400
  res.status(code).json(err)
}

// app.use('/api', api, errorHandler)
app.use('/api', errorHandler)

const server = app.listen(port, () => {
  logger.info('cA Server is listening on http:// ' + host + ':' + port)
})

const emitter = new EventEmitter2({
  newListener: false,
  wildcard: true
})

/* emitter.on('mongo-connected', () => {
  console.log('server.timeout ', server.timeout)
  // loginService(server, emitter, mongo)
}) */
const mongo = new MongoClient(mongoUrl, dbName, emitter)
app.use((req, res, next) => {
  logger.info('cA req.url ' + req.url)
  // movacilet cin '/auth'   /auth/login -> /login
  req.url = req.url.replace('/auth', '')
  res.append('Access-Control-Allow-Origin', ['*'])
  //res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.append('Access-Control-Allow-Headers', 'Content-Type')
  next()
})
app.post('/login', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  // logger.info('cA request.body' + request.body)
  if (request.body.meta && request.body.meta.test) {
    // am shemtxvevashi mongo ver ascrebs daqoqvas da amitom ase vaketebt
    testLogin(request, response, '/login')
  } else {
    loginService('/login', request.body, response, emitter, mongo)
  }
})
// es chatAuth.test.js-istvis aris
// es jer mongo-s sheqmnas moucdis da mere imushavebs
function testLogin(request, response, loginOrSignup) {
  emitter.on('mongo-connected', () => {
    logger.info('cA mongo-connected ')
    loginService(loginOrSignup, request.body, response, emitter, mongo)
  })
}
app.post('/signup', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  loginService('/signup', request.body, response, emitter, mongo)
})
app.post('/sendEmail', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  const email = new EmailService(mailFromOptions)
  email.sendEmail(request.body, response)
  // gavugzavne adminsac mis mailze
  email.sendEmail({ email: adminEmail })
})
app.post('/updateUser', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  // console.log(request.body)
  loginService('/updateUser', request.body, response, emitter, mongo)
})
app.post('/loadAllItems', (request, response, next) => {
  logger.info('cA loadAllItems ')
  if (!request.body) return response.sendStatus(400)
  loginService('/loadAllItems', request.body, response, emitter, mongo)
})
app.post('/loadUserOrders', (request, response, next) => {
  if (!request.body) return response.sendStatus(400)
  loginService('/loadUserOrders', request.body, response, emitter, mongo)
})

app.post('/uploadFoto', (request, response, next) => {
  // console.log('153request.body ', request.body)
  if (!request.body) return response.sendStatus(400)
  loginService('/uploadFoto', request, response, emitter, mongo)
})
app.post('/saveOrder', (request, response, next) => {
  // console.log('153request.body ', request.body)
  if (!request.body) return response.sendStatus(400)
  loginService('/saveOrder', request.body, response, emitter, mongo)
})
exports.app = app
// loginService('/login', null, null, emitter, mongo)

/* 

// /avatar_default/erti/ori/avat.png -s gaushvebs aqit ../../avatar_default/erti/ori/avat.png
// sheizleba shemocmeba davamatot
/* app.get('/*', (req, res) => {
  // req.path -> /tfotos/5d6ae3b14f4c182674954eea/avatar/1567600555465_14285.png
  
  let filename = path.posix.basename(req.path) // 1567600555465_14285.png
  let dirname = path.dirname(req.path) // /tfotos/5d6ae3b14f4c182674954eea/avatar
  // filepath -> ../../tfotos/5d6ae3b14f4c182674954eea/avatar
  let filedir = '..' + dirname
  
  res.sendFile(filename, { root: filedir })
  // console.log('99 res ', res)
}) 


/app.get('/tfotos/*', (req, res) => {
  console.log('req.path ', req.path)  
  let filepath = '../../'+ req.path      // __dirname + req.path
  console.log('avatar filepath', filepath)
  res.sendFile(filepath) 
})
app.get('/flags/*', (req, res) => {
  console.log('req.path ', req.path)  
  let filepath = __dirname + req.path+'.png'  
  console.log('flags filepath', filepath)
  res.sendFile(filepath) 
})
  ssl_certificate_key  path: /etc/letsencrypt/live/kfz-soft.com-0001/privkey.pem
  ssl_certificate path: /etc/letsencrypt/live/kfz-soft.com-0001/cert.pem
  const options = {
  key: fs.readFileSync('/etc/letsencrypt/live/kfz-soft.com-0001/privkey.pem'),
  cert: fs.readFileSync('/etc/letsencrypt/live/kfz-soft.com-0001/cert.pem')
}
  https.createServer(options, app).listen(port)
*/
